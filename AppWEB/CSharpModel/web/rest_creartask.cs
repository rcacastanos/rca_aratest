using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class rest_creartask : GXProcedure
   {
      public rest_creartask( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public rest_creartask( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( string aP0_TaskName ,
                           string aP1_TaskDescription ,
                           short aP2_TaskStatus ,
                           short aP3_UserId ,
                           out bool aP4_IsOK ,
                           out string aP5_ErrorMessage )
      {
         this.AV20TaskName = aP0_TaskName;
         this.AV18TaskDescription = aP1_TaskDescription;
         this.AV21TaskStatus = aP2_TaskStatus;
         this.AV22UserId = aP3_UserId;
         this.AV12IsOK = false ;
         this.AV10ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP4_IsOK=this.AV12IsOK;
         aP5_ErrorMessage=this.AV10ErrorMessage;
      }

      public string executeUdp( string aP0_TaskName ,
                                string aP1_TaskDescription ,
                                short aP2_TaskStatus ,
                                short aP3_UserId ,
                                out bool aP4_IsOK )
      {
         execute(aP0_TaskName, aP1_TaskDescription, aP2_TaskStatus, aP3_UserId, out aP4_IsOK, out aP5_ErrorMessage);
         return AV10ErrorMessage ;
      }

      public void executeSubmit( string aP0_TaskName ,
                                 string aP1_TaskDescription ,
                                 short aP2_TaskStatus ,
                                 short aP3_UserId ,
                                 out bool aP4_IsOK ,
                                 out string aP5_ErrorMessage )
      {
         rest_creartask objrest_creartask;
         objrest_creartask = new rest_creartask();
         objrest_creartask.AV20TaskName = aP0_TaskName;
         objrest_creartask.AV18TaskDescription = aP1_TaskDescription;
         objrest_creartask.AV21TaskStatus = aP2_TaskStatus;
         objrest_creartask.AV22UserId = aP3_UserId;
         objrest_creartask.AV12IsOK = false ;
         objrest_creartask.AV10ErrorMessage = "" ;
         objrest_creartask.context.SetSubmitInitialConfig(context);
         objrest_creartask.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objrest_creartask);
         aP4_IsOK=this.AV12IsOK;
         aP5_ErrorMessage=this.AV10ErrorMessage;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((rest_creartask)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV12IsOK = false;
         AV14ServiceName = "WSTaskCreate";
         AV8CadenaJSON = "{\"TaskName\": \"" + StringUtil.Trim( AV20TaskName) + "\",\"TaskDescription\": \"" + StringUtil.Trim( AV18TaskDescription) + "\", \"TaskStatus\":" + StringUtil.Trim( StringUtil.Str( (decimal)(AV21TaskStatus), 4, 0)) + ",\"UserId\": " + StringUtil.Trim( StringUtil.Str( (decimal)(AV22UserId), 4, 0)) + "}";
         new api_rest(context ).execute(  AV14ServiceName,  AV8CadenaJSON, out  AV13ResultadoJson, out  AV9ErrorCode, out  AV10ErrorMessage) ;
         if ( AV9ErrorCode == 0 )
         {
            AV12IsOK = true;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections();
         }
         ExitApp();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV10ErrorMessage = "";
         AV14ServiceName = "";
         AV8CadenaJSON = "";
         AV13ResultadoJson = "";
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV21TaskStatus ;
      private short AV22UserId ;
      private short AV9ErrorCode ;
      private bool AV12IsOK ;
      private string AV18TaskDescription ;
      private string AV10ErrorMessage ;
      private string AV8CadenaJSON ;
      private string AV13ResultadoJson ;
      private string AV20TaskName ;
      private string AV14ServiceName ;
      private bool aP4_IsOK ;
      private string aP5_ErrorMessage ;
   }

}
