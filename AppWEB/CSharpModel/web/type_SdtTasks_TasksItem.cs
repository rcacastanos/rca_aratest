/*
				   File: type_SdtTasks_TasksItem
			Description: Tasks
				 Author: Nemo 🐠 for C# version 17.0.6.154974
		   Program type: Callable routine
			  Main DBMS: 
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Reflection;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Web.Services.Protocols;


namespace GeneXus.Programs
{
	[XmlSerializerFormat]
	[XmlRoot(ElementName="TasksItem")]
	[XmlType(TypeName="TasksItem" , Namespace="ArqTest" )]
	[Serializable]
	public class SdtTasks_TasksItem : GxUserType
	{
		public SdtTasks_TasksItem( )
		{
			/* Constructor for serialization */
			gxTv_SdtTasks_TasksItem_Taskname = "";

			gxTv_SdtTasks_TasksItem_Taskdescription = "";

			gxTv_SdtTasks_TasksItem_Taskcreatedate = (DateTime)(DateTime.MinValue);

		}

		public SdtTasks_TasksItem(IGxContext context)
		{
			this.context = context;	
			initialize();
		}

		#region Json
		private static Hashtable mapper;
		public override string JsonMap(string value)
		{
			if (mapper == null)
			{
				mapper = new Hashtable();
			}
			return (string)mapper[value]; ;
		}

		public override void ToJSON()
		{
			ToJSON(true) ;
			return;
		}

		public override void ToJSON(bool includeState)
		{
			AddObjectProperty("TaskId", gxTpr_Taskid, false);


			AddObjectProperty("TaskName", gxTpr_Taskname, false);


			AddObjectProperty("TaskDescription", gxTpr_Taskdescription, false);


			datetime_STZ = gxTpr_Taskcreatedate;
			sDateCnv = "";
			sNumToPad = StringUtil.Trim(StringUtil.Str((decimal)(DateTimeUtil.Year(datetime_STZ)), 10, 0));
			sDateCnv = sDateCnv + StringUtil.Substring("0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
			sDateCnv = sDateCnv + "-";
			sNumToPad = StringUtil.Trim( StringUtil.Str((decimal)(DateTimeUtil.Month(datetime_STZ)), 10, 0));
			sDateCnv = sDateCnv + StringUtil.Substring("00", 1, 2-StringUtil.Len(sNumToPad)) + sNumToPad;
			sDateCnv = sDateCnv + "-";
			sNumToPad = StringUtil.Trim(StringUtil.Str((decimal)(DateTimeUtil.Day(datetime_STZ)), 10, 0));
			sDateCnv = sDateCnv + StringUtil.Substring("00", 1, 2-StringUtil.Len(sNumToPad)) + sNumToPad;
			sDateCnv = sDateCnv + "T";
			sNumToPad = StringUtil.Trim(StringUtil.Str((decimal)(DateTimeUtil.Hour(datetime_STZ)), 10, 0));
			sDateCnv = sDateCnv + StringUtil.Substring("00", 1, 2-StringUtil.Len(sNumToPad)) + sNumToPad;
			sDateCnv = sDateCnv + ":";
			sNumToPad = StringUtil.Trim(StringUtil.Str((decimal)(DateTimeUtil.Minute(datetime_STZ)), 10, 0));
			sDateCnv = sDateCnv + StringUtil.Substring("00", 1, 2-StringUtil.Len(sNumToPad)) + sNumToPad;
			sDateCnv = sDateCnv + ":";
			sNumToPad = StringUtil.Trim(StringUtil.Str((decimal)(DateTimeUtil.Second(datetime_STZ)), 10, 0));
			sDateCnv = sDateCnv + StringUtil.Substring("00", 1, 2-StringUtil.Len(sNumToPad)) + sNumToPad;
			AddObjectProperty("TaskCreateDate", sDateCnv, false);


			AddObjectProperty("TaskStatus", gxTpr_Taskstatus, false);


			AddObjectProperty("UserId", gxTpr_Userid, false);

			return;
		}
		#endregion

		#region Properties

		[SoapElement(ElementName="TaskId")]
		[XmlElement(ElementName="TaskId")]
		public short gxTpr_Taskid
		{
			get { 
				return gxTv_SdtTasks_TasksItem_Taskid; 
			}
			set { 
				gxTv_SdtTasks_TasksItem_Taskid = value;
				SetDirty("Taskid");
			}
		}




		[SoapElement(ElementName="TaskName")]
		[XmlElement(ElementName="TaskName")]
		public string gxTpr_Taskname
		{
			get { 
				return gxTv_SdtTasks_TasksItem_Taskname; 
			}
			set { 
				gxTv_SdtTasks_TasksItem_Taskname = value;
				SetDirty("Taskname");
			}
		}




		[SoapElement(ElementName="TaskDescription")]
		[XmlElement(ElementName="TaskDescription")]
		public string gxTpr_Taskdescription
		{
			get { 
				return gxTv_SdtTasks_TasksItem_Taskdescription; 
			}
			set { 
				gxTv_SdtTasks_TasksItem_Taskdescription = value;
				SetDirty("Taskdescription");
			}
		}



		[SoapElement(ElementName="TaskCreateDate")]
		[XmlElement(ElementName="TaskCreateDate" , IsNullable=true)]
		public string gxTpr_Taskcreatedate_Nullable
		{
			get {
				if ( gxTv_SdtTasks_TasksItem_Taskcreatedate == DateTime.MinValue)
					return null;
				return new GxDatetimeString(gxTv_SdtTasks_TasksItem_Taskcreatedate).value ;
			}
			set {
				gxTv_SdtTasks_TasksItem_Taskcreatedate = DateTimeUtil.CToD2(value);
			}
		}

		[SoapIgnore]
		[XmlIgnore]
		public DateTime gxTpr_Taskcreatedate
		{
			get { 
				return gxTv_SdtTasks_TasksItem_Taskcreatedate; 
			}
			set { 
				gxTv_SdtTasks_TasksItem_Taskcreatedate = value;
				SetDirty("Taskcreatedate");
			}
		}



		[SoapElement(ElementName="TaskStatus")]
		[XmlElement(ElementName="TaskStatus")]
		public short gxTpr_Taskstatus
		{
			get { 
				return gxTv_SdtTasks_TasksItem_Taskstatus; 
			}
			set { 
				gxTv_SdtTasks_TasksItem_Taskstatus = value;
				SetDirty("Taskstatus");
			}
		}




		[SoapElement(ElementName="UserId")]
		[XmlElement(ElementName="UserId")]
		public short gxTpr_Userid
		{
			get { 
				return gxTv_SdtTasks_TasksItem_Userid; 
			}
			set { 
				gxTv_SdtTasks_TasksItem_Userid = value;
				SetDirty("Userid");
			}
		}




		public override bool ShouldSerializeSdtJson()
		{
		 
		  return true; 
		}

		#endregion

		#region Initialization

		public void initialize( )
		{
			gxTv_SdtTasks_TasksItem_Taskname = "";
			gxTv_SdtTasks_TasksItem_Taskdescription = "";
			gxTv_SdtTasks_TasksItem_Taskcreatedate = (DateTime)(DateTime.MinValue);


			datetime_STZ = (DateTime)(DateTime.MinValue);
			sDateCnv = "";
			sNumToPad = "";
			return  ;
		}



		#endregion

		#region Declaration

		protected string sDateCnv ;
		protected string sNumToPad ;
		protected DateTime datetime_STZ ;

		protected short gxTv_SdtTasks_TasksItem_Taskid;
		 

		protected string gxTv_SdtTasks_TasksItem_Taskname;
		 

		protected string gxTv_SdtTasks_TasksItem_Taskdescription;
		 

		protected DateTime gxTv_SdtTasks_TasksItem_Taskcreatedate;
		 

		protected short gxTv_SdtTasks_TasksItem_Taskstatus;
		 

		protected short gxTv_SdtTasks_TasksItem_Userid;
		 


		#endregion
	}
	#region Rest interface
	[DataContract(Name=@"TasksItem", Namespace="ArqTest")]
	public class SdtTasks_TasksItem_RESTInterface : GxGenericCollectionItem<SdtTasks_TasksItem>, System.Web.SessionState.IRequiresSessionState
	{
		public SdtTasks_TasksItem_RESTInterface( ) : base()
		{	
		}

		public SdtTasks_TasksItem_RESTInterface( SdtTasks_TasksItem psdt ) : base(psdt)
		{	
		}

		#region Rest Properties
		[DataMember(Name="TaskId", Order=0)]
		public short gxTpr_Taskid
		{
			get { 
				return sdt.gxTpr_Taskid;

			}
			set { 
				sdt.gxTpr_Taskid = value;
			}
		}

		[DataMember(Name="TaskName", Order=1)]
		public  string gxTpr_Taskname
		{
			get { 
				return sdt.gxTpr_Taskname;

			}
			set { 
				 sdt.gxTpr_Taskname = value;
			}
		}

		[DataMember(Name="TaskDescription", Order=2)]
		public  string gxTpr_Taskdescription
		{
			get { 
				return sdt.gxTpr_Taskdescription;

			}
			set { 
				 sdt.gxTpr_Taskdescription = value;
			}
		}

		[DataMember(Name="TaskCreateDate", Order=3)]
		public  string gxTpr_Taskcreatedate
		{
			get { 
				return DateTimeUtil.TToC2( sdt.gxTpr_Taskcreatedate);

			}
			set { 
				sdt.gxTpr_Taskcreatedate = DateTimeUtil.CToT2(value);
			}
		}

		[DataMember(Name="TaskStatus", Order=4)]
		public short gxTpr_Taskstatus
		{
			get { 
				return sdt.gxTpr_Taskstatus;

			}
			set { 
				sdt.gxTpr_Taskstatus = value;
			}
		}

		[DataMember(Name="UserId", Order=5)]
		public short gxTpr_Userid
		{
			get { 
				return sdt.gxTpr_Userid;

			}
			set { 
				sdt.gxTpr_Userid = value;
			}
		}


		#endregion

		public SdtTasks_TasksItem sdt
		{
			get { 
				return (SdtTasks_TasksItem)Sdt;
			}
			set { 
				Sdt = value;
			}
		}

		[OnDeserializing]
		void checkSdt( StreamingContext ctx )
		{
			if ( sdt == null )
			{
				sdt = new SdtTasks_TasksItem() ;
			}
		}
	}
	#endregion
}