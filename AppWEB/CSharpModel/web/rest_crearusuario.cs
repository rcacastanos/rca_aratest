using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class rest_crearusuario : GXProcedure
   {
      public rest_crearusuario( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public rest_crearusuario( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( string aP0_UserName ,
                           string aP1_UserNickName ,
                           string aP2_UserPassword ,
                           out bool aP3_IsOK ,
                           out string aP4_ErrorMessage )
      {
         this.AV9UserName = aP0_UserName;
         this.AV17UserNickName = aP1_UserNickName;
         this.AV10UserPassword = aP2_UserPassword;
         this.AV18IsOK = false ;
         this.AV15ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP3_IsOK=this.AV18IsOK;
         aP4_ErrorMessage=this.AV15ErrorMessage;
      }

      public string executeUdp( string aP0_UserName ,
                                string aP1_UserNickName ,
                                string aP2_UserPassword ,
                                out bool aP3_IsOK )
      {
         execute(aP0_UserName, aP1_UserNickName, aP2_UserPassword, out aP3_IsOK, out aP4_ErrorMessage);
         return AV15ErrorMessage ;
      }

      public void executeSubmit( string aP0_UserName ,
                                 string aP1_UserNickName ,
                                 string aP2_UserPassword ,
                                 out bool aP3_IsOK ,
                                 out string aP4_ErrorMessage )
      {
         rest_crearusuario objrest_crearusuario;
         objrest_crearusuario = new rest_crearusuario();
         objrest_crearusuario.AV9UserName = aP0_UserName;
         objrest_crearusuario.AV17UserNickName = aP1_UserNickName;
         objrest_crearusuario.AV10UserPassword = aP2_UserPassword;
         objrest_crearusuario.AV18IsOK = false ;
         objrest_crearusuario.AV15ErrorMessage = "" ;
         objrest_crearusuario.context.SetSubmitInitialConfig(context);
         objrest_crearusuario.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objrest_crearusuario);
         aP3_IsOK=this.AV18IsOK;
         aP4_ErrorMessage=this.AV15ErrorMessage;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((rest_crearusuario)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV18IsOK = false;
         AV16ServiceName = "WSCreateUser";
         AV11CadenaJSON = "{\"UserName\": \"" + StringUtil.Trim( AV9UserName) + "\",\"UserNickName\": \"" + StringUtil.Trim( AV17UserNickName) + "\", \"UserPassword\":\"" + AV10UserPassword + "\"}";
         new api_rest(context ).execute(  AV16ServiceName,  AV11CadenaJSON, out  AV13ResultadoJson, out  AV14ErrorCode, out  AV15ErrorMessage) ;
         if ( AV14ErrorCode == 0 )
         {
            AV18IsOK = true;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections();
         }
         ExitApp();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV15ErrorMessage = "";
         AV16ServiceName = "";
         AV11CadenaJSON = "";
         AV13ResultadoJson = "";
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV14ErrorCode ;
      private bool AV18IsOK ;
      private string AV15ErrorMessage ;
      private string AV11CadenaJSON ;
      private string AV13ResultadoJson ;
      private string AV9UserName ;
      private string AV17UserNickName ;
      private string AV10UserPassword ;
      private string AV16ServiceName ;
      private bool aP3_IsOK ;
      private string aP4_ErrorMessage ;
   }

}
