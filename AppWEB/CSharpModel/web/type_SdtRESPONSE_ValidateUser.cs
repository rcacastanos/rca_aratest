/*
				   File: type_SdtRESPONSE_ValidateUser
			Description: RESPONSE_ValidateUser
				 Author: Nemo 🐠 for C# version 17.0.6.154974
		   Program type: Callable routine
			  Main DBMS: 
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Reflection;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Web.Services.Protocols;


namespace GeneXus.Programs
{
	[XmlSerializerFormat]
	[XmlRoot(ElementName="RESPONSE_ValidateUser")]
	[XmlType(TypeName="RESPONSE_ValidateUser" , Namespace="ArqTest" )]
	[Serializable]
	public class SdtRESPONSE_ValidateUser : GxUserType
	{
		public SdtRESPONSE_ValidateUser( )
		{
			/* Constructor for serialization */
		}

		public SdtRESPONSE_ValidateUser(IGxContext context)
		{
			this.context = context;	
			initialize();
		}

		#region Json
		private static Hashtable mapper;
		public override string JsonMap(string value)
		{
			if (mapper == null)
			{
				mapper = new Hashtable();
			}
			return (string)mapper[value]; ;
		}

		public override void ToJSON()
		{
			ToJSON(true) ;
			return;
		}

		public override void ToJSON(bool includeState)
		{
			AddObjectProperty("IsAccess", gxTpr_Isaccess, false);


			AddObjectProperty("UserId", gxTpr_Userid, false);

			return;
		}
		#endregion

		#region Properties

		[SoapElement(ElementName="IsAccess")]
		[XmlElement(ElementName="IsAccess")]
		public bool gxTpr_Isaccess
		{
			get { 
				return gxTv_SdtRESPONSE_ValidateUser_Isaccess; 
			}
			set { 
				gxTv_SdtRESPONSE_ValidateUser_Isaccess = value;
				SetDirty("Isaccess");
			}
		}



		[SoapElement(ElementName="UserId")]
		[XmlElement(ElementName="UserId")]
		public string gxTpr_Userid_double
		{
			get {
				return Convert.ToString(gxTv_SdtRESPONSE_ValidateUser_Userid, System.Globalization.CultureInfo.InvariantCulture);
			}
			set {
				gxTv_SdtRESPONSE_ValidateUser_Userid = (decimal)(Convert.ToDecimal(value, System.Globalization.CultureInfo.InvariantCulture));
			}
		}
		[SoapIgnore]
		[XmlIgnore]
		public decimal gxTpr_Userid
		{
			get { 
				return gxTv_SdtRESPONSE_ValidateUser_Userid; 
			}
			set { 
				gxTv_SdtRESPONSE_ValidateUser_Userid = value;
				SetDirty("Userid");
			}
		}




		public override bool ShouldSerializeSdtJson()
		{
		 
		  return true; 
		}

		#endregion

		#region Initialization

		public void initialize( )
		{
			return  ;
		}



		#endregion

		#region Declaration

		protected bool gxTv_SdtRESPONSE_ValidateUser_Isaccess;
		 

		protected decimal gxTv_SdtRESPONSE_ValidateUser_Userid;
		 


		#endregion
	}
	#region Rest interface
	[GxUnWrappedJson()]
	[DataContract(Name=@"RESPONSE_ValidateUser", Namespace="ArqTest")]
	public class SdtRESPONSE_ValidateUser_RESTInterface : GxGenericCollectionItem<SdtRESPONSE_ValidateUser>, System.Web.SessionState.IRequiresSessionState
	{
		public SdtRESPONSE_ValidateUser_RESTInterface( ) : base()
		{	
		}

		public SdtRESPONSE_ValidateUser_RESTInterface( SdtRESPONSE_ValidateUser psdt ) : base(psdt)
		{	
		}

		#region Rest Properties
		[DataMember(Name="IsAccess", Order=0)]
		public bool gxTpr_Isaccess
		{
			get { 
				return sdt.gxTpr_Isaccess;

			}
			set { 
				sdt.gxTpr_Isaccess = value;
			}
		}

		[DataMember(Name="UserId", Order=1)]
		public  string gxTpr_Userid
		{
			get { 
				return StringUtil.LTrim( StringUtil.Str(  sdt.gxTpr_Userid, 10, 5));

			}
			set { 
				sdt.gxTpr_Userid =  NumberUtil.Val( value, ".");
			}
		}


		#endregion

		public SdtRESPONSE_ValidateUser sdt
		{
			get { 
				return (SdtRESPONSE_ValidateUser)Sdt;
			}
			set { 
				Sdt = value;
			}
		}

		[OnDeserializing]
		void checkSdt( StreamingContext ctx )
		{
			if ( sdt == null )
			{
				sdt = new SdtRESPONSE_ValidateUser() ;
			}
		}
	}
	#endregion
}