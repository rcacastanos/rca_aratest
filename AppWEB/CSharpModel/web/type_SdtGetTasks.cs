/*
				   File: type_SdtGetTasks
			Description: GetTasks
				 Author: Nemo 🐠 for C# version 17.0.6.154974
		   Program type: Callable routine
			  Main DBMS: 
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Reflection;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Web.Services.Protocols;


namespace GeneXus.Programs
{
	[XmlSerializerFormat]
	[XmlRoot(ElementName="GetTasks")]
	[XmlType(TypeName="GetTasks" , Namespace="ArqTest" )]
	[Serializable]
	public class SdtGetTasks : GxUserType
	{
		public SdtGetTasks( )
		{
			/* Constructor for serialization */
		}

		public SdtGetTasks(IGxContext context)
		{
			this.context = context;	
			initialize();
		}

		#region Json
		private static Hashtable mapper;
		public override string JsonMap(string value)
		{
			if (mapper == null)
			{
				mapper = new Hashtable();
			}
			return (string)mapper[value]; ;
		}

		public override void ToJSON()
		{
			ToJSON(true) ;
			return;
		}

		public override void ToJSON(bool includeState)
		{
			if (gxTv_SdtGetTasks_Tasks != null)
			{
				AddObjectProperty("Tasks", gxTv_SdtGetTasks_Tasks, false);  
			}
			return;
		}
		#endregion

		#region Properties

		[SoapElement(ElementName="Tasks" )]
		[XmlArray(ElementName="Tasks"  )]
		[XmlArrayItemAttribute(ElementName="TasksItem" , IsNullable=false )]
		public GXBaseCollection<SdtGetTasks_TasksItem> gxTpr_Tasks
		{
			get {
				if ( gxTv_SdtGetTasks_Tasks == null )
				{
					gxTv_SdtGetTasks_Tasks = new GXBaseCollection<SdtGetTasks_TasksItem>( context, "GetTasks.TasksItem", "");
				}
				return gxTv_SdtGetTasks_Tasks;
			}
			set {
				if ( gxTv_SdtGetTasks_Tasks == null )
				{
					gxTv_SdtGetTasks_Tasks = new GXBaseCollection<SdtGetTasks_TasksItem>( context, "GetTasks.TasksItem", "");
				}
				gxTv_SdtGetTasks_Tasks_N = 0;

				gxTv_SdtGetTasks_Tasks = value;
				SetDirty("Tasks");
			}
		}

		public void gxTv_SdtGetTasks_Tasks_SetNull()
		{
			gxTv_SdtGetTasks_Tasks_N = 1;

			gxTv_SdtGetTasks_Tasks = null;
			return  ;
		}

		public bool gxTv_SdtGetTasks_Tasks_IsNull()
		{
			if (gxTv_SdtGetTasks_Tasks == null)
			{
				return true ;
			}
			return false ;
		}

		public bool ShouldSerializegxTpr_Tasks_GxSimpleCollection_Json()
		{
				return gxTv_SdtGetTasks_Tasks != null && gxTv_SdtGetTasks_Tasks.Count > 0;

		}



		public override bool ShouldSerializeSdtJson()
		{
		  return ( 
		  ShouldSerializegxTpr_Tasks_GxSimpleCollection_Json() || 
		  false  );
		}

		#endregion

		#region Initialization

		public void initialize( )
		{
			gxTv_SdtGetTasks_Tasks_N = 1;

			return  ;
		}



		#endregion

		#region Declaration

		protected short gxTv_SdtGetTasks_Tasks_N;
		protected GXBaseCollection<SdtGetTasks_TasksItem> gxTv_SdtGetTasks_Tasks = null; 



		#endregion
	}
	#region Rest interface
	[GxUnWrappedJson()]
	[DataContract(Name=@"GetTasks", Namespace="ArqTest")]
	public class SdtGetTasks_RESTInterface : GxGenericCollectionItem<SdtGetTasks>, System.Web.SessionState.IRequiresSessionState
	{
		public SdtGetTasks_RESTInterface( ) : base()
		{	
		}

		public SdtGetTasks_RESTInterface( SdtGetTasks psdt ) : base(psdt)
		{	
		}

		#region Rest Properties
		[DataMember(Name="Tasks", Order=0, EmitDefaultValue=false)]
		public GxGenericCollection<SdtGetTasks_TasksItem_RESTInterface> gxTpr_Tasks
		{
			get {
				if (sdt.ShouldSerializegxTpr_Tasks_GxSimpleCollection_Json())
					return new GxGenericCollection<SdtGetTasks_TasksItem_RESTInterface>(sdt.gxTpr_Tasks);
				else
					return null;

			}
			set {
				value.LoadCollection(sdt.gxTpr_Tasks);
			}
		}


		#endregion

		public SdtGetTasks sdt
		{
			get { 
				return (SdtGetTasks)Sdt;
			}
			set { 
				Sdt = value;
			}
		}

		[OnDeserializing]
		void checkSdt( StreamingContext ctx )
		{
			if ( sdt == null )
			{
				sdt = new SdtGetTasks() ;
			}
		}
	}
	#endregion
}