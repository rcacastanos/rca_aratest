using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class rest_gettarea : GXProcedure
   {
      public rest_gettarea( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public rest_gettarea( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( short aP0_TaskId ,
                           out GXBaseCollection<SdtTasks_TasksItem> aP1_Tasks ,
                           out bool aP2_IsOK ,
                           out string aP3_ErrorMessage )
      {
         this.AV15TaskId = aP0_TaskId;
         this.AV17Tasks = new GXBaseCollection<SdtTasks_TasksItem>( context, "TasksItem", "ArqTest") ;
         this.AV11IsOK = false ;
         this.AV10ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP1_Tasks=this.AV17Tasks;
         aP2_IsOK=this.AV11IsOK;
         aP3_ErrorMessage=this.AV10ErrorMessage;
      }

      public string executeUdp( short aP0_TaskId ,
                                out GXBaseCollection<SdtTasks_TasksItem> aP1_Tasks ,
                                out bool aP2_IsOK )
      {
         execute(aP0_TaskId, out aP1_Tasks, out aP2_IsOK, out aP3_ErrorMessage);
         return AV10ErrorMessage ;
      }

      public void executeSubmit( short aP0_TaskId ,
                                 out GXBaseCollection<SdtTasks_TasksItem> aP1_Tasks ,
                                 out bool aP2_IsOK ,
                                 out string aP3_ErrorMessage )
      {
         rest_gettarea objrest_gettarea;
         objrest_gettarea = new rest_gettarea();
         objrest_gettarea.AV15TaskId = aP0_TaskId;
         objrest_gettarea.AV17Tasks = new GXBaseCollection<SdtTasks_TasksItem>( context, "TasksItem", "ArqTest") ;
         objrest_gettarea.AV11IsOK = false ;
         objrest_gettarea.AV10ErrorMessage = "" ;
         objrest_gettarea.context.SetSubmitInitialConfig(context);
         objrest_gettarea.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objrest_gettarea);
         aP1_Tasks=this.AV17Tasks;
         aP2_IsOK=this.AV11IsOK;
         aP3_ErrorMessage=this.AV10ErrorMessage;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((rest_gettarea)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV11IsOK = false;
         AV13ServiceName = "WSGetTask";
         AV8CadenaJSON = "{\"TaskId\": " + StringUtil.Trim( StringUtil.Str( (decimal)(AV15TaskId), 4, 0)) + "}";
         new api_rest(context ).execute(  AV13ServiceName,  AV8CadenaJSON, out  AV12ResultadoJson, out  AV9ErrorCode, out  AV10ErrorMessage) ;
         if ( AV9ErrorCode == 0 )
         {
            AV20GetTasks.FromJSonString(AV12ResultadoJson, null);
            AV17Tasks.FromJSonString(AV20GetTasks.gxTpr_Tasks.ToJSonString(false), null);
            AV11IsOK = true;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections();
         }
         ExitApp();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV17Tasks = new GXBaseCollection<SdtTasks_TasksItem>( context, "TasksItem", "ArqTest");
         AV10ErrorMessage = "";
         AV13ServiceName = "";
         AV8CadenaJSON = "";
         AV12ResultadoJson = "";
         AV20GetTasks = new SdtGetTasks(context);
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV15TaskId ;
      private short AV9ErrorCode ;
      private bool AV11IsOK ;
      private string AV10ErrorMessage ;
      private string AV8CadenaJSON ;
      private string AV12ResultadoJson ;
      private string AV13ServiceName ;
      private GXBaseCollection<SdtTasks_TasksItem> aP1_Tasks ;
      private bool aP2_IsOK ;
      private string aP3_ErrorMessage ;
      private GXBaseCollection<SdtTasks_TasksItem> AV17Tasks ;
      private SdtGetTasks AV20GetTasks ;
   }

}
