gx.evt.autoSkip = false;
gx.define('deletetask', false, function () {
   this.ServerClass =  "deletetask" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.ServerFullClass =  "deletetask.aspx" ;
   this.setObjectType("web");
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.ajaxSecurityToken =  true ;
   this.SetStandaloneVars=function()
   {
   };
   this.e120d2_client=function()
   {
      return this.executeServerEvent("'CONFIRMAR'", false, null, false, false);
   };
   this.e130d2_client=function()
   {
      return this.executeServerEvent("'CANCELAR'", false, null, false, false);
   };
   this.e150d2_client=function()
   {
      return this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e160d2_client=function()
   {
      return this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,36,38];
   this.GXLastCtrlId =38;
   GXValidFnc[2]={ id: 2, fld:"",grid:0};
   GXValidFnc[3]={ id: 3, fld:"MAINTABLE",grid:0};
   GXValidFnc[4]={ id: 4, fld:"",grid:0};
   GXValidFnc[5]={ id: 5, fld:"",grid:0};
   GXValidFnc[6]={ id: 6, fld:"GROUP1",grid:0};
   GXValidFnc[7]={ id: 7, fld:"GROUP1TABLE",grid:0};
   GXValidFnc[8]={ id: 8, fld:"",grid:0};
   GXValidFnc[9]={ id: 9, fld:"",grid:0};
   GXValidFnc[10]={ id: 10, fld:"TABLE1",grid:0};
   GXValidFnc[11]={ id: 11, fld:"",grid:0};
   GXValidFnc[12]={ id: 12, fld:"",grid:0};
   GXValidFnc[13]={ id: 13, fld:"",grid:0};
   GXValidFnc[14]={ id: 14, fld:"",grid:0};
   GXValidFnc[15]={ id:15 ,lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[],fld:"vTASKID",gxz:"ZV6TaskId",gxold:"OV6TaskId",gxvar:"AV6TaskId",ucs:[],op:[],ip:[],
						nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV6TaskId=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV6TaskId=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vTASKID",gx.O.AV6TaskId,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV6TaskId=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vTASKID",'.')},nac:gx.falseFn};
   GXValidFnc[16]={ id: 16, fld:"",grid:0};
   GXValidFnc[17]={ id: 17, fld:"",grid:0};
   GXValidFnc[18]={ id: 18, fld:"",grid:0};
   GXValidFnc[19]={ id: 19, fld:"",grid:0};
   GXValidFnc[20]={ id:20 ,lvl:0,type:"svchar",len:40,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[],fld:"vTASKNAME",gxz:"ZV7TaskName",gxold:"OV7TaskName",gxvar:"AV7TaskName",ucs:[],op:[],ip:[],
						nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV7TaskName=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV7TaskName=Value},v2c:function(){gx.fn.setControlValue("vTASKNAME",gx.O.AV7TaskName,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV7TaskName=this.val()},val:function(){return gx.fn.getControlValue("vTASKNAME")},nac:gx.falseFn};
   GXValidFnc[21]={ id: 21, fld:"",grid:0};
   GXValidFnc[22]={ id: 22, fld:"",grid:0};
   GXValidFnc[23]={ id: 23, fld:"",grid:0};
   GXValidFnc[24]={ id: 24, fld:"",grid:0};
   GXValidFnc[25]={ id:25 ,lvl:0,type:"vchar",len:2097152,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[],fld:"vTASKDESCRIPTION",gxz:"ZV5TaskDescription",gxold:"OV5TaskDescription",gxvar:"AV5TaskDescription",ucs:[],op:[],ip:[],
						nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV5TaskDescription=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV5TaskDescription=Value},v2c:function(){gx.fn.setControlValue("vTASKDESCRIPTION",gx.O.AV5TaskDescription,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV5TaskDescription=this.val()},val:function(){return gx.fn.getControlValue("vTASKDESCRIPTION")},nac:gx.falseFn};
   GXValidFnc[26]={ id: 26, fld:"",grid:0};
   GXValidFnc[27]={ id: 27, fld:"",grid:0};
   GXValidFnc[28]={ id: 28, fld:"",grid:0};
   GXValidFnc[29]={ id: 29, fld:"",grid:0};
   GXValidFnc[30]={ id:30 ,lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[],fld:"vTASKSTATUS",gxz:"ZV8TaskStatus",gxold:"OV8TaskStatus",gxvar:"AV8TaskStatus",ucs:[],op:[],ip:[],
						nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV8TaskStatus=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV8TaskStatus=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vTASKSTATUS",gx.O.AV8TaskStatus)},c2v:function(){if(this.val()!==undefined)gx.O.AV8TaskStatus=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vTASKSTATUS",'.')},nac:gx.falseFn};
   GXValidFnc[31]={ id: 31, fld:"",grid:0};
   GXValidFnc[32]={ id: 32, fld:"",grid:0};
   GXValidFnc[33]={ id: 33, fld:"TABLE2",grid:0};
   GXValidFnc[36]={ id: 36, fld:"CONFIRMAR",grid:0,evt:"e120d2_client"};
   GXValidFnc[38]={ id: 38, fld:"CANCELAR",grid:0,evt:"e130d2_client"};
   this.AV6TaskId = 0 ;
   this.ZV6TaskId = 0 ;
   this.OV6TaskId = 0 ;
   this.AV7TaskName = "" ;
   this.ZV7TaskName = "" ;
   this.OV7TaskName = "" ;
   this.AV5TaskDescription = "" ;
   this.ZV5TaskDescription = "" ;
   this.OV5TaskDescription = "" ;
   this.AV8TaskStatus = 0 ;
   this.ZV8TaskStatus = 0 ;
   this.OV8TaskStatus = 0 ;
   this.AV6TaskId = 0 ;
   this.AV7TaskName = "" ;
   this.AV5TaskDescription = "" ;
   this.AV8TaskStatus = 0 ;
   this.Events = {"e120d2_client": ["'CONFIRMAR'", true] ,"e130d2_client": ["'CANCELAR'", true] ,"e150d2_client": ["ENTER", true] ,"e160d2_client": ["CANCEL", true]};
   this.EvtParms["REFRESH"] = [[{av:'AV6TaskId',fld:'vTASKID',pic:'ZZZ9',hsh:true},{av:'AV7TaskName',fld:'vTASKNAME',pic:''},{av:'AV5TaskDescription',fld:'vTASKDESCRIPTION',pic:''},{ctrl:'vTASKSTATUS'},{av:'AV8TaskStatus',fld:'vTASKSTATUS',pic:'ZZZ9'}],[]];
   this.EvtParms["START"] = [[{av:'AV6TaskId',fld:'vTASKID',pic:'ZZZ9',hsh:true}],[{av:'AV7TaskName',fld:'vTASKNAME',pic:''},{av:'AV5TaskDescription',fld:'vTASKDESCRIPTION',pic:''},{ctrl:'vTASKSTATUS'},{av:'AV8TaskStatus',fld:'vTASKSTATUS',pic:'ZZZ9'}]];
   this.EvtParms["'CONFIRMAR'"] = [[{av:'AV7TaskName',fld:'vTASKNAME',pic:''},{av:'AV5TaskDescription',fld:'vTASKDESCRIPTION',pic:''},{ctrl:'vTASKSTATUS'},{av:'AV8TaskStatus',fld:'vTASKSTATUS',pic:'ZZZ9'},{av:'AV6TaskId',fld:'vTASKID',pic:'ZZZ9',hsh:true}],[]];
   this.EvtParms["'CANCELAR'"] = [[],[]];
   this.Initialize( );
});
gx.wi( function() { gx.createParentObj(this.deletetask);});
