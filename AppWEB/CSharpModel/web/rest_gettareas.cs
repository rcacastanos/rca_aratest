using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class rest_gettareas : GXProcedure
   {
      public rest_gettareas( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public rest_gettareas( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( short aP0_UserId ,
                           out GXBaseCollection<SdtTasks_TasksItem> aP1_Tasks ,
                           out bool aP2_IsOK ,
                           out string aP3_ErrorMessage )
      {
         this.AV18UserId = aP0_UserId;
         this.AV19Tasks = new GXBaseCollection<SdtTasks_TasksItem>( context, "TasksItem", "ArqTest") ;
         this.AV11IsOK = false ;
         this.AV10ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP1_Tasks=this.AV19Tasks;
         aP2_IsOK=this.AV11IsOK;
         aP3_ErrorMessage=this.AV10ErrorMessage;
      }

      public string executeUdp( short aP0_UserId ,
                                out GXBaseCollection<SdtTasks_TasksItem> aP1_Tasks ,
                                out bool aP2_IsOK )
      {
         execute(aP0_UserId, out aP1_Tasks, out aP2_IsOK, out aP3_ErrorMessage);
         return AV10ErrorMessage ;
      }

      public void executeSubmit( short aP0_UserId ,
                                 out GXBaseCollection<SdtTasks_TasksItem> aP1_Tasks ,
                                 out bool aP2_IsOK ,
                                 out string aP3_ErrorMessage )
      {
         rest_gettareas objrest_gettareas;
         objrest_gettareas = new rest_gettareas();
         objrest_gettareas.AV18UserId = aP0_UserId;
         objrest_gettareas.AV19Tasks = new GXBaseCollection<SdtTasks_TasksItem>( context, "TasksItem", "ArqTest") ;
         objrest_gettareas.AV11IsOK = false ;
         objrest_gettareas.AV10ErrorMessage = "" ;
         objrest_gettareas.context.SetSubmitInitialConfig(context);
         objrest_gettareas.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objrest_gettareas);
         aP1_Tasks=this.AV19Tasks;
         aP2_IsOK=this.AV11IsOK;
         aP3_ErrorMessage=this.AV10ErrorMessage;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((rest_gettareas)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV11IsOK = false;
         AV13ServiceName = "WSTaskGetAll";
         AV8CadenaJSON = "{\"UserId\": " + StringUtil.Trim( StringUtil.Str( (decimal)(AV18UserId), 4, 0)) + "}";
         new api_rest(context ).execute(  AV13ServiceName,  AV8CadenaJSON, out  AV12ResultadoJson, out  AV9ErrorCode, out  AV10ErrorMessage) ;
         if ( AV9ErrorCode == 0 )
         {
            AV20GetTasks.FromJSonString(AV12ResultadoJson, null);
            AV19Tasks.FromJSonString(AV20GetTasks.gxTpr_Tasks.ToJSonString(false), null);
            AV11IsOK = true;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections();
         }
         ExitApp();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV19Tasks = new GXBaseCollection<SdtTasks_TasksItem>( context, "TasksItem", "ArqTest");
         AV10ErrorMessage = "";
         AV13ServiceName = "";
         AV8CadenaJSON = "";
         AV12ResultadoJson = "";
         AV20GetTasks = new SdtGetTasks(context);
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV18UserId ;
      private short AV9ErrorCode ;
      private bool AV11IsOK ;
      private string AV10ErrorMessage ;
      private string AV8CadenaJSON ;
      private string AV12ResultadoJson ;
      private string AV13ServiceName ;
      private GXBaseCollection<SdtTasks_TasksItem> aP1_Tasks ;
      private bool aP2_IsOK ;
      private string aP3_ErrorMessage ;
      private GXBaseCollection<SdtTasks_TasksItem> AV19Tasks ;
      private SdtGetTasks AV20GetTasks ;
   }

}
