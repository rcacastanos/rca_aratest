using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class rest_validarusuario : GXProcedure
   {
      public rest_validarusuario( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public rest_validarusuario( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( string aP0_UserName ,
                           string aP1_UserPassword ,
                           out bool aP2_IsAccess ,
                           out string aP3_ErrorMessage )
      {
         this.AV9UserName = aP0_UserName;
         this.AV10UserPassword = aP1_UserPassword;
         this.AV11IsAccess = false ;
         this.AV16ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP2_IsAccess=this.AV11IsAccess;
         aP3_ErrorMessage=this.AV16ErrorMessage;
      }

      public string executeUdp( string aP0_UserName ,
                                string aP1_UserPassword ,
                                out bool aP2_IsAccess )
      {
         execute(aP0_UserName, aP1_UserPassword, out aP2_IsAccess, out aP3_ErrorMessage);
         return AV16ErrorMessage ;
      }

      public void executeSubmit( string aP0_UserName ,
                                 string aP1_UserPassword ,
                                 out bool aP2_IsAccess ,
                                 out string aP3_ErrorMessage )
      {
         rest_validarusuario objrest_validarusuario;
         objrest_validarusuario = new rest_validarusuario();
         objrest_validarusuario.AV9UserName = aP0_UserName;
         objrest_validarusuario.AV10UserPassword = aP1_UserPassword;
         objrest_validarusuario.AV11IsAccess = false ;
         objrest_validarusuario.AV16ErrorMessage = "" ;
         objrest_validarusuario.context.SetSubmitInitialConfig(context);
         objrest_validarusuario.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objrest_validarusuario);
         aP2_IsAccess=this.AV11IsAccess;
         aP3_ErrorMessage=this.AV16ErrorMessage;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((rest_validarusuario)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV17ServiceName = "WSValidateUser";
         AV12CadenaJSON = "{\"UserNickName\": \"" + StringUtil.Trim( AV9UserName) + "\", \"UserPassword\":\"" + AV10UserPassword + "\" }";
         new api_rest(context ).execute(  AV17ServiceName,  AV12CadenaJSON, out  AV14ResultadoJson, out  AV15ErrorCode, out  AV16ErrorMessage) ;
         AV18RESPONSE_ValidateUser.FromJSonString(AV14ResultadoJson, null);
         if ( AV18RESPONSE_ValidateUser.gxTpr_Isaccess )
         {
            AV11IsAccess = true;
            AV19WebSession.Set("UserId", StringUtil.Str( AV18RESPONSE_ValidateUser.gxTpr_Userid, 10, 5));
         }
         else
         {
            AV16ErrorMessage = "USUARIO NO ENCONTRADO";
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections();
         }
         ExitApp();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV16ErrorMessage = "";
         AV17ServiceName = "";
         AV12CadenaJSON = "";
         AV14ResultadoJson = "";
         AV18RESPONSE_ValidateUser = new SdtRESPONSE_ValidateUser(context);
         AV19WebSession = context.GetSession();
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV15ErrorCode ;
      private bool AV11IsAccess ;
      private string AV16ErrorMessage ;
      private string AV12CadenaJSON ;
      private string AV14ResultadoJson ;
      private string AV9UserName ;
      private string AV10UserPassword ;
      private string AV17ServiceName ;
      private bool aP2_IsAccess ;
      private string aP3_ErrorMessage ;
      private IGxSession AV19WebSession ;
      private SdtRESPONSE_ValidateUser AV18RESPONSE_ValidateUser ;
   }

}
