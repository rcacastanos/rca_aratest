using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class tasklist : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public tasklist( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public tasklist( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavCtltaskstatus = new GXCombobox();
      }

      protected void INITWEB( )
      {
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid1") == 0 )
            {
               nRC_GXsfl_19 = (int)(NumberUtil.Val( GetPar( "nRC_GXsfl_19"), "."));
               nGXsfl_19_idx = (int)(NumberUtil.Val( GetPar( "nGXsfl_19_idx"), "."));
               sGXsfl_19_idx = GetPar( "sGXsfl_19_idx");
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid1_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid1") == 0 )
            {
               AV9UserId = (short)(NumberUtil.Val( GetPar( "UserId"), "."));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV5Tasks);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid1_refresh( AV9UserId, AV5Tasks) ;
               AddString( context.getJSONResponse( )) ;
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("rwdmasterpage", "GeneXus.Programs.rwdmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "no-store");
               }
               if ( ! context.WillRedirect( ) )
               {
                  AddString( context.getJSONResponse( )) ;
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA0A2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START0A2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 1810380), false, true);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 1810380), false, true);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 1810380), false, true);
         context.AddJavascriptSource("gxcfg.js", "?2022127117548", false, true);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 1810380), false, true);
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 1810380), false, true);
         context.AddJavascriptSource("calendar-es.js", "?"+context.GetBuildNumber( 1810380), false, true);
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle += "-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle += " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" autocomplete=\"off\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("tasklist.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" title=\"submit\" style=\"display:none\" disabled>") ;
         AssignProp("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_vTASKS", GetSecureSignedToken( "", AV5Tasks, context));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSERID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9UserId), "ZZZ9"), context));
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "Tasks", AV5Tasks);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("Tasks", AV5Tasks);
         }
         GxWebStd.gx_hidden_field( context, "gxhash_Tasks", GetSecureSignedToken( "", AV5Tasks, context));
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_19", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_19), 8, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTASKS", AV5Tasks);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTASKS", AV5Tasks);
         }
         GxWebStd.gx_hidden_field( context, "gxhash_vTASKS", GetSecureSignedToken( "", AV5Tasks, context));
         GxWebStd.gx_hidden_field( context, "vUSERID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9UserId), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSERID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9UserId), "ZZZ9"), context));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendSecurityToken((string)(sPrefix));
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE0A2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT0A2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override string GetSelfLink( )
      {
         return formatLink("tasklist.aspx")  ;
      }

      public override string GetPgmname( )
      {
         return "TaskList" ;
      }

      public override string GetPgmdesc( )
      {
         return "Lista de Tareas" ;
      }

      protected void WB0A0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-gx-base-lib=\"bootstrapv3\""+" "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "BIENVENIDOS", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "BigTitle", 0, "", 1, 1, 0, 0, "HLP_TaskList.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroup1_Internalname, "TAREAS", 1, 0, "px", 0, "px", "GroupTitle", "", "HLP_TaskList.htm");
            /* Div Control */
            GxWebStd.gx_div_start( context, divGroup1table_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTable1_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttAgregartarea_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(19), 2, 0)+","+"null"+");", "Agregar Tarea", bttAgregartarea_Jsonclick, 7, "Agregar Tarea", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"e110a1_client"+"'", TempTags, "", 2, "HLP_TaskList.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /*  Grid Control  */
            Grid1Container.SetWrapped(nGXWrapped);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"Grid1Container"+"DivS\" data-gxgridid=\"19\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid1_Internalname, subGrid1_Internalname, "", "WorkWith", 0, "", "", 1, 2, sStyleString, "", "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid1_Backcolorstyle == 0 )
               {
                  subGrid1_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid1_Class) > 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Title";
                  }
               }
               else
               {
                  subGrid1_Titlebackstyle = 1;
                  if ( subGrid1_Backcolorstyle == 1 )
                  {
                     subGrid1_Titlebackcolor = subGrid1_Allbackcolor;
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Image"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Image"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Id") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Nombre") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Descripci�n") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Fecha de Creaci�n") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Estatus") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               Grid1Container.AddObjectProperty("GridName", "Grid1");
            }
            else
            {
               Grid1Container.AddObjectProperty("GridName", "Grid1");
               Grid1Container.AddObjectProperty("Header", subGrid1_Header);
               Grid1Container.AddObjectProperty("Class", "WorkWith");
               Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("CmpContext", "");
               Grid1Container.AddObjectProperty("InMasterPage", "false");
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", context.convertURL( AV6Editar));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", context.convertURL( AV12Eliminar));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtltaskid_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtltaskname_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtltaskdescription_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtltaskcreatedate_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavCtltaskstatus.Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Container.AddObjectProperty("Selectedindex", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Selectedindex), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowselection), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Selectioncolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowhovering), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Hoveringcolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowcollapsing), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 19 )
         {
            wbEnd = 0;
            nRC_GXsfl_19 = (int)(nGXsfl_19_idx-1);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               AV15GXV1 = nGXsfl_19_idx;
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"Grid1Container"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid1", Grid1Container);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid1ContainerData", Grid1Container.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid1ContainerData"+"V", Grid1Container.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Grid1ContainerData"+"V"+"\" value='"+Grid1Container.GridValuesHidden()+"'/>") ;
               }
            }
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</fieldset>") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
         }
         if ( wbEnd == 19 )
         {
            wbEnd = 0;
            if ( isFullAjaxMode( ) )
            {
               if ( Grid1Container.GetWrapped() == 1 )
               {
                  context.WriteHtmlText( "</table>") ;
                  context.WriteHtmlText( "</div>") ;
               }
               else
               {
                  AV15GXV1 = nGXsfl_19_idx;
                  sStyleString = "";
                  context.WriteHtmlText( "<div id=\""+"Grid1Container"+"Div\" "+sStyleString+">"+"</div>") ;
                  context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid1", Grid1Container);
                  if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
                  {
                     GxWebStd.gx_hidden_field( context, "Grid1ContainerData", Grid1Container.ToJavascriptSource());
                  }
                  if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
                  {
                     GxWebStd.gx_hidden_field( context, "Grid1ContainerData"+"V", Grid1Container.GridValuesHidden());
                  }
                  else
                  {
                     context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Grid1ContainerData"+"V"+"\" value='"+Grid1Container.GridValuesHidden()+"'/>") ;
                  }
               }
            }
         }
         wbLoad = true;
      }

      protected void START0A2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            if ( context.ExposeMetadata( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 17_0_6-154974", 0) ;
            }
            Form.Meta.addItem("description", "Lista de Tareas", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP0A0( ) ;
      }

      protected void WS0A2( )
      {
         START0A2( ) ;
         EVT0A2( ) ;
      }

      protected void EVT0A2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "VEDITAR.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 15), "VELIMINAR.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 10), "GRID1.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "VEDITAR.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 15), "VELIMINAR.CLICK") == 0 ) )
                           {
                              nGXsfl_19_idx = (int)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_19_idx = StringUtil.PadL( StringUtil.LTrimStr( (decimal)(nGXsfl_19_idx), 4, 0), 4, "0");
                              SubsflControlProps_192( ) ;
                              AV15GXV1 = nGXsfl_19_idx;
                              if ( ( AV5Tasks.Count >= AV15GXV1 ) && ( AV15GXV1 > 0 ) )
                              {
                                 AV5Tasks.CurrentItem = ((SdtTasks_TasksItem)AV5Tasks.Item(AV15GXV1));
                                 AV6Editar = cgiGet( edtavEditar_Internalname);
                                 AssignProp("", false, edtavEditar_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV6Editar)) ? AV21Editar_GXI : context.convertURL( context.PathToRelativeUrl( AV6Editar))), !bGXsfl_19_Refreshing);
                                 AssignProp("", false, edtavEditar_Internalname, "SrcSet", context.GetImageSrcSet( AV6Editar), true);
                                 AV12Eliminar = cgiGet( edtavEliminar_Internalname);
                                 AssignProp("", false, edtavEliminar_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV12Eliminar)) ? AV22Eliminar_GXI : context.convertURL( context.PathToRelativeUrl( AV12Eliminar))), !bGXsfl_19_Refreshing);
                                 AssignProp("", false, edtavEliminar_Internalname, "SrcSet", context.GetImageSrcSet( AV12Eliminar), true);
                              }
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: Start */
                                    E120A2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VEDITAR.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    E130A2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VELIMINAR.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    E140A2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: Refresh */
                                    E150A2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID1.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    E160A2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                    /* No code required for Cancel button. It is implemented as the Reset button. */
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE0A2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA0A2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", (short)(context.GetHttpSecure( )));
            }
            GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            init_web_controls( ) ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid1_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_192( ) ;
         while ( nGXsfl_19_idx <= nRC_GXsfl_19 )
         {
            sendrow_192( ) ;
            nGXsfl_19_idx = ((subGrid1_Islastpage==1)&&(nGXsfl_19_idx+1>subGrid1_fnc_Recordsperpage( )) ? 1 : nGXsfl_19_idx+1);
            sGXsfl_19_idx = StringUtil.PadL( StringUtil.LTrimStr( (decimal)(nGXsfl_19_idx), 4, 0), 4, "0");
            SubsflControlProps_192( ) ;
         }
         AddString( context.httpAjaxContext.getJSONContainerResponse( Grid1Container)) ;
         /* End function gxnrGrid1_newrow */
      }

      protected void gxgrGrid1_refresh( short AV9UserId ,
                                        GXBaseCollection<SdtTasks_TasksItem> AV5Tasks )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         /* Execute user event: Refresh */
         E150A2 ();
         GRID1_nCurrentRecord = 0;
         RF0A2( ) ;
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         send_integrity_footer_hashes( ) ;
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         /* End function gxgrGrid1_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void clear_multi_value_controls( )
      {
         if ( context.isAjaxRequest( ) )
         {
            dynload_actions( ) ;
            before_start_formulas( ) ;
         }
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF0A2( ) ;
         if ( isFullAjaxMode( ) )
         {
            send_integrity_footer_hashes( ) ;
         }
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavCtltaskid_Enabled = 0;
         AssignProp("", false, edtavCtltaskid_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtavCtltaskid_Enabled), 5, 0), !bGXsfl_19_Refreshing);
         edtavCtltaskname_Enabled = 0;
         AssignProp("", false, edtavCtltaskname_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtavCtltaskname_Enabled), 5, 0), !bGXsfl_19_Refreshing);
         edtavCtltaskdescription_Enabled = 0;
         AssignProp("", false, edtavCtltaskdescription_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtavCtltaskdescription_Enabled), 5, 0), !bGXsfl_19_Refreshing);
         edtavCtltaskcreatedate_Enabled = 0;
         AssignProp("", false, edtavCtltaskcreatedate_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtavCtltaskcreatedate_Enabled), 5, 0), !bGXsfl_19_Refreshing);
         cmbavCtltaskstatus.Enabled = 0;
         AssignProp("", false, cmbavCtltaskstatus_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(cmbavCtltaskstatus.Enabled), 5, 0), !bGXsfl_19_Refreshing);
      }

      protected void RF0A2( )
      {
         initialize_formulas( ) ;
         clear_multi_value_controls( ) ;
         if ( isAjaxCallMode( ) )
         {
            Grid1Container.ClearRows();
         }
         wbStart = 19;
         /* Execute user event: Refresh */
         E150A2 ();
         nGXsfl_19_idx = 1;
         sGXsfl_19_idx = StringUtil.PadL( StringUtil.LTrimStr( (decimal)(nGXsfl_19_idx), 4, 0), 4, "0");
         SubsflControlProps_192( ) ;
         bGXsfl_19_Refreshing = true;
         Grid1Container.AddObjectProperty("GridName", "Grid1");
         Grid1Container.AddObjectProperty("CmpContext", "");
         Grid1Container.AddObjectProperty("InMasterPage", "false");
         Grid1Container.AddObjectProperty("Class", "WorkWith");
         Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
         Grid1Container.PageSize = subGrid1_fnc_Recordsperpage( );
         gxdyncontrolsrefreshing = true;
         fix_multi_value_controls( ) ;
         gxdyncontrolsrefreshing = false;
         if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_192( ) ;
            E160A2 ();
            wbEnd = 19;
            WB0A0( ) ;
         }
         bGXsfl_19_Refreshing = true;
      }

      protected void send_integrity_lvl_hashes0A2( )
      {
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTASKS", AV5Tasks);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTASKS", AV5Tasks);
         }
         GxWebStd.gx_hidden_field( context, "gxhash_vTASKS", GetSecureSignedToken( "", AV5Tasks, context));
         GxWebStd.gx_hidden_field( context, "vUSERID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9UserId), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSERID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9UserId), "ZZZ9"), context));
      }

      protected int subGrid1_fnc_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_fnc_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_fnc_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_fnc_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void before_start_formulas( )
      {
         context.Gx_err = 0;
         edtavCtltaskid_Enabled = 0;
         AssignProp("", false, edtavCtltaskid_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtavCtltaskid_Enabled), 5, 0), !bGXsfl_19_Refreshing);
         edtavCtltaskname_Enabled = 0;
         AssignProp("", false, edtavCtltaskname_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtavCtltaskname_Enabled), 5, 0), !bGXsfl_19_Refreshing);
         edtavCtltaskdescription_Enabled = 0;
         AssignProp("", false, edtavCtltaskdescription_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtavCtltaskdescription_Enabled), 5, 0), !bGXsfl_19_Refreshing);
         edtavCtltaskcreatedate_Enabled = 0;
         AssignProp("", false, edtavCtltaskcreatedate_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtavCtltaskcreatedate_Enabled), 5, 0), !bGXsfl_19_Refreshing);
         cmbavCtltaskstatus.Enabled = 0;
         AssignProp("", false, cmbavCtltaskstatus_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(cmbavCtltaskstatus.Enabled), 5, 0), !bGXsfl_19_Refreshing);
         fix_multi_value_controls( ) ;
      }

      protected void STRUP0A0( )
      {
         /* Before Start, stand alone formulas. */
         before_start_formulas( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: Start */
         E120A2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "Tasks"), AV5Tasks);
            /* Read saved values. */
            nRC_GXsfl_19 = (int)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_19"), ",", "."));
            nRC_GXsfl_19 = (int)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_19"), ",", "."));
            nGXsfl_19_fel_idx = 0;
            while ( nGXsfl_19_fel_idx < nRC_GXsfl_19 )
            {
               nGXsfl_19_fel_idx = ((subGrid1_Islastpage==1)&&(nGXsfl_19_fel_idx+1>subGrid1_fnc_Recordsperpage( )) ? 1 : nGXsfl_19_fel_idx+1);
               sGXsfl_19_fel_idx = StringUtil.PadL( StringUtil.LTrimStr( (decimal)(nGXsfl_19_fel_idx), 4, 0), 4, "0");
               SubsflControlProps_fel_192( ) ;
               AV15GXV1 = nGXsfl_19_fel_idx;
               if ( ( AV5Tasks.Count >= AV15GXV1 ) && ( AV15GXV1 > 0 ) )
               {
                  AV5Tasks.CurrentItem = ((SdtTasks_TasksItem)AV5Tasks.Item(AV15GXV1));
                  AV6Editar = cgiGet( edtavEditar_Internalname);
                  AV12Eliminar = cgiGet( edtavEliminar_Internalname);
               }
            }
            if ( nGXsfl_19_fel_idx == 0 )
            {
               nGXsfl_19_idx = 1;
               sGXsfl_19_idx = StringUtil.PadL( StringUtil.LTrimStr( (decimal)(nGXsfl_19_idx), 4, 0), 4, "0");
               SubsflControlProps_192( ) ;
            }
            nGXsfl_19_fel_idx = 1;
            /* Read variables values. */
            /* Read subfile selected row values. */
            nGXsfl_19_idx = (int)(context.localUtil.CToN( cgiGet( subGrid1_Internalname+"_ROW"), ",", "."));
            sGXsfl_19_idx = StringUtil.PadL( StringUtil.LTrimStr( (decimal)(nGXsfl_19_idx), 4, 0), 4, "0");
            SubsflControlProps_192( ) ;
            AV15GXV1 = nGXsfl_19_idx;
            if ( nGXsfl_19_idx > 0 )
            {
               AV15GXV1 = nGXsfl_19_idx;
               if ( ( AV5Tasks.Count >= AV15GXV1 ) && ( AV15GXV1 > 0 ) )
               {
                  AV5Tasks.CurrentItem = ((SdtTasks_TasksItem)AV5Tasks.Item(AV15GXV1));
                  AV6Editar = cgiGet( edtavEditar_Internalname);
                  AV12Eliminar = cgiGet( edtavEliminar_Internalname);
               }
               if ( AV5Tasks.Count >= AV15GXV1 )
               {
                  AV5Tasks.CurrentItem = ((SdtTasks_TasksItem)AV5Tasks.Item(AV15GXV1));
               }
            }
            /* Read hidden variables. */
            GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: Start */
         E120A2 ();
         if (returnInSub) return;
      }

      protected void E120A2( )
      {
         /* Start Routine */
         returnInSub = false;
         AV9UserId = (short)(NumberUtil.Val( AV8WebSession.Get("UserId"), "."));
         AssignAttri("", false, "AV9UserId", StringUtil.LTrimStr( (decimal)(AV9UserId), 4, 0));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSERID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9UserId), "ZZZ9"), context));
      }

      protected void E130A2( )
      {
         AV15GXV1 = nGXsfl_19_idx;
         if ( AV5Tasks.Count >= AV15GXV1 )
         {
            AV5Tasks.CurrentItem = ((SdtTasks_TasksItem)AV5Tasks.Item(AV15GXV1));
         }
         /* Editar_Click Routine */
         returnInSub = false;
         AV7TastkId = ((SdtTasks_TasksItem)(AV5Tasks.CurrentItem)).gxTpr_Taskid;
         CallWebObject(formatLink("updatetask.aspx", new object[] {UrlEncode(StringUtil.LTrimStr(AV7TastkId,4,0))}, new string[] {"TaskId"}) );
         context.wjLocDisableFrm = 1;
      }

      protected void E140A2( )
      {
         AV15GXV1 = nGXsfl_19_idx;
         if ( AV5Tasks.Count >= AV15GXV1 )
         {
            AV5Tasks.CurrentItem = ((SdtTasks_TasksItem)AV5Tasks.Item(AV15GXV1));
         }
         /* Eliminar_Click Routine */
         returnInSub = false;
         AV7TastkId = ((SdtTasks_TasksItem)(AV5Tasks.CurrentItem)).gxTpr_Taskid;
         CallWebObject(formatLink("deletetask.aspx", new object[] {UrlEncode(StringUtil.LTrimStr(AV7TastkId,4,0))}, new string[] {"TaskId"}) );
         context.wjLocDisableFrm = 1;
      }

      protected void E150A2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         returnInSub = false;
         new rest_gettareas(context ).execute(  AV9UserId, out  AV5Tasks, out  AV10IsOK, out  AV11ErrorMessage) ;
         gx_BV19 = true;
         /*  Sending Event outputs  */
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5Tasks", AV5Tasks);
      }

      private void E160A2( )
      {
         /* Grid1_Load Routine */
         returnInSub = false;
         AV15GXV1 = 1;
         while ( AV15GXV1 <= AV5Tasks.Count )
         {
            AV5Tasks.CurrentItem = ((SdtTasks_TasksItem)AV5Tasks.Item(AV15GXV1));
            AV6Editar = context.GetImagePath( "b1dff4a1-a85a-4c94-af55-98e911105ce2", "", context.GetTheme( ));
            AssignAttri("", false, edtavEditar_Internalname, AV6Editar);
            AV21Editar_GXI = GXDbFile.PathToUrl( context.GetImagePath( "b1dff4a1-a85a-4c94-af55-98e911105ce2", "", context.GetTheme( )));
            AV12Eliminar = context.GetImagePath( "479b77e5-f0d1-408e-b83f-da52e6ef20be", "", context.GetTheme( ));
            AssignAttri("", false, edtavEliminar_Internalname, AV12Eliminar);
            AV22Eliminar_GXI = GXDbFile.PathToUrl( context.GetImagePath( "479b77e5-f0d1-408e-b83f-da52e6ef20be", "", context.GetTheme( )));
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 19;
            }
            sendrow_192( ) ;
            if ( isFullAjaxMode( ) && ! bGXsfl_19_Refreshing )
            {
               context.DoAjaxLoad(19, Grid1Row);
            }
            AV15GXV1 = (int)(AV15GXV1+1);
         }
         /*  Sending Event outputs  */
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override string getresponse( string sGXDynURL )
      {
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA0A2( ) ;
         WS0A2( ) ;
         WE0A2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( string sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ));
         bool outputEnabled = isOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((string)Form.Jscriptsrc.Item(idxLst))), "?20221271175427", true, true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.spa.js", "?"+GetCacheInvalidationToken( ), false, true);
         context.AddJavascriptSource("tasklist.js", "?20221271175427", false, true);
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_192( )
      {
         edtavEditar_Internalname = "vEDITAR_"+sGXsfl_19_idx;
         edtavEliminar_Internalname = "vELIMINAR_"+sGXsfl_19_idx;
         edtavCtltaskid_Internalname = "CTLTASKID_"+sGXsfl_19_idx;
         edtavCtltaskname_Internalname = "CTLTASKNAME_"+sGXsfl_19_idx;
         edtavCtltaskdescription_Internalname = "CTLTASKDESCRIPTION_"+sGXsfl_19_idx;
         edtavCtltaskcreatedate_Internalname = "CTLTASKCREATEDATE_"+sGXsfl_19_idx;
         cmbavCtltaskstatus_Internalname = "CTLTASKSTATUS_"+sGXsfl_19_idx;
      }

      protected void SubsflControlProps_fel_192( )
      {
         edtavEditar_Internalname = "vEDITAR_"+sGXsfl_19_fel_idx;
         edtavEliminar_Internalname = "vELIMINAR_"+sGXsfl_19_fel_idx;
         edtavCtltaskid_Internalname = "CTLTASKID_"+sGXsfl_19_fel_idx;
         edtavCtltaskname_Internalname = "CTLTASKNAME_"+sGXsfl_19_fel_idx;
         edtavCtltaskdescription_Internalname = "CTLTASKDESCRIPTION_"+sGXsfl_19_fel_idx;
         edtavCtltaskcreatedate_Internalname = "CTLTASKCREATEDATE_"+sGXsfl_19_fel_idx;
         cmbavCtltaskstatus_Internalname = "CTLTASKSTATUS_"+sGXsfl_19_fel_idx;
      }

      protected void sendrow_192( )
      {
         SubsflControlProps_192( ) ;
         WB0A0( ) ;
         Grid1Row = GXWebRow.GetNew(context,Grid1Container);
         if ( subGrid1_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid1_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Odd";
            }
         }
         else if ( subGrid1_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid1_Backstyle = 0;
            subGrid1_Backcolor = subGrid1_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Uniform";
            }
         }
         else if ( subGrid1_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid1_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Odd";
            }
            subGrid1_Backcolor = (int)(0x0);
         }
         else if ( subGrid1_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid1_Backstyle = 1;
            if ( ((int)((nGXsfl_19_idx) % (2))) == 0 )
            {
               subGrid1_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Even";
               }
            }
            else
            {
               subGrid1_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Odd";
               }
            }
         }
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+"WorkWith"+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_19_idx+"\">") ;
         }
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
         }
         /* Active Bitmap Variable */
         TempTags = " " + ((edtavEditar_Enabled!=0)&&(edtavEditar_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 20,'',false,'',19)\"" : " ");
         ClassString = "Image";
         StyleString = "";
         AV6Editar_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV6Editar))&&String.IsNullOrEmpty(StringUtil.RTrim( AV21Editar_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV6Editar)));
         sImgUrl = (String.IsNullOrEmpty(StringUtil.RTrim( AV6Editar)) ? AV21Editar_GXI : context.PathToRelativeUrl( AV6Editar));
         Grid1Row.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(string)edtavEditar_Internalname,(string)sImgUrl,(string)"",(string)"",(string)"",context.GetTheme( ),(short)-1,(short)1,(string)"",(string)"",(short)0,(short)-1,(short)0,(string)"px",(short)0,(string)"px",(short)0,(short)0,(short)5,(string)edtavEditar_Jsonclick,"'"+""+"'"+",false,"+"'"+"EVEDITAR.CLICK."+sGXsfl_19_idx+"'",(string)StyleString,(string)ClassString,(string)"",(string)"",(string)"",(string)"",(string)""+TempTags,(string)"",(string)"",(short)1,(bool)AV6Editar_IsBlob,(bool)false,context.GetImageSrcSet( sImgUrl)});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
         }
         /* Active Bitmap Variable */
         TempTags = " " + ((edtavEliminar_Enabled!=0)&&(edtavEliminar_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 21,'',false,'',19)\"" : " ");
         ClassString = "Image";
         StyleString = "";
         AV12Eliminar_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV12Eliminar))&&String.IsNullOrEmpty(StringUtil.RTrim( AV22Eliminar_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV12Eliminar)));
         sImgUrl = (String.IsNullOrEmpty(StringUtil.RTrim( AV12Eliminar)) ? AV22Eliminar_GXI : context.PathToRelativeUrl( AV12Eliminar));
         Grid1Row.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(string)edtavEliminar_Internalname,(string)sImgUrl,(string)"",(string)"",(string)"",context.GetTheme( ),(short)-1,(short)1,(string)"",(string)"",(short)0,(short)-1,(short)0,(string)"px",(short)0,(string)"px",(short)0,(short)0,(short)5,(string)edtavEliminar_Jsonclick,"'"+""+"'"+",false,"+"'"+"EVELIMINAR.CLICK."+sGXsfl_19_idx+"'",(string)StyleString,(string)ClassString,(string)"",(string)"",(string)"",(string)"",(string)""+TempTags,(string)"",(string)"",(short)1,(bool)AV12Eliminar_IsBlob,(bool)false,context.GetImageSrcSet( sImgUrl)});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(string)edtavCtltaskid_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtTasks_TasksItem)AV5Tasks.Item(AV15GXV1)).gxTpr_Taskid), 4, 0, ",", "")),((edtavCtltaskid_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtTasks_TasksItem)AV5Tasks.Item(AV15GXV1)).gxTpr_Taskid), "ZZZ9")) : context.localUtil.Format( (decimal)(((SdtTasks_TasksItem)AV5Tasks.Item(AV15GXV1)).gxTpr_Taskid), "ZZZ9")),(string)"",(string)"'"+""+"'"+",false,"+"'"+""+"'",(string)"",(string)"",(string)"",(string)"",(string)edtavCtltaskid_Jsonclick,(short)0,(string)"Attribute",(string)"",(string)ROClassString,(string)"",(string)"",(short)-1,(int)edtavCtltaskid_Enabled,(short)0,(string)"number",(string)"1",(short)0,(string)"px",(short)17,(string)"px",(short)4,(short)0,(short)0,(short)19,(short)1,(short)-1,(short)0,(bool)true,(string)"",(string)"right",(bool)false,(string)""});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(string)edtavCtltaskname_Internalname,((SdtTasks_TasksItem)AV5Tasks.Item(AV15GXV1)).gxTpr_Taskname,(string)"",(string)"",(string)"'"+""+"'"+",false,"+"'"+""+"'",(string)"",(string)"",(string)"",(string)"",(string)edtavCtltaskname_Jsonclick,(short)0,(string)"Attribute",(string)"",(string)ROClassString,(string)"",(string)"",(short)-1,(int)edtavCtltaskname_Enabled,(short)0,(string)"text",(string)"",(short)0,(string)"px",(short)17,(string)"px",(short)40,(short)0,(short)0,(short)19,(short)1,(short)-1,(short)-1,(bool)true,(string)"",(string)"left",(bool)true,(string)""});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(string)edtavCtltaskdescription_Internalname,((SdtTasks_TasksItem)AV5Tasks.Item(AV15GXV1)).gxTpr_Taskdescription,((SdtTasks_TasksItem)AV5Tasks.Item(AV15GXV1)).gxTpr_Taskdescription,(string)"",(string)"'"+""+"'"+",false,"+"'"+""+"'",(string)"",(string)"",(string)"",(string)"",(string)edtavCtltaskdescription_Jsonclick,(short)0,(string)"Attribute",(string)"",(string)ROClassString,(string)"",(string)"",(short)-1,(int)edtavCtltaskdescription_Enabled,(short)0,(string)"text",(string)"",(short)0,(string)"px",(short)17,(string)"px",(int)2097152,(short)0,(short)0,(short)19,(short)1,(short)0,(short)-1,(bool)true,(string)"",(string)"left",(bool)false,(string)""});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(string)edtavCtltaskcreatedate_Internalname,context.localUtil.TToC( ((SdtTasks_TasksItem)AV5Tasks.Item(AV15GXV1)).gxTpr_Taskcreatedate, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( ((SdtTasks_TasksItem)AV5Tasks.Item(AV15GXV1)).gxTpr_Taskcreatedate, "99/99/99 99:99"),(string)"",(string)"'"+""+"'"+",false,"+"'"+""+"'",(string)"",(string)"",(string)"",(string)"",(string)edtavCtltaskcreatedate_Jsonclick,(short)0,(string)"Attribute",(string)"",(string)ROClassString,(string)"",(string)"",(short)-1,(int)edtavCtltaskcreatedate_Enabled,(short)0,(string)"text",(string)"",(short)0,(string)"px",(short)17,(string)"px",(short)14,(short)0,(short)0,(short)19,(short)1,(short)-1,(short)0,(bool)true,(string)"",(string)"right",(bool)false,(string)""});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         if ( ( cmbavCtltaskstatus.ItemCount == 0 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "CTLTASKSTATUS_" + sGXsfl_19_idx;
            cmbavCtltaskstatus.Name = GXCCtl;
            cmbavCtltaskstatus.WebTags = "";
            cmbavCtltaskstatus.addItem("1", "Pendiente", 0);
            cmbavCtltaskstatus.addItem("2", "Completada", 0);
            if ( cmbavCtltaskstatus.ItemCount > 0 )
            {
               if ( ( AV15GXV1 > 0 ) && ( AV5Tasks.Count >= AV15GXV1 ) && (0==((SdtTasks_TasksItem)AV5Tasks.Item(AV15GXV1)).gxTpr_Taskstatus) )
               {
                  ((SdtTasks_TasksItem)AV5Tasks.Item(AV15GXV1)).gxTpr_Taskstatus = (short)(NumberUtil.Val( cmbavCtltaskstatus.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(((SdtTasks_TasksItem)AV5Tasks.Item(AV15GXV1)).gxTpr_Taskstatus), 4, 0))), "."));
               }
            }
         }
         /* ComboBox */
         Grid1Row.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavCtltaskstatus,(string)cmbavCtltaskstatus_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(((SdtTasks_TasksItem)AV5Tasks.Item(AV15GXV1)).gxTpr_Taskstatus), 4, 0)),(short)1,(string)cmbavCtltaskstatus_Jsonclick,(short)0,(string)"'"+""+"'"+",false,"+"'"+""+"'",(string)"int",(string)"",(short)-1,cmbavCtltaskstatus.Enabled,(short)0,(short)0,(short)0,(string)"px",(short)0,(string)"px",(string)"",(string)"Attribute",(string)"",(string)"",(string)"",(string)"",(bool)true,(short)1});
         cmbavCtltaskstatus.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(((SdtTasks_TasksItem)AV5Tasks.Item(AV15GXV1)).gxTpr_Taskstatus), 4, 0));
         AssignProp("", false, cmbavCtltaskstatus_Internalname, "Values", (string)(cmbavCtltaskstatus.ToJavascriptSource()), !bGXsfl_19_Refreshing);
         send_integrity_lvl_hashes0A2( ) ;
         Grid1Container.AddRow(Grid1Row);
         nGXsfl_19_idx = ((subGrid1_Islastpage==1)&&(nGXsfl_19_idx+1>subGrid1_fnc_Recordsperpage( )) ? 1 : nGXsfl_19_idx+1);
         sGXsfl_19_idx = StringUtil.PadL( StringUtil.LTrimStr( (decimal)(nGXsfl_19_idx), 4, 0), 4, "0");
         SubsflControlProps_192( ) ;
         /* End function sendrow_192 */
      }

      protected void init_web_controls( )
      {
         GXCCtl = "CTLTASKSTATUS_" + sGXsfl_19_idx;
         cmbavCtltaskstatus.Name = GXCCtl;
         cmbavCtltaskstatus.WebTags = "";
         cmbavCtltaskstatus.addItem("1", "Pendiente", 0);
         cmbavCtltaskstatus.addItem("2", "Completada", 0);
         if ( cmbavCtltaskstatus.ItemCount > 0 )
         {
            if ( ( AV15GXV1 > 0 ) && ( AV5Tasks.Count >= AV15GXV1 ) && (0==((SdtTasks_TasksItem)AV5Tasks.Item(AV15GXV1)).gxTpr_Taskstatus) )
            {
               ((SdtTasks_TasksItem)AV5Tasks.Item(AV15GXV1)).gxTpr_Taskstatus = (short)(NumberUtil.Val( cmbavCtltaskstatus.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(((SdtTasks_TasksItem)AV5Tasks.Item(AV15GXV1)).gxTpr_Taskstatus), 4, 0))), "."));
            }
         }
         /* End function init_web_controls */
      }

      protected void init_default_properties( )
      {
         lblTextblock1_Internalname = "TEXTBLOCK1";
         bttAgregartarea_Internalname = "AGREGARTAREA";
         edtavEditar_Internalname = "vEDITAR";
         edtavEliminar_Internalname = "vELIMINAR";
         edtavCtltaskid_Internalname = "CTLTASKID";
         edtavCtltaskname_Internalname = "CTLTASKNAME";
         edtavCtltaskdescription_Internalname = "CTLTASKDESCRIPTION";
         edtavCtltaskcreatedate_Internalname = "CTLTASKCREATEDATE";
         cmbavCtltaskstatus_Internalname = "CTLTASKSTATUS";
         divTable1_Internalname = "TABLE1";
         divGroup1table_Internalname = "GROUP1TABLE";
         grpGroup1_Internalname = "GROUP1";
         divMaintable_Internalname = "MAINTABLE";
         Form.Internalname = "FORM";
         subGrid1_Internalname = "GRID1";
      }

      public override void initialize_properties( )
      {
         context.SetDefaultTheme("Carmine");
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbavCtltaskstatus_Jsonclick = "";
         edtavCtltaskcreatedate_Jsonclick = "";
         edtavCtltaskdescription_Jsonclick = "";
         edtavCtltaskname_Jsonclick = "";
         edtavCtltaskid_Jsonclick = "";
         edtavEliminar_Jsonclick = "";
         edtavEliminar_Visible = -1;
         edtavEliminar_Enabled = 1;
         edtavEditar_Jsonclick = "";
         edtavEditar_Visible = -1;
         edtavEditar_Enabled = 1;
         cmbavCtltaskstatus.Enabled = -1;
         edtavCtltaskcreatedate_Enabled = -1;
         edtavCtltaskdescription_Enabled = -1;
         edtavCtltaskname_Enabled = -1;
         edtavCtltaskid_Enabled = -1;
         subGrid1_Allowcollapsing = 0;
         subGrid1_Allowhovering = -1;
         subGrid1_Allowselection = 1;
         cmbavCtltaskstatus.Enabled = 0;
         edtavCtltaskcreatedate_Enabled = 0;
         edtavCtltaskdescription_Enabled = 0;
         edtavCtltaskname_Enabled = 0;
         edtavCtltaskid_Enabled = 0;
         subGrid1_Header = "";
         subGrid1_Class = "WorkWith";
         subGrid1_Backcolorstyle = 0;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Lista de Tareas";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID1_nFirstRecordOnPage'},{av:'GRID1_nEOF'},{av:'AV5Tasks',fld:'vTASKS',grid:19,pic:'',hsh:true},{av:'nRC_GXsfl_19',ctrl:'GRID1',prop:'GridRC',grid:19},{av:'AV9UserId',fld:'vUSERID',pic:'ZZZ9',hsh:true}]");
         setEventMetadata("REFRESH",",oparms:[{av:'AV5Tasks',fld:'vTASKS',grid:19,pic:'',hsh:true},{av:'GRID1_nFirstRecordOnPage'},{av:'nRC_GXsfl_19',ctrl:'GRID1',prop:'GridRC',grid:19}]}");
         setEventMetadata("VEDITAR.CLICK","{handler:'E130A2',iparms:[{av:'AV5Tasks',fld:'vTASKS',grid:19,pic:'',hsh:true},{av:'GRID1_nFirstRecordOnPage'},{av:'nRC_GXsfl_19',ctrl:'GRID1',prop:'GridRC',grid:19}]");
         setEventMetadata("VEDITAR.CLICK",",oparms:[]}");
         setEventMetadata("VELIMINAR.CLICK","{handler:'E140A2',iparms:[{av:'AV5Tasks',fld:'vTASKS',grid:19,pic:'',hsh:true},{av:'GRID1_nFirstRecordOnPage'},{av:'nRC_GXsfl_19',ctrl:'GRID1',prop:'GridRC',grid:19}]");
         setEventMetadata("VELIMINAR.CLICK",",oparms:[]}");
         setEventMetadata("'AGREGAR TAREA'","{handler:'E110A1',iparms:[]");
         setEventMetadata("'AGREGAR TAREA'",",oparms:[]}");
         setEventMetadata("GRID1.LOAD","{handler:'E160A2',iparms:[]");
         setEventMetadata("GRID1.LOAD",",oparms:[{av:'AV6Editar',fld:'vEDITAR',pic:''},{av:'AV12Eliminar',fld:'vELIMINAR',pic:''}]}");
         setEventMetadata("NULL","{handler:'Validv_Gxv6',iparms:[]");
         setEventMetadata("NULL",",oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections();
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV5Tasks = new GXBaseCollection<SdtTasks_TasksItem>( context, "TasksItem", "ArqTest");
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         lblTextblock1_Jsonclick = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttAgregartarea_Jsonclick = "";
         Grid1Container = new GXWebGrid( context);
         sStyleString = "";
         subGrid1_Linesclass = "";
         Grid1Column = new GXWebColumn();
         AV6Editar = "";
         AV12Eliminar = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV21Editar_GXI = "";
         AV22Eliminar_GXI = "";
         AV8WebSession = context.GetSession();
         AV11ErrorMessage = "";
         Grid1Row = new GXWebRow();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sImgUrl = "";
         ROClassString = "";
         GXCCtl = "";
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavCtltaskid_Enabled = 0;
         edtavCtltaskname_Enabled = 0;
         edtavCtltaskdescription_Enabled = 0;
         edtavCtltaskcreatedate_Enabled = 0;
         cmbavCtltaskstatus.Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short AV9UserId ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short subGrid1_Backcolorstyle ;
      private short subGrid1_Titlebackstyle ;
      private short subGrid1_Allowselection ;
      private short subGrid1_Allowhovering ;
      private short subGrid1_Allowcollapsing ;
      private short subGrid1_Collapsed ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV7TastkId ;
      private short GRID1_nEOF ;
      private short nGXWrapped ;
      private short subGrid1_Backstyle ;
      private int nRC_GXsfl_19 ;
      private int nGXsfl_19_idx=1 ;
      private int subGrid1_Titlebackcolor ;
      private int subGrid1_Allbackcolor ;
      private int edtavCtltaskid_Enabled ;
      private int edtavCtltaskname_Enabled ;
      private int edtavCtltaskdescription_Enabled ;
      private int edtavCtltaskcreatedate_Enabled ;
      private int subGrid1_Selectedindex ;
      private int subGrid1_Selectioncolor ;
      private int subGrid1_Hoveringcolor ;
      private int AV15GXV1 ;
      private int subGrid1_Islastpage ;
      private int nGXsfl_19_fel_idx=1 ;
      private int idxLst ;
      private int subGrid1_Backcolor ;
      private int edtavEditar_Enabled ;
      private int edtavEditar_Visible ;
      private int edtavEliminar_Enabled ;
      private int edtavEliminar_Visible ;
      private long GRID1_nCurrentRecord ;
      private long GRID1_nFirstRecordOnPage ;
      private string gxfirstwebparm ;
      private string gxfirstwebparm_bkp ;
      private string sGXsfl_19_idx="0001" ;
      private string sDynURL ;
      private string FormProcess ;
      private string bodyStyle ;
      private string GXKey ;
      private string GX_FocusControl ;
      private string sPrefix ;
      private string divMaintable_Internalname ;
      private string lblTextblock1_Internalname ;
      private string lblTextblock1_Jsonclick ;
      private string grpGroup1_Internalname ;
      private string divGroup1table_Internalname ;
      private string divTable1_Internalname ;
      private string TempTags ;
      private string ClassString ;
      private string StyleString ;
      private string bttAgregartarea_Internalname ;
      private string bttAgregartarea_Jsonclick ;
      private string sStyleString ;
      private string subGrid1_Internalname ;
      private string subGrid1_Class ;
      private string subGrid1_Linesclass ;
      private string subGrid1_Header ;
      private string sEvt ;
      private string EvtGridId ;
      private string EvtRowId ;
      private string sEvtType ;
      private string edtavEditar_Internalname ;
      private string edtavEliminar_Internalname ;
      private string edtavCtltaskid_Internalname ;
      private string edtavCtltaskname_Internalname ;
      private string edtavCtltaskdescription_Internalname ;
      private string edtavCtltaskcreatedate_Internalname ;
      private string cmbavCtltaskstatus_Internalname ;
      private string sGXsfl_19_fel_idx="0001" ;
      private string sImgUrl ;
      private string edtavEditar_Jsonclick ;
      private string edtavEliminar_Jsonclick ;
      private string ROClassString ;
      private string edtavCtltaskid_Jsonclick ;
      private string edtavCtltaskname_Jsonclick ;
      private string edtavCtltaskdescription_Jsonclick ;
      private string edtavCtltaskcreatedate_Jsonclick ;
      private string GXCCtl ;
      private string cmbavCtltaskstatus_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool bGXsfl_19_Refreshing=false ;
      private bool gxdyncontrolsrefreshing ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV10IsOK ;
      private bool gx_BV19 ;
      private bool AV6Editar_IsBlob ;
      private bool AV12Eliminar_IsBlob ;
      private string AV11ErrorMessage ;
      private string AV21Editar_GXI ;
      private string AV22Eliminar_GXI ;
      private string AV6Editar ;
      private string AV12Eliminar ;
      private IGxSession AV8WebSession ;
      private GXWebGrid Grid1Container ;
      private GXWebRow Grid1Row ;
      private GXWebColumn Grid1Column ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavCtltaskstatus ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXBaseCollection<SdtTasks_TasksItem> AV5Tasks ;
      private GXWebForm Form ;
   }

}
