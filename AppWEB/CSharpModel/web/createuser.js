gx.evt.autoSkip = false;
gx.define('createuser', false, function () {
   this.ServerClass =  "createuser" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.ServerFullClass =  "createuser.aspx" ;
   this.setObjectType("web");
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.ajaxSecurityToken =  true ;
   this.SetStandaloneVars=function()
   {
   };
   this.e11082_client=function()
   {
      return this.executeServerEvent("'CONFIRMAR'", false, null, false, false);
   };
   this.e13082_client=function()
   {
      return this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e14082_client=function()
   {
      return this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,26,27,28];
   this.GXLastCtrlId =28;
   GXValidFnc[2]={ id: 2, fld:"",grid:0};
   GXValidFnc[3]={ id: 3, fld:"MAINTABLE",grid:0};
   GXValidFnc[4]={ id: 4, fld:"",grid:0};
   GXValidFnc[5]={ id: 5, fld:"",grid:0};
   GXValidFnc[6]={ id: 6, fld:"GROUP1",grid:0};
   GXValidFnc[7]={ id: 7, fld:"GROUP1TABLE",grid:0};
   GXValidFnc[8]={ id: 8, fld:"",grid:0};
   GXValidFnc[9]={ id: 9, fld:"",grid:0};
   GXValidFnc[10]={ id: 10, fld:"",grid:0};
   GXValidFnc[11]={ id: 11, fld:"",grid:0};
   GXValidFnc[12]={ id:12 ,lvl:0,type:"svchar",len:100,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[],fld:"vUSERNAME",gxz:"ZV5UserName",gxold:"OV5UserName",gxvar:"AV5UserName",ucs:[],op:[],ip:[],
						nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV5UserName=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV5UserName=Value},v2c:function(){gx.fn.setControlValue("vUSERNAME",gx.O.AV5UserName,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV5UserName=this.val()},val:function(){return gx.fn.getControlValue("vUSERNAME")},nac:gx.falseFn};
   GXValidFnc[13]={ id: 13, fld:"",grid:0};
   GXValidFnc[14]={ id: 14, fld:"",grid:0};
   GXValidFnc[15]={ id: 15, fld:"",grid:0};
   GXValidFnc[16]={ id: 16, fld:"",grid:0};
   GXValidFnc[17]={ id:17 ,lvl:0,type:"svchar",len:40,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[],fld:"vUSERNICKNAME",gxz:"ZV6UserNickName",gxold:"OV6UserNickName",gxvar:"AV6UserNickName",ucs:[],op:[],ip:[],
						nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV6UserNickName=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV6UserNickName=Value},v2c:function(){gx.fn.setControlValue("vUSERNICKNAME",gx.O.AV6UserNickName,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV6UserNickName=this.val()},val:function(){return gx.fn.getControlValue("vUSERNICKNAME")},nac:gx.falseFn};
   GXValidFnc[18]={ id: 18, fld:"",grid:0};
   GXValidFnc[19]={ id: 19, fld:"",grid:0};
   GXValidFnc[20]={ id: 20, fld:"",grid:0};
   GXValidFnc[21]={ id: 21, fld:"",grid:0};
   GXValidFnc[22]={ id:22 ,lvl:0,type:"svchar",len:40,dec:0,sign:false,isPwd:true,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[],fld:"vUSERPASSWORD",gxz:"ZV7UserPassword",gxold:"OV7UserPassword",gxvar:"AV7UserPassword",ucs:[],op:[],ip:[],
						nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV7UserPassword=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV7UserPassword=Value},v2c:function(){gx.fn.setControlValue("vUSERPASSWORD",gx.O.AV7UserPassword,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV7UserPassword=this.val()},val:function(){return gx.fn.getControlValue("vUSERPASSWORD")},nac:gx.falseFn};
   GXValidFnc[23]={ id: 23, fld:"",grid:0};
   GXValidFnc[24]={ id: 24, fld:"",grid:0};
   GXValidFnc[26]={ id: 26, fld:"",grid:0};
   GXValidFnc[27]={ id: 27, fld:"",grid:0};
   GXValidFnc[28]={ id: 28, fld:"CONFIRMAR",grid:0,evt:"e11082_client"};
   this.AV5UserName = "" ;
   this.ZV5UserName = "" ;
   this.OV5UserName = "" ;
   this.AV6UserNickName = "" ;
   this.ZV6UserNickName = "" ;
   this.OV6UserNickName = "" ;
   this.AV7UserPassword = "" ;
   this.ZV7UserPassword = "" ;
   this.OV7UserPassword = "" ;
   this.AV5UserName = "" ;
   this.AV6UserNickName = "" ;
   this.AV7UserPassword = "" ;
   this.Events = {"e11082_client": ["'CONFIRMAR'", true] ,"e13082_client": ["ENTER", true] ,"e14082_client": ["CANCEL", true]};
   this.EvtParms["REFRESH"] = [[],[]];
   this.EvtParms["'CONFIRMAR'"] = [[{av:'AV5UserName',fld:'vUSERNAME',pic:''},{av:'AV6UserNickName',fld:'vUSERNICKNAME',pic:''},{av:'AV7UserPassword',fld:'vUSERPASSWORD',pic:''}],[]];
   this.Initialize( );
});
gx.wi( function() { gx.createParentObj(this.createuser);});
