gx.evt.autoSkip = false;
gx.define('createtask', false, function () {
   this.ServerClass =  "createtask" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.ServerFullClass =  "createtask.aspx" ;
   this.setObjectType("web");
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.ajaxSecurityToken =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV9UserId=gx.fn.getIntegerValue("vUSERID",'.') ;
   };
   this.e120b2_client=function()
   {
      return this.executeServerEvent("'CONFIRMAR'", false, null, false, false);
   };
   this.e130b2_client=function()
   {
      return this.executeServerEvent("'CANCELAR'", false, null, false, false);
   };
   this.e150b2_client=function()
   {
      return this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e160b2_client=function()
   {
      return this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,31,33];
   this.GXLastCtrlId =33;
   GXValidFnc[2]={ id: 2, fld:"",grid:0};
   GXValidFnc[3]={ id: 3, fld:"MAINTABLE",grid:0};
   GXValidFnc[4]={ id: 4, fld:"",grid:0};
   GXValidFnc[5]={ id: 5, fld:"",grid:0};
   GXValidFnc[6]={ id: 6, fld:"GROUP1",grid:0};
   GXValidFnc[7]={ id: 7, fld:"GROUP1TABLE",grid:0};
   GXValidFnc[8]={ id: 8, fld:"",grid:0};
   GXValidFnc[9]={ id: 9, fld:"",grid:0};
   GXValidFnc[10]={ id: 10, fld:"TABLE1",grid:0};
   GXValidFnc[11]={ id: 11, fld:"",grid:0};
   GXValidFnc[12]={ id: 12, fld:"",grid:0};
   GXValidFnc[13]={ id: 13, fld:"",grid:0};
   GXValidFnc[14]={ id: 14, fld:"",grid:0};
   GXValidFnc[15]={ id:15 ,lvl:0,type:"svchar",len:40,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[],fld:"vTASKNAME",gxz:"ZV6TaskName",gxold:"OV6TaskName",gxvar:"AV6TaskName",ucs:[],op:[],ip:[],
						nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV6TaskName=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV6TaskName=Value},v2c:function(){gx.fn.setControlValue("vTASKNAME",gx.O.AV6TaskName,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV6TaskName=this.val()},val:function(){return gx.fn.getControlValue("vTASKNAME")},nac:gx.falseFn};
   GXValidFnc[16]={ id: 16, fld:"",grid:0};
   GXValidFnc[17]={ id: 17, fld:"",grid:0};
   GXValidFnc[18]={ id: 18, fld:"",grid:0};
   GXValidFnc[19]={ id: 19, fld:"",grid:0};
   GXValidFnc[20]={ id:20 ,lvl:0,type:"vchar",len:2097152,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[],fld:"vTASKDESCRIPTION",gxz:"ZV7TaskDescription",gxold:"OV7TaskDescription",gxvar:"AV7TaskDescription",ucs:[],op:[],ip:[],
						nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV7TaskDescription=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV7TaskDescription=Value},v2c:function(){gx.fn.setControlValue("vTASKDESCRIPTION",gx.O.AV7TaskDescription,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV7TaskDescription=this.val()},val:function(){return gx.fn.getControlValue("vTASKDESCRIPTION")},nac:gx.falseFn};
   GXValidFnc[21]={ id: 21, fld:"",grid:0};
   GXValidFnc[22]={ id: 22, fld:"",grid:0};
   GXValidFnc[23]={ id: 23, fld:"",grid:0};
   GXValidFnc[24]={ id: 24, fld:"",grid:0};
   GXValidFnc[25]={ id:25 ,lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[],fld:"vTASKSTATUS",gxz:"ZV8TaskStatus",gxold:"OV8TaskStatus",gxvar:"AV8TaskStatus",ucs:[],op:[],ip:[],
						nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV8TaskStatus=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV8TaskStatus=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vTASKSTATUS",gx.O.AV8TaskStatus)},c2v:function(){if(this.val()!==undefined)gx.O.AV8TaskStatus=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vTASKSTATUS",'.')},nac:gx.falseFn};
   GXValidFnc[26]={ id: 26, fld:"",grid:0};
   GXValidFnc[27]={ id: 27, fld:"",grid:0};
   GXValidFnc[28]={ id: 28, fld:"TABLE2",grid:0};
   GXValidFnc[31]={ id: 31, fld:"CONFIRMAR",grid:0,evt:"e120b2_client"};
   GXValidFnc[33]={ id: 33, fld:"CANCELAR",grid:0,evt:"e130b2_client"};
   this.AV6TaskName = "" ;
   this.ZV6TaskName = "" ;
   this.OV6TaskName = "" ;
   this.AV7TaskDescription = "" ;
   this.ZV7TaskDescription = "" ;
   this.OV7TaskDescription = "" ;
   this.AV8TaskStatus = 0 ;
   this.ZV8TaskStatus = 0 ;
   this.OV8TaskStatus = 0 ;
   this.AV6TaskName = "" ;
   this.AV7TaskDescription = "" ;
   this.AV8TaskStatus = 0 ;
   this.AV9UserId = 0 ;
   this.Events = {"e120b2_client": ["'CONFIRMAR'", true] ,"e130b2_client": ["'CANCELAR'", true] ,"e150b2_client": ["ENTER", true] ,"e160b2_client": ["CANCEL", true]};
   this.EvtParms["REFRESH"] = [[{av:'AV9UserId',fld:'vUSERID',pic:'ZZZ9',hsh:true}],[]];
   this.EvtParms["START"] = [[],[{av:'AV9UserId',fld:'vUSERID',pic:'ZZZ9',hsh:true}]];
   this.EvtParms["'CONFIRMAR'"] = [[{av:'AV6TaskName',fld:'vTASKNAME',pic:'@!'},{av:'AV7TaskDescription',fld:'vTASKDESCRIPTION',pic:''},{ctrl:'vTASKSTATUS'},{av:'AV8TaskStatus',fld:'vTASKSTATUS',pic:'ZZZ9'},{av:'AV9UserId',fld:'vUSERID',pic:'ZZZ9',hsh:true}],[]];
   this.EvtParms["'CANCELAR'"] = [[],[]];
   this.setVCMap("AV9UserId", "vUSERID", 0, "int", 4, 0);
   this.Initialize( );
});
gx.wi( function() { gx.createParentObj(this.createtask);});
