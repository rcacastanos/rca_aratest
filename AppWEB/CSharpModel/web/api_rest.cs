using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class api_rest : GXProcedure
   {
      public api_rest( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public api_rest( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( string aP0_ServiceName ,
                           string aP1_CadenaJSON ,
                           out string aP2_ResultadoJson ,
                           out short aP3_ErrorCode ,
                           out string aP4_ErrorMessage )
      {
         this.AV17ServiceName = aP0_ServiceName;
         this.AV12CadenaJSON = aP1_CadenaJSON;
         this.AV13ResultadoJson = "" ;
         this.AV16ErrorCode = 0 ;
         this.AV15ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP2_ResultadoJson=this.AV13ResultadoJson;
         aP3_ErrorCode=this.AV16ErrorCode;
         aP4_ErrorMessage=this.AV15ErrorMessage;
      }

      public string executeUdp( string aP0_ServiceName ,
                                string aP1_CadenaJSON ,
                                out string aP2_ResultadoJson ,
                                out short aP3_ErrorCode )
      {
         execute(aP0_ServiceName, aP1_CadenaJSON, out aP2_ResultadoJson, out aP3_ErrorCode, out aP4_ErrorMessage);
         return AV15ErrorMessage ;
      }

      public void executeSubmit( string aP0_ServiceName ,
                                 string aP1_CadenaJSON ,
                                 out string aP2_ResultadoJson ,
                                 out short aP3_ErrorCode ,
                                 out string aP4_ErrorMessage )
      {
         api_rest objapi_rest;
         objapi_rest = new api_rest();
         objapi_rest.AV17ServiceName = aP0_ServiceName;
         objapi_rest.AV12CadenaJSON = aP1_CadenaJSON;
         objapi_rest.AV13ResultadoJson = "" ;
         objapi_rest.AV16ErrorCode = 0 ;
         objapi_rest.AV15ErrorMessage = "" ;
         objapi_rest.context.SetSubmitInitialConfig(context);
         objapi_rest.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objapi_rest);
         aP2_ResultadoJson=this.AV13ResultadoJson;
         aP3_ErrorCode=this.AV16ErrorCode;
         aP4_ErrorMessage=this.AV15ErrorMessage;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((api_rest)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8HttpClient.Host = "localhost/Servicios.NetEnvironment";
         AV8HttpClient.Secure = 0;
         AV8HttpClient.BaseURL = "/rest/";
         AV8HttpClient.IncludeCookies = true;
         AV8HttpClient.AddHeader("Content-Type", "application/json");
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12CadenaJSON)) )
         {
            AV8HttpClient.AddString(AV12CadenaJSON);
         }
         AV8HttpClient.Execute("POST", AV17ServiceName);
         AV16ErrorCode = AV8HttpClient.ErrCode;
         if ( AV8HttpClient.StatusCode == 200 )
         {
            AV13ResultadoJson = AV8HttpClient.ToString();
            AV15ErrorMessage = "OK";
         }
         else
         {
            AV13ResultadoJson = AV8HttpClient.ToString();
            AV15ErrorMessage = AV8HttpClient.ErrDescription;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections();
         }
         ExitApp();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV13ResultadoJson = "";
         AV15ErrorMessage = "";
         AV8HttpClient = new GxHttpClient( context);
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV16ErrorCode ;
      private string AV12CadenaJSON ;
      private string AV13ResultadoJson ;
      private string AV15ErrorMessage ;
      private string AV17ServiceName ;
      private string aP2_ResultadoJson ;
      private short aP3_ErrorCode ;
      private string aP4_ErrorMessage ;
      private GxHttpClient AV8HttpClient ;
   }

}
