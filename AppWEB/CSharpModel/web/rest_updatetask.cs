using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class rest_updatetask : GXProcedure
   {
      public rest_updatetask( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public rest_updatetask( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( short aP0_TaskId ,
                           string aP1_TaskName ,
                           string aP2_TaskDescription ,
                           short aP3_TaskStatus ,
                           short aP4_UserId ,
                           out bool aP5_IsOK ,
                           out string aP6_ErrorMessage )
      {
         this.AV15TaskId = aP0_TaskId;
         this.AV16TaskName = aP1_TaskName;
         this.AV14TaskDescription = aP2_TaskDescription;
         this.AV17TaskStatus = aP3_TaskStatus;
         this.AV18UserId = aP4_UserId;
         this.AV11IsOK = false ;
         this.AV10ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP5_IsOK=this.AV11IsOK;
         aP6_ErrorMessage=this.AV10ErrorMessage;
      }

      public string executeUdp( short aP0_TaskId ,
                                string aP1_TaskName ,
                                string aP2_TaskDescription ,
                                short aP3_TaskStatus ,
                                short aP4_UserId ,
                                out bool aP5_IsOK )
      {
         execute(aP0_TaskId, aP1_TaskName, aP2_TaskDescription, aP3_TaskStatus, aP4_UserId, out aP5_IsOK, out aP6_ErrorMessage);
         return AV10ErrorMessage ;
      }

      public void executeSubmit( short aP0_TaskId ,
                                 string aP1_TaskName ,
                                 string aP2_TaskDescription ,
                                 short aP3_TaskStatus ,
                                 short aP4_UserId ,
                                 out bool aP5_IsOK ,
                                 out string aP6_ErrorMessage )
      {
         rest_updatetask objrest_updatetask;
         objrest_updatetask = new rest_updatetask();
         objrest_updatetask.AV15TaskId = aP0_TaskId;
         objrest_updatetask.AV16TaskName = aP1_TaskName;
         objrest_updatetask.AV14TaskDescription = aP2_TaskDescription;
         objrest_updatetask.AV17TaskStatus = aP3_TaskStatus;
         objrest_updatetask.AV18UserId = aP4_UserId;
         objrest_updatetask.AV11IsOK = false ;
         objrest_updatetask.AV10ErrorMessage = "" ;
         objrest_updatetask.context.SetSubmitInitialConfig(context);
         objrest_updatetask.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objrest_updatetask);
         aP5_IsOK=this.AV11IsOK;
         aP6_ErrorMessage=this.AV10ErrorMessage;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((rest_updatetask)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV11IsOK = false;
         AV13ServiceName = "WSTaskUpdate";
         AV8CadenaJSON = "{\"TaskId\":" + StringUtil.Str( (decimal)(AV15TaskId), 4, 0) + ", \"TaskName\": \"" + StringUtil.Trim( AV16TaskName) + "\",\"TaskDescription\": \"" + StringUtil.Trim( AV14TaskDescription) + "\", \"TaskStatus\":" + StringUtil.Trim( StringUtil.Str( (decimal)(AV17TaskStatus), 4, 0)) + "}";
         new api_rest(context ).execute(  AV13ServiceName,  AV8CadenaJSON, out  AV12ResultadoJson, out  AV9ErrorCode, out  AV10ErrorMessage) ;
         if ( AV9ErrorCode == 0 )
         {
            AV11IsOK = true;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections();
         }
         ExitApp();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV10ErrorMessage = "";
         AV13ServiceName = "";
         AV8CadenaJSON = "";
         AV12ResultadoJson = "";
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV15TaskId ;
      private short AV17TaskStatus ;
      private short AV18UserId ;
      private short AV9ErrorCode ;
      private bool AV11IsOK ;
      private string AV14TaskDescription ;
      private string AV10ErrorMessage ;
      private string AV8CadenaJSON ;
      private string AV12ResultadoJson ;
      private string AV16TaskName ;
      private string AV13ServiceName ;
      private bool aP5_IsOK ;
      private string aP6_ErrorMessage ;
   }

}
