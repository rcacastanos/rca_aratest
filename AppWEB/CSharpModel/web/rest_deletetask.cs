using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class rest_deletetask : GXProcedure
   {
      public rest_deletetask( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public rest_deletetask( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( short aP0_TaskId ,
                           out bool aP1_IsOK ,
                           out string aP2_ErrorMessage )
      {
         this.AV15TaskId = aP0_TaskId;
         this.AV11IsOK = false ;
         this.AV10ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP1_IsOK=this.AV11IsOK;
         aP2_ErrorMessage=this.AV10ErrorMessage;
      }

      public string executeUdp( short aP0_TaskId ,
                                out bool aP1_IsOK )
      {
         execute(aP0_TaskId, out aP1_IsOK, out aP2_ErrorMessage);
         return AV10ErrorMessage ;
      }

      public void executeSubmit( short aP0_TaskId ,
                                 out bool aP1_IsOK ,
                                 out string aP2_ErrorMessage )
      {
         rest_deletetask objrest_deletetask;
         objrest_deletetask = new rest_deletetask();
         objrest_deletetask.AV15TaskId = aP0_TaskId;
         objrest_deletetask.AV11IsOK = false ;
         objrest_deletetask.AV10ErrorMessage = "" ;
         objrest_deletetask.context.SetSubmitInitialConfig(context);
         objrest_deletetask.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objrest_deletetask);
         aP1_IsOK=this.AV11IsOK;
         aP2_ErrorMessage=this.AV10ErrorMessage;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((rest_deletetask)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV11IsOK = false;
         AV13ServiceName = "WSTaskDelete";
         AV8CadenaJSON = "{\"TaskId\":" + StringUtil.Str( (decimal)(AV15TaskId), 4, 0) + "}";
         new api_rest(context ).execute(  AV13ServiceName,  AV8CadenaJSON, out  AV12ResultadoJson, out  AV9ErrorCode, out  AV10ErrorMessage) ;
         if ( AV9ErrorCode == 0 )
         {
            AV11IsOK = true;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections();
         }
         ExitApp();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV10ErrorMessage = "";
         AV13ServiceName = "";
         AV8CadenaJSON = "";
         AV12ResultadoJson = "";
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV15TaskId ;
      private short AV9ErrorCode ;
      private bool AV11IsOK ;
      private string AV10ErrorMessage ;
      private string AV8CadenaJSON ;
      private string AV12ResultadoJson ;
      private string AV13ServiceName ;
      private bool aP1_IsOK ;
      private string aP2_ErrorMessage ;
   }

}
