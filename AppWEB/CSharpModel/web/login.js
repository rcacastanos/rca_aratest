gx.evt.autoSkip = false;
gx.define('login', false, function () {
   this.ServerClass =  "login" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.ServerFullClass =  "login.aspx" ;
   this.setObjectType("web");
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.ajaxSecurityToken =  true ;
   this.SetStandaloneVars=function()
   {
   };
   this.e11071_client=function()
   {
      this.clearMessages();
      this.popupOpenUrl(gx.http.formatLink("createuser.aspx",[]), []);
      this.refreshOutputs([]);
      this.OnClientEventEnd();
      return gx.$.Deferred().resolve();
   };
   this.e12072_client=function()
   {
      return this.executeServerEvent("'LOGIN'", false, null, false, false);
   };
   this.e14072_client=function()
   {
      return this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e15072_client=function()
   {
      return this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,23,24,25,26,27];
   this.GXLastCtrlId =27;
   GXValidFnc[2]={ id: 2, fld:"",grid:0};
   GXValidFnc[3]={ id: 3, fld:"MAINTABLE",grid:0};
   GXValidFnc[4]={ id: 4, fld:"",grid:0};
   GXValidFnc[5]={ id: 5, fld:"",grid:0};
   GXValidFnc[6]={ id: 6, fld:"TABLE1",grid:0};
   GXValidFnc[7]={ id: 7, fld:"",grid:0};
   GXValidFnc[8]={ id: 8, fld:"",grid:0};
   GXValidFnc[9]={ id: 9, fld:"",grid:0};
   GXValidFnc[10]={ id: 10, fld:"",grid:0};
   GXValidFnc[11]={ id:11 ,lvl:0,type:"svchar",len:40,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[],fld:"vUSERNAME",gxz:"ZV5UserName",gxold:"OV5UserName",gxvar:"AV5UserName",ucs:[],op:[],ip:[],
						nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV5UserName=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV5UserName=Value},v2c:function(){gx.fn.setControlValue("vUSERNAME",gx.O.AV5UserName,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV5UserName=this.val()},val:function(){return gx.fn.getControlValue("vUSERNAME")},nac:gx.falseFn};
   GXValidFnc[12]={ id: 12, fld:"",grid:0};
   GXValidFnc[13]={ id: 13, fld:"",grid:0};
   GXValidFnc[14]={ id: 14, fld:"",grid:0};
   GXValidFnc[15]={ id: 15, fld:"",grid:0};
   GXValidFnc[16]={ id:16 ,lvl:0,type:"svchar",len:40,dec:0,sign:false,isPwd:true,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[],fld:"vUSERPASSWORD",gxz:"ZV6UserPassword",gxold:"OV6UserPassword",gxvar:"AV6UserPassword",ucs:[],op:[],ip:[],
						nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV6UserPassword=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV6UserPassword=Value},v2c:function(){gx.fn.setControlValue("vUSERPASSWORD",gx.O.AV6UserPassword,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV6UserPassword=this.val()},val:function(){return gx.fn.getControlValue("vUSERPASSWORD")},nac:gx.falseFn};
   GXValidFnc[17]={ id: 17, fld:"",grid:0};
   GXValidFnc[18]={ id: 18, fld:"",grid:0};
   GXValidFnc[19]={ id: 19, fld:"LOGIN",grid:0,evt:"e12072_client"};
   GXValidFnc[20]={ id: 20, fld:"",grid:0};
   GXValidFnc[21]={ id: 21, fld:"",grid:0};
   GXValidFnc[23]={ id: 23, fld:"",grid:0};
   GXValidFnc[24]={ id: 24, fld:"",grid:0};
   GXValidFnc[25]={ id: 25, fld:"",grid:0};
   GXValidFnc[26]={ id: 26, fld:"",grid:0};
   GXValidFnc[27]={ id: 27, fld:"TXTREGISTRO", format:0,grid:0,evt:"e11071_client", ctrltype: "textblock"};
   this.AV5UserName = "" ;
   this.ZV5UserName = "" ;
   this.OV5UserName = "" ;
   this.AV6UserPassword = "" ;
   this.ZV6UserPassword = "" ;
   this.OV6UserPassword = "" ;
   this.AV5UserName = "" ;
   this.AV6UserPassword = "" ;
   this.Events = {"e12072_client": ["'LOGIN'", true] ,"e14072_client": ["ENTER", true] ,"e15072_client": ["CANCEL", true] ,"e11071_client": ["TXTREGISTRO.CLICK", false]};
   this.EvtParms["REFRESH"] = [[],[]];
   this.EvtParms["'LOGIN'"] = [[{av:'AV5UserName',fld:'vUSERNAME',pic:''},{av:'AV6UserPassword',fld:'vUSERPASSWORD',pic:''}],[]];
   this.EvtParms["TXTREGISTRO.CLICK"] = [[],[]];
   this.Initialize( );
});
gx.wi( function() { gx.createParentObj(this.login);});
