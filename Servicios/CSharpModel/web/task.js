gx.evt.autoSkip = false;
gx.define('task', false, function () {
   this.ServerClass =  "task" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.ServerFullClass =  "task.aspx" ;
   this.setObjectType("trn");
   this.hasEnterEvent = true;
   this.skipOnEnter = false;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.ajaxSecurityToken =  true ;
   this.SetStandaloneVars=function()
   {
   };
   this.Valid_Taskid=function()
   {
      return this.validSrvEvt("Valid_Taskid", 0).then((function (ret) {
      return ret;
      }).closure(this));
   }
   this.Valid_Taskcreatedate=function()
   {
      return this.validCliEvt("Valid_Taskcreatedate", 0, function () {
      try {
         var gxballoon = gx.util.balloon.getNew("TASKCREATEDATE");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.A14TaskCreateDate)==0) || new gx.date.gxdate( this.A14TaskCreateDate ).compare( gx.date.ymdhmstot( 1753, 1, 1, 0, 0, 0) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Task Create Date fuera de rango");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
          if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
      });
   }
   this.Valid_Userid=function()
   {
      return this.validSrvEvt("Valid_Userid", 0).then((function (ret) {
      return ret;
      }).closure(this));
   }
   this.e11022_client=function()
   {
      return this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e12022_client=function()
   {
      return this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,3,4,5,6,7,8,9,10,11,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68];
   this.GXLastCtrlId =68;
   GXValidFnc[2]={ id: 2, fld:"",grid:0};
   GXValidFnc[3]={ id: 3, fld:"MAINTABLE",grid:0};
   GXValidFnc[4]={ id: 4, fld:"",grid:0};
   GXValidFnc[5]={ id: 5, fld:"",grid:0};
   GXValidFnc[6]={ id: 6, fld:"TITLECONTAINER",grid:0};
   GXValidFnc[7]={ id: 7, fld:"",grid:0};
   GXValidFnc[8]={ id: 8, fld:"",grid:0};
   GXValidFnc[9]={ id: 9, fld:"TITLE", format:0,grid:0, ctrltype: "textblock"};
   GXValidFnc[10]={ id: 10, fld:"",grid:0};
   GXValidFnc[11]={ id: 11, fld:"",grid:0};
   GXValidFnc[13]={ id: 13, fld:"",grid:0};
   GXValidFnc[14]={ id: 14, fld:"",grid:0};
   GXValidFnc[15]={ id: 15, fld:"FORMCONTAINER",grid:0};
   GXValidFnc[16]={ id: 16, fld:"",grid:0};
   GXValidFnc[17]={ id: 17, fld:"TOOLBARCELL",grid:0};
   GXValidFnc[18]={ id: 18, fld:"",grid:0};
   GXValidFnc[19]={ id: 19, fld:"",grid:0};
   GXValidFnc[20]={ id: 20, fld:"",grid:0};
   GXValidFnc[21]={ id: 21, fld:"BTN_FIRST",grid:0,evt:"e13022_client",std:"FIRST"};
   GXValidFnc[22]={ id: 22, fld:"",grid:0};
   GXValidFnc[23]={ id: 23, fld:"BTN_PREVIOUS",grid:0,evt:"e14022_client",std:"PREVIOUS"};
   GXValidFnc[24]={ id: 24, fld:"",grid:0};
   GXValidFnc[25]={ id: 25, fld:"BTN_NEXT",grid:0,evt:"e15022_client",std:"NEXT"};
   GXValidFnc[26]={ id: 26, fld:"",grid:0};
   GXValidFnc[27]={ id: 27, fld:"BTN_LAST",grid:0,evt:"e16022_client",std:"LAST"};
   GXValidFnc[28]={ id: 28, fld:"",grid:0};
   GXValidFnc[29]={ id: 29, fld:"BTN_SELECT",grid:0,evt:"e17022_client",std:"SELECT"};
   GXValidFnc[30]={ id: 30, fld:"",grid:0};
   GXValidFnc[31]={ id: 31, fld:"",grid:0};
   GXValidFnc[32]={ id: 32, fld:"",grid:0};
   GXValidFnc[33]={ id: 33, fld:"",grid:0};
   GXValidFnc[34]={ id:34 ,lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:this.Valid_Taskid,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[],fld:"TASKID",gxz:"Z5TaskId",gxold:"O5TaskId",gxvar:"A5TaskId",ucs:[],op:[59,54,49,44,39],ip:[59,54,49,44,39,34],
						nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A5TaskId=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z5TaskId=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("TASKID",gx.O.A5TaskId,0)},c2v:function(){if(this.val()!==undefined)gx.O.A5TaskId=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("TASKID",'.')},nac:gx.falseFn};
   GXValidFnc[35]={ id: 35, fld:"",grid:0};
   GXValidFnc[36]={ id: 36, fld:"",grid:0};
   GXValidFnc[37]={ id: 37, fld:"",grid:0};
   GXValidFnc[38]={ id: 38, fld:"",grid:0};
   GXValidFnc[39]={ id:39 ,lvl:0,type:"svchar",len:40,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[],fld:"TASKNAME",gxz:"Z6TaskName",gxold:"O6TaskName",gxvar:"A6TaskName",ucs:[],op:[],ip:[],
						nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A6TaskName=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z6TaskName=Value},v2c:function(){gx.fn.setControlValue("TASKNAME",gx.O.A6TaskName,0)},c2v:function(){if(this.val()!==undefined)gx.O.A6TaskName=this.val()},val:function(){return gx.fn.getControlValue("TASKNAME")},nac:gx.falseFn};
   GXValidFnc[40]={ id: 40, fld:"",grid:0};
   GXValidFnc[41]={ id: 41, fld:"",grid:0};
   GXValidFnc[42]={ id: 42, fld:"",grid:0};
   GXValidFnc[43]={ id: 43, fld:"",grid:0};
   GXValidFnc[44]={ id:44 ,lvl:0,type:"vchar",len:2097152,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[],fld:"TASKDESCRIPTION",gxz:"Z7TaskDescription",gxold:"O7TaskDescription",gxvar:"A7TaskDescription",ucs:[],op:[],ip:[],
						nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A7TaskDescription=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z7TaskDescription=Value},v2c:function(){gx.fn.setControlValue("TASKDESCRIPTION",gx.O.A7TaskDescription,0)},c2v:function(){if(this.val()!==undefined)gx.O.A7TaskDescription=this.val()},val:function(){return gx.fn.getControlValue("TASKDESCRIPTION")},nac:gx.falseFn};
   GXValidFnc[45]={ id: 45, fld:"",grid:0};
   GXValidFnc[46]={ id: 46, fld:"",grid:0};
   GXValidFnc[47]={ id: 47, fld:"",grid:0};
   GXValidFnc[48]={ id: 48, fld:"",grid:0};
   GXValidFnc[49]={ id:49 ,lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Valid_Taskcreatedate,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[],fld:"TASKCREATEDATE",gxz:"Z14TaskCreateDate",gxold:"O14TaskCreateDate",gxvar:"A14TaskCreateDate",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[49],ip:[49],
						nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A14TaskCreateDate=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z14TaskCreateDate=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("TASKCREATEDATE",gx.O.A14TaskCreateDate,0)},c2v:function(){if(this.val()!==undefined)gx.O.A14TaskCreateDate=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("TASKCREATEDATE")},nac:gx.falseFn};
   GXValidFnc[50]={ id: 50, fld:"",grid:0};
   GXValidFnc[51]={ id: 51, fld:"",grid:0};
   GXValidFnc[52]={ id: 52, fld:"",grid:0};
   GXValidFnc[53]={ id: 53, fld:"",grid:0};
   GXValidFnc[54]={ id:54 ,lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[],fld:"TASKSTATUS",gxz:"Z8TaskStatus",gxold:"O8TaskStatus",gxvar:"A8TaskStatus",ucs:[],op:[],ip:[],
						nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A8TaskStatus=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z8TaskStatus=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("TASKSTATUS",gx.O.A8TaskStatus)},c2v:function(){if(this.val()!==undefined)gx.O.A8TaskStatus=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("TASKSTATUS",'.')},nac:gx.falseFn};
   GXValidFnc[55]={ id: 55, fld:"",grid:0};
   GXValidFnc[56]={ id: 56, fld:"",grid:0};
   GXValidFnc[57]={ id: 57, fld:"",grid:0};
   GXValidFnc[58]={ id: 58, fld:"",grid:0};
   GXValidFnc[59]={ id:59 ,lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:this.Valid_Userid,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[],fld:"USERID",gxz:"Z9UserId",gxold:"O9UserId",gxvar:"A9UserId",ucs:[],op:[],ip:[59],
						nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A9UserId=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z9UserId=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("USERID",gx.O.A9UserId,0)},c2v:function(){if(this.val()!==undefined)gx.O.A9UserId=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("USERID",'.')},nac:gx.falseFn};
   GXValidFnc[60]={ id: 60, fld:"",grid:0};
   GXValidFnc[61]={ id: 61, fld:"",grid:0};
   GXValidFnc[62]={ id: 62, fld:"",grid:0};
   GXValidFnc[63]={ id: 63, fld:"",grid:0};
   GXValidFnc[64]={ id: 64, fld:"BTN_ENTER",grid:0,evt:"e11022_client",std:"ENTER"};
   GXValidFnc[65]={ id: 65, fld:"",grid:0};
   GXValidFnc[66]={ id: 66, fld:"BTN_CANCEL",grid:0,evt:"e12022_client"};
   GXValidFnc[67]={ id: 67, fld:"",grid:0};
   GXValidFnc[68]={ id: 68, fld:"BTN_DELETE",grid:0,evt:"e18022_client",std:"DELETE"};
   this.A5TaskId = 0 ;
   this.Z5TaskId = 0 ;
   this.O5TaskId = 0 ;
   this.A6TaskName = "" ;
   this.Z6TaskName = "" ;
   this.O6TaskName = "" ;
   this.A7TaskDescription = "" ;
   this.Z7TaskDescription = "" ;
   this.O7TaskDescription = "" ;
   this.A14TaskCreateDate = gx.date.nullDate() ;
   this.Z14TaskCreateDate = gx.date.nullDate() ;
   this.O14TaskCreateDate = gx.date.nullDate() ;
   this.A8TaskStatus = 0 ;
   this.Z8TaskStatus = 0 ;
   this.O8TaskStatus = 0 ;
   this.A9UserId = 0 ;
   this.Z9UserId = 0 ;
   this.O9UserId = 0 ;
   this.A5TaskId = 0 ;
   this.A6TaskName = "" ;
   this.A7TaskDescription = "" ;
   this.A14TaskCreateDate = gx.date.nullDate() ;
   this.A8TaskStatus = 0 ;
   this.A9UserId = 0 ;
   this.Events = {"e11022_client": ["ENTER", true] ,"e12022_client": ["CANCEL", true]};
   this.EvtParms["ENTER"] = [[{postForm:true}],[]];
   this.EvtParms["REFRESH"] = [[],[]];
   this.EvtParms["VALID_TASKID"] = [[{ctrl:'TASKSTATUS'},{av:'A8TaskStatus',fld:'TASKSTATUS',pic:'ZZZ9'},{av:'A5TaskId',fld:'TASKID',pic:'ZZZ9'},{av:'Gx_mode',fld:'vMODE',pic:'@!'}],[{av:'A6TaskName',fld:'TASKNAME',pic:''},{av:'A7TaskDescription',fld:'TASKDESCRIPTION',pic:''},{av:'A14TaskCreateDate',fld:'TASKCREATEDATE',pic:'99/99/99 99:99'},{ctrl:'TASKSTATUS'},{av:'A8TaskStatus',fld:'TASKSTATUS',pic:'ZZZ9'},{av:'A9UserId',fld:'USERID',pic:'ZZZ9'},{av:'Gx_mode',fld:'vMODE',pic:'@!'},{av:'Z5TaskId'},{av:'Z6TaskName'},{av:'Z7TaskDescription'},{av:'Z14TaskCreateDate'},{av:'Z8TaskStatus'},{av:'Z9UserId'},{ctrl:'BTN_DELETE',prop:'Enabled'},{ctrl:'BTN_ENTER',prop:'Enabled'}]];
   this.EvtParms["VALID_TASKCREATEDATE"] = [[{av:'A14TaskCreateDate',fld:'TASKCREATEDATE',pic:'99/99/99 99:99'}],[{av:'A14TaskCreateDate',fld:'TASKCREATEDATE',pic:'99/99/99 99:99'}]];
   this.EvtParms["VALID_USERID"] = [[{av:'A9UserId',fld:'USERID',pic:'ZZZ9'}],[]];
   this.EnterCtrl = ["BTN_ENTER"];
   this.Initialize( );
});
gx.wi( function() { gx.createParentObj(this.task);});
