using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wscreateuser : GXProcedure
   {
      public wscreateuser( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public wscreateuser( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( string aP0_UserName ,
                           string aP1_UserNickName ,
                           string aP2_UserPassword ,
                           out short aP3_UserId )
      {
         this.AV8UserName = aP0_UserName;
         this.AV9UserNickName = aP1_UserNickName;
         this.AV10UserPassword = aP2_UserPassword;
         this.AV11UserId = 0 ;
         initialize();
         executePrivate();
         aP3_UserId=this.AV11UserId;
      }

      public short executeUdp( string aP0_UserName ,
                               string aP1_UserNickName ,
                               string aP2_UserPassword )
      {
         execute(aP0_UserName, aP1_UserNickName, aP2_UserPassword, out aP3_UserId);
         return AV11UserId ;
      }

      public void executeSubmit( string aP0_UserName ,
                                 string aP1_UserNickName ,
                                 string aP2_UserPassword ,
                                 out short aP3_UserId )
      {
         wscreateuser objwscreateuser;
         objwscreateuser = new wscreateuser();
         objwscreateuser.AV8UserName = aP0_UserName;
         objwscreateuser.AV9UserNickName = aP1_UserNickName;
         objwscreateuser.AV10UserPassword = aP2_UserPassword;
         objwscreateuser.AV11UserId = 0 ;
         objwscreateuser.context.SetSubmitInitialConfig(context);
         objwscreateuser.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objwscreateuser);
         aP3_UserId=this.AV11UserId;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((wscreateuser)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /*
            INSERT RECORD ON TABLE User

         */
         A10UserName = AV8UserName;
         A11UserNickName = AV9UserNickName;
         A12UserPassword = AV10UserPassword;
         /* Using cursor P000E2 */
         pr_default.execute(0, new Object[] {A10UserName, A11UserNickName, A12UserPassword});
         A9UserId = P000E2_A9UserId[0];
         pr_default.close(0);
         dsDefault.SmartCacheProvider.SetUpdated("User");
         if ( (pr_default.getStatus(0) == 1) )
         {
            context.Gx_err = 1;
            Gx_emsg = (string)(context.GetMessage( "GXM_noupdate", ""));
         }
         else
         {
            context.Gx_err = 0;
            Gx_emsg = "";
         }
         /* End Insert */
         context.CommitDataStores("wscreateuser",pr_default);
         AV11UserId = A9UserId;
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections();
         }
         ExitApp();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         A10UserName = "";
         A11UserNickName = "";
         A12UserPassword = "";
         P000E2_A9UserId = new short[1] ;
         Gx_emsg = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wscreateuser__default(),
            new Object[][] {
                new Object[] {
               P000E2_A9UserId
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV11UserId ;
      private short A9UserId ;
      private int GX_INS3 ;
      private string Gx_emsg ;
      private string AV8UserName ;
      private string AV9UserNickName ;
      private string AV10UserPassword ;
      private string A10UserName ;
      private string A11UserNickName ;
      private string A12UserPassword ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private short[] P000E2_A9UserId ;
      private short aP3_UserId ;
   }

   public class wscreateuser__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP000E2;
          prmP000E2 = new Object[] {
          new ParDef("@UserName",GXType.NVarChar,100,0) ,
          new ParDef("@UserNickName",GXType.NVarChar,40,0) ,
          new ParDef("@UserPassword",GXType.NVarChar,40,0)
          };
          def= new CursorDef[] {
              new CursorDef("P000E2", "INSERT INTO [User]([UserName], [UserNickName], [UserPassword]) VALUES(@UserName, @UserNickName, @UserPassword); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP000E2)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1);
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.wscreateuser_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class wscreateuser_services : GxRestService
 {
    [OperationContract(Name = "WSCreateUser" )]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( string UserName ,
                         string UserNickName ,
                         string UserPassword ,
                         out short UserId )
    {
       UserId = 0 ;
       try
       {
          if ( ! ProcessHeaders("wscreateuser") )
          {
             return  ;
          }
          wscreateuser worker = new wscreateuser(context);
          worker.IsMain = RunAsMain ;
          worker.execute(UserName,UserNickName,UserPassword,out UserId );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
