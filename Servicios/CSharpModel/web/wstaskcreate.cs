using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wstaskcreate : GXProcedure
   {
      public wstaskcreate( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public wstaskcreate( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( string aP0_TaskName ,
                           string aP1_TaskDescription ,
                           short aP2_TaskStatus ,
                           short aP3_UserId )
      {
         this.AV8TaskName = aP0_TaskName;
         this.AV9TaskDescription = aP1_TaskDescription;
         this.AV10TaskStatus = aP2_TaskStatus;
         this.AV12UserId = aP3_UserId;
         initialize();
         executePrivate();
      }

      public void executeSubmit( string aP0_TaskName ,
                                 string aP1_TaskDescription ,
                                 short aP2_TaskStatus ,
                                 short aP3_UserId )
      {
         wstaskcreate objwstaskcreate;
         objwstaskcreate = new wstaskcreate();
         objwstaskcreate.AV8TaskName = aP0_TaskName;
         objwstaskcreate.AV9TaskDescription = aP1_TaskDescription;
         objwstaskcreate.AV10TaskStatus = aP2_TaskStatus;
         objwstaskcreate.AV12UserId = aP3_UserId;
         objwstaskcreate.context.SetSubmitInitialConfig(context);
         objwstaskcreate.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objwstaskcreate);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((wstaskcreate)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /*
            INSERT RECORD ON TABLE Task

         */
         A6TaskName = StringUtil.Upper( AV8TaskName);
         A7TaskDescription = AV9TaskDescription;
         A8TaskStatus = AV10TaskStatus;
         A14TaskCreateDate = DateTimeUtil.ServerNow( context, pr_default);
         A9UserId = AV12UserId;
         /* Using cursor P000G2 */
         pr_default.execute(0, new Object[] {A6TaskName, A7TaskDescription, A8TaskStatus, A14TaskCreateDate, A9UserId});
         A5TaskId = P000G2_A5TaskId[0];
         pr_default.close(0);
         dsDefault.SmartCacheProvider.SetUpdated("Task");
         if ( (pr_default.getStatus(0) == 1) )
         {
            context.Gx_err = 1;
            Gx_emsg = (string)(context.GetMessage( "GXM_noupdate", ""));
         }
         else
         {
            context.Gx_err = 0;
            Gx_emsg = "";
         }
         /* End Insert */
         context.CommitDataStores("wstaskcreate",pr_default);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections();
         }
         ExitApp();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         A6TaskName = "";
         A7TaskDescription = "";
         A14TaskCreateDate = (DateTime)(DateTime.MinValue);
         P000G2_A5TaskId = new short[1] ;
         Gx_emsg = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wstaskcreate__default(),
            new Object[][] {
                new Object[] {
               P000G2_A5TaskId
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV10TaskStatus ;
      private short AV12UserId ;
      private short A8TaskStatus ;
      private short A9UserId ;
      private short A5TaskId ;
      private int GX_INS2 ;
      private string Gx_emsg ;
      private DateTime A14TaskCreateDate ;
      private string AV9TaskDescription ;
      private string A7TaskDescription ;
      private string AV8TaskName ;
      private string A6TaskName ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private short[] P000G2_A5TaskId ;
   }

   public class wstaskcreate__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP000G2;
          prmP000G2 = new Object[] {
          new ParDef("@TaskName",GXType.NVarChar,40,0) ,
          new ParDef("@TaskDescription",GXType.NVarChar,2097152,0) ,
          new ParDef("@TaskStatus",GXType.Int16,4,0) ,
          new ParDef("@TaskCreateDate",GXType.DateTime,8,5) ,
          new ParDef("@UserId",GXType.Int16,4,0)
          };
          def= new CursorDef[] {
              new CursorDef("P000G2", "INSERT INTO [Task]([TaskName], [TaskDescription], [TaskStatus], [TaskCreateDate], [UserId]) VALUES(@TaskName, @TaskDescription, @TaskStatus, @TaskCreateDate, @UserId); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP000G2)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1);
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.wstaskcreate_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class wstaskcreate_services : GxRestService
 {
    [OperationContract(Name = "WSTaskCreate" )]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( string TaskName ,
                         string TaskDescription ,
                         short TaskStatus ,
                         short UserId )
    {
       try
       {
          if ( ! ProcessHeaders("wstaskcreate") )
          {
             return  ;
          }
          wstaskcreate worker = new wstaskcreate(context);
          worker.IsMain = RunAsMain ;
          worker.execute(TaskName,TaskDescription,TaskStatus,UserId );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
