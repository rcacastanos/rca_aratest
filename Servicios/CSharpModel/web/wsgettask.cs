using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wsgettask : GXProcedure
   {
      public wsgettask( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public wsgettask( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( short aP0_TaskId ,
                           out GXBaseCollection<SdtTasks_TasksItem> aP1_Tasks )
      {
         this.AV9TaskId = aP0_TaskId;
         this.AV11Tasks = new GXBaseCollection<SdtTasks_TasksItem>( context, "TasksItem", "Servicios") ;
         initialize();
         executePrivate();
         aP1_Tasks=this.AV11Tasks;
      }

      public GXBaseCollection<SdtTasks_TasksItem> executeUdp( short aP0_TaskId )
      {
         execute(aP0_TaskId, out aP1_Tasks);
         return AV11Tasks ;
      }

      public void executeSubmit( short aP0_TaskId ,
                                 out GXBaseCollection<SdtTasks_TasksItem> aP1_Tasks )
      {
         wsgettask objwsgettask;
         objwsgettask = new wsgettask();
         objwsgettask.AV9TaskId = aP0_TaskId;
         objwsgettask.AV11Tasks = new GXBaseCollection<SdtTasks_TasksItem>( context, "TasksItem", "Servicios") ;
         objwsgettask.context.SetSubmitInitialConfig(context);
         objwsgettask.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objwsgettask);
         aP1_Tasks=this.AV11Tasks;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((wsgettask)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P000K2 */
         pr_default.execute(0, new Object[] {AV9TaskId});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A5TaskId = P000K2_A5TaskId[0];
            A6TaskName = P000K2_A6TaskName[0];
            A7TaskDescription = P000K2_A7TaskDescription[0];
            A14TaskCreateDate = P000K2_A14TaskCreateDate[0];
            A8TaskStatus = P000K2_A8TaskStatus[0];
            A9UserId = P000K2_A9UserId[0];
            AV12TasksItem = new SdtTasks_TasksItem(context);
            AV12TasksItem.gxTpr_Taskid = A5TaskId;
            AV12TasksItem.gxTpr_Taskname = A6TaskName;
            AV12TasksItem.gxTpr_Taskdescription = A7TaskDescription;
            AV12TasksItem.gxTpr_Taskcreatedate = A14TaskCreateDate;
            AV12TasksItem.gxTpr_Taskstatus = A8TaskStatus;
            AV12TasksItem.gxTpr_Userid = A9UserId;
            AV11Tasks.Add(AV12TasksItem, 0);
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections();
         }
         ExitApp();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV11Tasks = new GXBaseCollection<SdtTasks_TasksItem>( context, "TasksItem", "Servicios");
         scmdbuf = "";
         P000K2_A5TaskId = new short[1] ;
         P000K2_A6TaskName = new string[] {""} ;
         P000K2_A7TaskDescription = new string[] {""} ;
         P000K2_A14TaskCreateDate = new DateTime[] {DateTime.MinValue} ;
         P000K2_A8TaskStatus = new short[1] ;
         P000K2_A9UserId = new short[1] ;
         A6TaskName = "";
         A7TaskDescription = "";
         A14TaskCreateDate = (DateTime)(DateTime.MinValue);
         AV12TasksItem = new SdtTasks_TasksItem(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wsgettask__default(),
            new Object[][] {
                new Object[] {
               P000K2_A5TaskId, P000K2_A6TaskName, P000K2_A7TaskDescription, P000K2_A14TaskCreateDate, P000K2_A8TaskStatus, P000K2_A9UserId
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV9TaskId ;
      private short A5TaskId ;
      private short A8TaskStatus ;
      private short A9UserId ;
      private string scmdbuf ;
      private DateTime A14TaskCreateDate ;
      private string A7TaskDescription ;
      private string A6TaskName ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private short[] P000K2_A5TaskId ;
      private string[] P000K2_A6TaskName ;
      private string[] P000K2_A7TaskDescription ;
      private DateTime[] P000K2_A14TaskCreateDate ;
      private short[] P000K2_A8TaskStatus ;
      private short[] P000K2_A9UserId ;
      private GXBaseCollection<SdtTasks_TasksItem> aP1_Tasks ;
      private GXBaseCollection<SdtTasks_TasksItem> AV11Tasks ;
      private SdtTasks_TasksItem AV12TasksItem ;
   }

   public class wsgettask__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP000K2;
          prmP000K2 = new Object[] {
          new ParDef("@AV9TaskId",GXType.Int16,4,0)
          };
          def= new CursorDef[] {
              new CursorDef("P000K2", "SELECT [TaskId], [TaskName], [TaskDescription], [TaskCreateDate], [TaskStatus], [UserId] FROM [Task] WHERE [TaskId] = @AV9TaskId ORDER BY [TaskId] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP000K2,1, GxCacheFrequency.OFF ,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1);
                ((string[]) buf[1])[0] = rslt.getVarchar(2);
                ((string[]) buf[2])[0] = rslt.getLongVarchar(3);
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(4);
                ((short[]) buf[4])[0] = rslt.getShort(5);
                ((short[]) buf[5])[0] = rslt.getShort(6);
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.wsgettask_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class wsgettask_services : GxRestService
 {
    [OperationContract(Name = "WSGetTask" )]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( short TaskId ,
                         out GxGenericCollection<SdtTasks_TasksItem_RESTInterface> Tasks )
    {
       Tasks = new GxGenericCollection<SdtTasks_TasksItem_RESTInterface>() ;
       try
       {
          if ( ! ProcessHeaders("wsgettask") )
          {
             return  ;
          }
          wsgettask worker = new wsgettask(context);
          worker.IsMain = RunAsMain ;
          GXBaseCollection<SdtTasks_TasksItem> gxrTasks = new GXBaseCollection<SdtTasks_TasksItem>();
          worker.execute(TaskId,out gxrTasks );
          worker.cleanup( );
          Tasks = new GxGenericCollection<SdtTasks_TasksItem_RESTInterface>(gxrTasks) ;
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
