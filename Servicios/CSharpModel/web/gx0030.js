gx.evt.autoSkip = false;
gx.define('gx0030', false, function () {
   this.ServerClass =  "gx0030" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.ServerFullClass =  "gx0030.aspx" ;
   this.setObjectType("web");
   this.anyGridBaseTable = true;
   this.hasEnterEvent = true;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.ajaxSecurityToken =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV10pUserId=gx.fn.getIntegerValue("vPUSERID",'.') ;
   };
   this.e15081_client=function()
   {
      this.clearMessages();
      if ( gx.text.compare( gx.fn.getCtrlProperty("ADVANCEDCONTAINER","Class") , "AdvancedContainer" ) == 0 )
      {
         gx.fn.setCtrlProperty("ADVANCEDCONTAINER","Class", "AdvancedContainer"+" "+"AdvancedContainerVisible" );
         gx.fn.setCtrlProperty("BTNTOGGLE","Class", gx.fn.getCtrlProperty("BTNTOGGLE","Class")+" "+"BtnToggleActive" );
      }
      else
      {
         gx.fn.setCtrlProperty("ADVANCEDCONTAINER","Class", "AdvancedContainer" );
         gx.fn.setCtrlProperty("BTNTOGGLE","Class", "BtnToggle" );
      }
      this.refreshOutputs([{av:'gx.fn.getCtrlProperty("ADVANCEDCONTAINER","Class")',ctrl:'ADVANCEDCONTAINER',prop:'Class'},{ctrl:'BTNTOGGLE',prop:'Class'}]);
      this.OnClientEventEnd();
      return gx.$.Deferred().resolve();
   };
   this.e11081_client=function()
   {
      this.clearMessages();
      if ( gx.text.compare( gx.fn.getCtrlProperty("USERIDFILTERCONTAINER","Class") , "AdvancedContainerItem" ) == 0 )
      {
         gx.fn.setCtrlProperty("USERIDFILTERCONTAINER","Class", "AdvancedContainerItem"+" "+"AdvancedContainerItemExpanded" );
         gx.fn.setCtrlProperty("vCUSERID","Visible", true );
      }
      else
      {
         gx.fn.setCtrlProperty("USERIDFILTERCONTAINER","Class", "AdvancedContainerItem" );
         gx.fn.setCtrlProperty("vCUSERID","Visible", false );
      }
      this.refreshOutputs([{av:'gx.fn.getCtrlProperty("USERIDFILTERCONTAINER","Class")',ctrl:'USERIDFILTERCONTAINER',prop:'Class'},{av:'gx.fn.getCtrlProperty("vCUSERID","Visible")',ctrl:'vCUSERID',prop:'Visible'}]);
      this.OnClientEventEnd();
      return gx.$.Deferred().resolve();
   };
   this.e12081_client=function()
   {
      this.clearMessages();
      if ( gx.text.compare( gx.fn.getCtrlProperty("USERNAMEFILTERCONTAINER","Class") , "AdvancedContainerItem" ) == 0 )
      {
         gx.fn.setCtrlProperty("USERNAMEFILTERCONTAINER","Class", "AdvancedContainerItem"+" "+"AdvancedContainerItemExpanded" );
         gx.fn.setCtrlProperty("vCUSERNAME","Visible", true );
      }
      else
      {
         gx.fn.setCtrlProperty("USERNAMEFILTERCONTAINER","Class", "AdvancedContainerItem" );
         gx.fn.setCtrlProperty("vCUSERNAME","Visible", false );
      }
      this.refreshOutputs([{av:'gx.fn.getCtrlProperty("USERNAMEFILTERCONTAINER","Class")',ctrl:'USERNAMEFILTERCONTAINER',prop:'Class'},{av:'gx.fn.getCtrlProperty("vCUSERNAME","Visible")',ctrl:'vCUSERNAME',prop:'Visible'}]);
      this.OnClientEventEnd();
      return gx.$.Deferred().resolve();
   };
   this.e13081_client=function()
   {
      this.clearMessages();
      if ( gx.text.compare( gx.fn.getCtrlProperty("USERNICKNAMEFILTERCONTAINER","Class") , "AdvancedContainerItem" ) == 0 )
      {
         gx.fn.setCtrlProperty("USERNICKNAMEFILTERCONTAINER","Class", "AdvancedContainerItem"+" "+"AdvancedContainerItemExpanded" );
         gx.fn.setCtrlProperty("vCUSERNICKNAME","Visible", true );
      }
      else
      {
         gx.fn.setCtrlProperty("USERNICKNAMEFILTERCONTAINER","Class", "AdvancedContainerItem" );
         gx.fn.setCtrlProperty("vCUSERNICKNAME","Visible", false );
      }
      this.refreshOutputs([{av:'gx.fn.getCtrlProperty("USERNICKNAMEFILTERCONTAINER","Class")',ctrl:'USERNICKNAMEFILTERCONTAINER',prop:'Class'},{av:'gx.fn.getCtrlProperty("vCUSERNICKNAME","Visible")',ctrl:'vCUSERNICKNAME',prop:'Visible'}]);
      this.OnClientEventEnd();
      return gx.$.Deferred().resolve();
   };
   this.e14081_client=function()
   {
      this.clearMessages();
      if ( gx.text.compare( gx.fn.getCtrlProperty("USERPASSWORDFILTERCONTAINER","Class") , "AdvancedContainerItem" ) == 0 )
      {
         gx.fn.setCtrlProperty("USERPASSWORDFILTERCONTAINER","Class", "AdvancedContainerItem"+" "+"AdvancedContainerItemExpanded" );
         gx.fn.setCtrlProperty("vCUSERPASSWORD","Visible", true );
      }
      else
      {
         gx.fn.setCtrlProperty("USERPASSWORDFILTERCONTAINER","Class", "AdvancedContainerItem" );
         gx.fn.setCtrlProperty("vCUSERPASSWORD","Visible", false );
      }
      this.refreshOutputs([{av:'gx.fn.getCtrlProperty("USERPASSWORDFILTERCONTAINER","Class")',ctrl:'USERPASSWORDFILTERCONTAINER',prop:'Class'},{av:'gx.fn.getCtrlProperty("vCUSERPASSWORD","Visible")',ctrl:'vCUSERPASSWORD',prop:'Visible'}]);
      this.OnClientEventEnd();
      return gx.$.Deferred().resolve();
   };
   this.e18082_client=function()
   {
      return this.executeServerEvent("ENTER", true, arguments[0], false, false);
   };
   this.e19081_client=function()
   {
      return this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,55,56,57,58,59,60];
   this.GXLastCtrlId =60;
   this.Grid1Container = new gx.grid.grid(this, 2,"WbpLvl2",54,"Grid1","Grid1","Grid1Container",this.CmpContext,this.IsMasterPage,"gx0030",[],false,1,false,true,10,true,false,false,"",0,"px",0,"px","Nueva fila",true,false,false,null,null,false,"",false,[1,1,1,1],false,0,true,false);
   var Grid1Container = this.Grid1Container;
   Grid1Container.addBitmap("&Linkselection","vLINKSELECTION",55,0,"px",17,"px",null,"","","SelectionAttribute","WWActionColumn");
   Grid1Container.addSingleLineEdit(9,56,"USERID","Id","","UserId","int",0,"px",4,4,"right",null,[],9,"UserId",true,0,false,false,"Attribute",1,"WWColumn");
   Grid1Container.addSingleLineEdit(10,57,"USERNAME","Name","","UserName","svchar",0,"px",100,80,"left",null,[],10,"UserName",true,0,false,false,"DescriptionAttribute",1,"WWColumn");
   this.Grid1Container.emptyText = "";
   this.setGrid(Grid1Container);
   GXValidFnc[2]={ id: 2, fld:"",grid:0};
   GXValidFnc[3]={ id: 3, fld:"MAIN",grid:0};
   GXValidFnc[4]={ id: 4, fld:"",grid:0};
   GXValidFnc[5]={ id: 5, fld:"",grid:0};
   GXValidFnc[6]={ id: 6, fld:"ADVANCEDCONTAINER",grid:0};
   GXValidFnc[7]={ id: 7, fld:"",grid:0};
   GXValidFnc[8]={ id: 8, fld:"",grid:0};
   GXValidFnc[9]={ id: 9, fld:"USERIDFILTERCONTAINER",grid:0};
   GXValidFnc[10]={ id: 10, fld:"",grid:0};
   GXValidFnc[11]={ id: 11, fld:"",grid:0};
   GXValidFnc[12]={ id: 12, fld:"LBLUSERIDFILTER", format:1,grid:0,evt:"e11081_client", ctrltype: "textblock"};
   GXValidFnc[13]={ id: 13, fld:"",grid:0};
   GXValidFnc[14]={ id: 14, fld:"",grid:0};
   GXValidFnc[15]={ id: 15, fld:"",grid:0};
   GXValidFnc[16]={ id:16 ,lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[this.Grid1Container],fld:"vCUSERID",gxz:"ZV6cUserId",gxold:"OV6cUserId",gxvar:"AV6cUserId",ucs:[],op:[],ip:[],
						nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV6cUserId=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV6cUserId=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vCUSERID",gx.O.AV6cUserId,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV6cUserId=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vCUSERID",'.')},nac:gx.falseFn};
   GXValidFnc[17]={ id: 17, fld:"",grid:0};
   GXValidFnc[18]={ id: 18, fld:"",grid:0};
   GXValidFnc[19]={ id: 19, fld:"USERNAMEFILTERCONTAINER",grid:0};
   GXValidFnc[20]={ id: 20, fld:"",grid:0};
   GXValidFnc[21]={ id: 21, fld:"",grid:0};
   GXValidFnc[22]={ id: 22, fld:"LBLUSERNAMEFILTER", format:1,grid:0,evt:"e12081_client", ctrltype: "textblock"};
   GXValidFnc[23]={ id: 23, fld:"",grid:0};
   GXValidFnc[24]={ id: 24, fld:"",grid:0};
   GXValidFnc[25]={ id: 25, fld:"",grid:0};
   GXValidFnc[26]={ id:26 ,lvl:0,type:"svchar",len:100,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[this.Grid1Container],fld:"vCUSERNAME",gxz:"ZV7cUserName",gxold:"OV7cUserName",gxvar:"AV7cUserName",ucs:[],op:[],ip:[],
						nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV7cUserName=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV7cUserName=Value},v2c:function(){gx.fn.setControlValue("vCUSERNAME",gx.O.AV7cUserName,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV7cUserName=this.val()},val:function(){return gx.fn.getControlValue("vCUSERNAME")},nac:gx.falseFn};
   GXValidFnc[27]={ id: 27, fld:"",grid:0};
   GXValidFnc[28]={ id: 28, fld:"",grid:0};
   GXValidFnc[29]={ id: 29, fld:"USERNICKNAMEFILTERCONTAINER",grid:0};
   GXValidFnc[30]={ id: 30, fld:"",grid:0};
   GXValidFnc[31]={ id: 31, fld:"",grid:0};
   GXValidFnc[32]={ id: 32, fld:"LBLUSERNICKNAMEFILTER", format:1,grid:0,evt:"e13081_client", ctrltype: "textblock"};
   GXValidFnc[33]={ id: 33, fld:"",grid:0};
   GXValidFnc[34]={ id: 34, fld:"",grid:0};
   GXValidFnc[35]={ id: 35, fld:"",grid:0};
   GXValidFnc[36]={ id:36 ,lvl:0,type:"svchar",len:40,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[this.Grid1Container],fld:"vCUSERNICKNAME",gxz:"ZV8cUserNickName",gxold:"OV8cUserNickName",gxvar:"AV8cUserNickName",ucs:[],op:[],ip:[],
						nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV8cUserNickName=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV8cUserNickName=Value},v2c:function(){gx.fn.setControlValue("vCUSERNICKNAME",gx.O.AV8cUserNickName,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV8cUserNickName=this.val()},val:function(){return gx.fn.getControlValue("vCUSERNICKNAME")},nac:gx.falseFn};
   GXValidFnc[37]={ id: 37, fld:"",grid:0};
   GXValidFnc[38]={ id: 38, fld:"",grid:0};
   GXValidFnc[39]={ id: 39, fld:"USERPASSWORDFILTERCONTAINER",grid:0};
   GXValidFnc[40]={ id: 40, fld:"",grid:0};
   GXValidFnc[41]={ id: 41, fld:"",grid:0};
   GXValidFnc[42]={ id: 42, fld:"LBLUSERPASSWORDFILTER", format:1,grid:0,evt:"e14081_client", ctrltype: "textblock"};
   GXValidFnc[43]={ id: 43, fld:"",grid:0};
   GXValidFnc[44]={ id: 44, fld:"",grid:0};
   GXValidFnc[45]={ id: 45, fld:"",grid:0};
   GXValidFnc[46]={ id:46 ,lvl:0,type:"svchar",len:40,dec:0,sign:false,isPwd:true,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[this.Grid1Container],fld:"vCUSERPASSWORD",gxz:"ZV9cUserPassword",gxold:"OV9cUserPassword",gxvar:"AV9cUserPassword",ucs:[],op:[],ip:[],
						nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV9cUserPassword=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV9cUserPassword=Value},v2c:function(){gx.fn.setControlValue("vCUSERPASSWORD",gx.O.AV9cUserPassword,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV9cUserPassword=this.val()},val:function(){return gx.fn.getControlValue("vCUSERPASSWORD")},nac:gx.falseFn};
   GXValidFnc[47]={ id: 47, fld:"",grid:0};
   GXValidFnc[48]={ id: 48, fld:"GRIDTABLE",grid:0};
   GXValidFnc[49]={ id: 49, fld:"",grid:0};
   GXValidFnc[50]={ id: 50, fld:"",grid:0};
   GXValidFnc[51]={ id: 51, fld:"BTNTOGGLE",grid:0,evt:"e15081_client"};
   GXValidFnc[52]={ id: 52, fld:"",grid:0};
   GXValidFnc[53]={ id: 53, fld:"",grid:0};
   GXValidFnc[55]={ id:55 ,lvl:2,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:54,gxgrid:this.Grid1Container,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[],fld:"vLINKSELECTION",gxz:"ZV5LinkSelection",gxold:"OV5LinkSelection",gxvar:"AV5LinkSelection",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV5LinkSelection=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV5LinkSelection=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vLINKSELECTION",row || gx.fn.currentGridRowImpl(54),gx.O.AV5LinkSelection,gx.O.AV14Linkselection_GXI)},c2v:function(row){gx.O.AV14Linkselection_GXI=this.val_GXI();if(this.val(row)!==undefined)gx.O.AV5LinkSelection=this.val(row)},val:function(row){return gx.fn.getGridControlValue("vLINKSELECTION",row || gx.fn.currentGridRowImpl(54))},val_GXI:function(row){return gx.fn.getGridControlValue("vLINKSELECTION_GXI",row || gx.fn.currentGridRowImpl(54))}, gxvar_GXI:'AV14Linkselection_GXI',nac:gx.falseFn};
   GXValidFnc[56]={ id:56 ,lvl:2,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:1,isacc:0,grid:54,gxgrid:this.Grid1Container,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[],fld:"USERID",gxz:"Z9UserId",gxold:"O9UserId",gxvar:"A9UserId",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'number',v2v:function(Value){if(Value!==undefined)gx.O.A9UserId=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z9UserId=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("USERID",row || gx.fn.currentGridRowImpl(54),gx.O.A9UserId,0)},c2v:function(row){if(this.val(row)!==undefined)gx.O.A9UserId=gx.num.intval(this.val(row))},val:function(row){return gx.fn.getGridIntegerValue("USERID",row || gx.fn.currentGridRowImpl(54),'.')},nac:gx.falseFn};
   GXValidFnc[57]={ id:57 ,lvl:2,type:"svchar",len:100,dec:0,sign:false,ro:1,isacc:0,grid:54,gxgrid:this.Grid1Container,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[],fld:"USERNAME",gxz:"Z10UserName",gxold:"O10UserName",gxvar:"A10UserName",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.A10UserName=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z10UserName=Value},v2c:function(row){gx.fn.setGridControlValue("USERNAME",row || gx.fn.currentGridRowImpl(54),gx.O.A10UserName,0)},c2v:function(row){if(this.val(row)!==undefined)gx.O.A10UserName=this.val(row)},val:function(row){return gx.fn.getGridControlValue("USERNAME",row || gx.fn.currentGridRowImpl(54))},nac:gx.falseFn};
   GXValidFnc[58]={ id: 58, fld:"",grid:0};
   GXValidFnc[59]={ id: 59, fld:"",grid:0};
   GXValidFnc[60]={ id: 60, fld:"BTN_CANCEL",grid:0,evt:"e19081_client"};
   this.AV6cUserId = 0 ;
   this.ZV6cUserId = 0 ;
   this.OV6cUserId = 0 ;
   this.AV7cUserName = "" ;
   this.ZV7cUserName = "" ;
   this.OV7cUserName = "" ;
   this.AV8cUserNickName = "" ;
   this.ZV8cUserNickName = "" ;
   this.OV8cUserNickName = "" ;
   this.AV9cUserPassword = "" ;
   this.ZV9cUserPassword = "" ;
   this.OV9cUserPassword = "" ;
   this.ZV5LinkSelection = "" ;
   this.OV5LinkSelection = "" ;
   this.Z9UserId = 0 ;
   this.O9UserId = 0 ;
   this.Z10UserName = "" ;
   this.O10UserName = "" ;
   this.AV6cUserId = 0 ;
   this.AV7cUserName = "" ;
   this.AV8cUserNickName = "" ;
   this.AV9cUserPassword = "" ;
   this.AV10pUserId = 0 ;
   this.A12UserPassword = "" ;
   this.A11UserNickName = "" ;
   this.AV5LinkSelection = "" ;
   this.A9UserId = 0 ;
   this.A10UserName = "" ;
   this.Events = {"e18082_client": ["ENTER", true] ,"e19081_client": ["CANCEL", true] ,"e15081_client": ["'TOGGLE'", false] ,"e11081_client": ["LBLUSERIDFILTER.CLICK", false] ,"e12081_client": ["LBLUSERNAMEFILTER.CLICK", false] ,"e13081_client": ["LBLUSERNICKNAMEFILTER.CLICK", false] ,"e14081_client": ["LBLUSERPASSWORDFILTER.CLICK", false]};
   this.EvtParms["REFRESH"] = [[{av:'GRID1_nFirstRecordOnPage'},{av:'GRID1_nEOF'},{ctrl:'GRID1',prop:'Rows'},{av:'AV6cUserId',fld:'vCUSERID',pic:'ZZZ9'},{av:'AV7cUserName',fld:'vCUSERNAME',pic:''},{av:'AV8cUserNickName',fld:'vCUSERNICKNAME',pic:''},{av:'AV9cUserPassword',fld:'vCUSERPASSWORD',pic:''}],[]];
   this.EvtParms["START"] = [[],[{ctrl:'FORM',prop:'Caption'}]];
   this.EvtParms["'TOGGLE'"] = [[{av:'gx.fn.getCtrlProperty("ADVANCEDCONTAINER","Class")',ctrl:'ADVANCEDCONTAINER',prop:'Class'},{ctrl:'BTNTOGGLE',prop:'Class'}],[{av:'gx.fn.getCtrlProperty("ADVANCEDCONTAINER","Class")',ctrl:'ADVANCEDCONTAINER',prop:'Class'},{ctrl:'BTNTOGGLE',prop:'Class'}]];
   this.EvtParms["LBLUSERIDFILTER.CLICK"] = [[{av:'gx.fn.getCtrlProperty("USERIDFILTERCONTAINER","Class")',ctrl:'USERIDFILTERCONTAINER',prop:'Class'}],[{av:'gx.fn.getCtrlProperty("USERIDFILTERCONTAINER","Class")',ctrl:'USERIDFILTERCONTAINER',prop:'Class'},{av:'gx.fn.getCtrlProperty("vCUSERID","Visible")',ctrl:'vCUSERID',prop:'Visible'}]];
   this.EvtParms["LBLUSERNAMEFILTER.CLICK"] = [[{av:'gx.fn.getCtrlProperty("USERNAMEFILTERCONTAINER","Class")',ctrl:'USERNAMEFILTERCONTAINER',prop:'Class'}],[{av:'gx.fn.getCtrlProperty("USERNAMEFILTERCONTAINER","Class")',ctrl:'USERNAMEFILTERCONTAINER',prop:'Class'},{av:'gx.fn.getCtrlProperty("vCUSERNAME","Visible")',ctrl:'vCUSERNAME',prop:'Visible'}]];
   this.EvtParms["LBLUSERNICKNAMEFILTER.CLICK"] = [[{av:'gx.fn.getCtrlProperty("USERNICKNAMEFILTERCONTAINER","Class")',ctrl:'USERNICKNAMEFILTERCONTAINER',prop:'Class'}],[{av:'gx.fn.getCtrlProperty("USERNICKNAMEFILTERCONTAINER","Class")',ctrl:'USERNICKNAMEFILTERCONTAINER',prop:'Class'},{av:'gx.fn.getCtrlProperty("vCUSERNICKNAME","Visible")',ctrl:'vCUSERNICKNAME',prop:'Visible'}]];
   this.EvtParms["LBLUSERPASSWORDFILTER.CLICK"] = [[{av:'gx.fn.getCtrlProperty("USERPASSWORDFILTERCONTAINER","Class")',ctrl:'USERPASSWORDFILTERCONTAINER',prop:'Class'}],[{av:'gx.fn.getCtrlProperty("USERPASSWORDFILTERCONTAINER","Class")',ctrl:'USERPASSWORDFILTERCONTAINER',prop:'Class'},{av:'gx.fn.getCtrlProperty("vCUSERPASSWORD","Visible")',ctrl:'vCUSERPASSWORD',prop:'Visible'}]];
   this.EvtParms["LOAD"] = [[],[{av:'AV5LinkSelection',fld:'vLINKSELECTION',pic:''}]];
   this.EvtParms["ENTER"] = [[{av:'A9UserId',fld:'USERID',pic:'ZZZ9',hsh:true}],[{av:'AV10pUserId',fld:'vPUSERID',pic:'ZZZ9'}]];
   this.EvtParms["GRID1_FIRSTPAGE"] = [[{av:'GRID1_nFirstRecordOnPage'},{av:'GRID1_nEOF'},{ctrl:'GRID1',prop:'Rows'},{av:'AV6cUserId',fld:'vCUSERID',pic:'ZZZ9'},{av:'AV7cUserName',fld:'vCUSERNAME',pic:''},{av:'AV8cUserNickName',fld:'vCUSERNICKNAME',pic:''},{av:'AV9cUserPassword',fld:'vCUSERPASSWORD',pic:''}],[]];
   this.EvtParms["GRID1_PREVPAGE"] = [[{av:'GRID1_nFirstRecordOnPage'},{av:'GRID1_nEOF'},{ctrl:'GRID1',prop:'Rows'},{av:'AV6cUserId',fld:'vCUSERID',pic:'ZZZ9'},{av:'AV7cUserName',fld:'vCUSERNAME',pic:''},{av:'AV8cUserNickName',fld:'vCUSERNICKNAME',pic:''},{av:'AV9cUserPassword',fld:'vCUSERPASSWORD',pic:''}],[]];
   this.EvtParms["GRID1_NEXTPAGE"] = [[{av:'GRID1_nFirstRecordOnPage'},{av:'GRID1_nEOF'},{ctrl:'GRID1',prop:'Rows'},{av:'AV6cUserId',fld:'vCUSERID',pic:'ZZZ9'},{av:'AV7cUserName',fld:'vCUSERNAME',pic:''},{av:'AV8cUserNickName',fld:'vCUSERNICKNAME',pic:''},{av:'AV9cUserPassword',fld:'vCUSERPASSWORD',pic:''}],[]];
   this.EvtParms["GRID1_LASTPAGE"] = [[{av:'GRID1_nFirstRecordOnPage'},{av:'GRID1_nEOF'},{ctrl:'GRID1',prop:'Rows'},{av:'AV6cUserId',fld:'vCUSERID',pic:'ZZZ9'},{av:'AV7cUserName',fld:'vCUSERNAME',pic:''},{av:'AV8cUserNickName',fld:'vCUSERNICKNAME',pic:''},{av:'AV9cUserPassword',fld:'vCUSERPASSWORD',pic:''}],[]];
   this.setVCMap("AV10pUserId", "vPUSERID", 0, "int", 4, 0);
   Grid1Container.addRefreshingParm({rfrProp:"Rows", gxGrid:"Grid1"});
   Grid1Container.addRefreshingVar(this.GXValidFnc[16]);
   Grid1Container.addRefreshingVar(this.GXValidFnc[26]);
   Grid1Container.addRefreshingVar(this.GXValidFnc[36]);
   Grid1Container.addRefreshingVar(this.GXValidFnc[46]);
   Grid1Container.addRefreshingParm(this.GXValidFnc[16]);
   Grid1Container.addRefreshingParm(this.GXValidFnc[26]);
   Grid1Container.addRefreshingParm(this.GXValidFnc[36]);
   Grid1Container.addRefreshingParm(this.GXValidFnc[46]);
   this.Initialize( );
});
gx.wi( function() { gx.createParentObj(this.gx0030);});
