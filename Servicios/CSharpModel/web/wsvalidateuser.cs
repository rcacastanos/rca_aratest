using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wsvalidateuser : GXProcedure
   {
      public wsvalidateuser( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public wsvalidateuser( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( string aP0_UserNickName ,
                           string aP1_UserPassword ,
                           out bool aP2_IsAccess ,
                           out short aP3_UserId )
      {
         this.AV8UserNickName = aP0_UserNickName;
         this.AV9UserPassword = aP1_UserPassword;
         this.AV10IsAccess = false ;
         this.AV12UserId = 0 ;
         initialize();
         executePrivate();
         aP2_IsAccess=this.AV10IsAccess;
         aP3_UserId=this.AV12UserId;
      }

      public short executeUdp( string aP0_UserNickName ,
                               string aP1_UserPassword ,
                               out bool aP2_IsAccess )
      {
         execute(aP0_UserNickName, aP1_UserPassword, out aP2_IsAccess, out aP3_UserId);
         return AV12UserId ;
      }

      public void executeSubmit( string aP0_UserNickName ,
                                 string aP1_UserPassword ,
                                 out bool aP2_IsAccess ,
                                 out short aP3_UserId )
      {
         wsvalidateuser objwsvalidateuser;
         objwsvalidateuser = new wsvalidateuser();
         objwsvalidateuser.AV8UserNickName = aP0_UserNickName;
         objwsvalidateuser.AV9UserPassword = aP1_UserPassword;
         objwsvalidateuser.AV10IsAccess = false ;
         objwsvalidateuser.AV12UserId = 0 ;
         objwsvalidateuser.context.SetSubmitInitialConfig(context);
         objwsvalidateuser.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objwsvalidateuser);
         aP2_IsAccess=this.AV10IsAccess;
         aP3_UserId=this.AV12UserId;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((wsvalidateuser)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV10IsAccess = false;
         /* Using cursor P000D2 */
         pr_default.execute(0, new Object[] {AV8UserNickName, AV9UserPassword});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A12UserPassword = P000D2_A12UserPassword[0];
            A11UserNickName = P000D2_A11UserNickName[0];
            A10UserName = P000D2_A10UserName[0];
            A9UserId = P000D2_A9UserId[0];
            AV12UserId = A9UserId;
            AV11UserName = A10UserName;
            AV10IsAccess = true;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections();
         }
         ExitApp();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P000D2_A12UserPassword = new string[] {""} ;
         P000D2_A11UserNickName = new string[] {""} ;
         P000D2_A10UserName = new string[] {""} ;
         P000D2_A9UserId = new short[1] ;
         A12UserPassword = "";
         A11UserNickName = "";
         A10UserName = "";
         AV11UserName = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wsvalidateuser__default(),
            new Object[][] {
                new Object[] {
               P000D2_A12UserPassword, P000D2_A11UserNickName, P000D2_A10UserName, P000D2_A9UserId
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV12UserId ;
      private short A9UserId ;
      private string scmdbuf ;
      private bool AV10IsAccess ;
      private string AV8UserNickName ;
      private string AV9UserPassword ;
      private string A12UserPassword ;
      private string A11UserNickName ;
      private string A10UserName ;
      private string AV11UserName ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private string[] P000D2_A12UserPassword ;
      private string[] P000D2_A11UserNickName ;
      private string[] P000D2_A10UserName ;
      private short[] P000D2_A9UserId ;
      private bool aP2_IsAccess ;
      private short aP3_UserId ;
   }

   public class wsvalidateuser__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP000D2;
          prmP000D2 = new Object[] {
          new ParDef("@AV8UserNickName",GXType.VarChar,40,0) ,
          new ParDef("@AV9UserPassword",GXType.VarChar,40,0)
          };
          def= new CursorDef[] {
              new CursorDef("P000D2", "SELECT [UserPassword], [UserNickName], [UserName], [UserId] FROM [User] WHERE (RTRIM(LTRIM(UPPER([UserNickName]))) = RTRIM(LTRIM(UPPER(@AV8UserNickName)))) AND (RTRIM(LTRIM(LOWER([UserPassword]))) = RTRIM(LTRIM(LOWER(@AV9UserPassword)))) ORDER BY [UserId] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP000D2,100, GxCacheFrequency.OFF ,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((string[]) buf[0])[0] = rslt.getVarchar(1);
                ((string[]) buf[1])[0] = rslt.getVarchar(2);
                ((string[]) buf[2])[0] = rslt.getVarchar(3);
                ((short[]) buf[3])[0] = rslt.getShort(4);
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.wsvalidateuser_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class wsvalidateuser_services : GxRestService
 {
    [OperationContract(Name = "WSValidateUser" )]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( string UserNickName ,
                         string UserPassword ,
                         out bool IsAccess ,
                         out short UserId )
    {
       IsAccess = false ;
       UserId = 0 ;
       try
       {
          if ( ! ProcessHeaders("wsvalidateuser") )
          {
             return  ;
          }
          wsvalidateuser worker = new wsvalidateuser(context);
          worker.IsMain = RunAsMain ;
          worker.execute(UserNickName,UserPassword,out IsAccess,out UserId );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
