using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wstaskgetall : GXProcedure
   {
      public wstaskgetall( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public wstaskgetall( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( short aP0_UserId ,
                           out GXBaseCollection<SdtTasks_TasksItem> aP1_Tasks )
      {
         this.AV14UserId = aP0_UserId;
         this.AV12Tasks = new GXBaseCollection<SdtTasks_TasksItem>( context, "TasksItem", "Servicios") ;
         initialize();
         executePrivate();
         aP1_Tasks=this.AV12Tasks;
      }

      public GXBaseCollection<SdtTasks_TasksItem> executeUdp( short aP0_UserId )
      {
         execute(aP0_UserId, out aP1_Tasks);
         return AV12Tasks ;
      }

      public void executeSubmit( short aP0_UserId ,
                                 out GXBaseCollection<SdtTasks_TasksItem> aP1_Tasks )
      {
         wstaskgetall objwstaskgetall;
         objwstaskgetall = new wstaskgetall();
         objwstaskgetall.AV14UserId = aP0_UserId;
         objwstaskgetall.AV12Tasks = new GXBaseCollection<SdtTasks_TasksItem>( context, "TasksItem", "Servicios") ;
         objwstaskgetall.context.SetSubmitInitialConfig(context);
         objwstaskgetall.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objwstaskgetall);
         aP1_Tasks=this.AV12Tasks;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((wstaskgetall)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P000J2 */
         pr_default.execute(0, new Object[] {AV14UserId});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A9UserId = P000J2_A9UserId[0];
            A5TaskId = P000J2_A5TaskId[0];
            A6TaskName = P000J2_A6TaskName[0];
            A7TaskDescription = P000J2_A7TaskDescription[0];
            A8TaskStatus = P000J2_A8TaskStatus[0];
            A14TaskCreateDate = P000J2_A14TaskCreateDate[0];
            AV13TasksItem = new SdtTasks_TasksItem(context);
            AV13TasksItem.gxTpr_Taskid = A5TaskId;
            AV13TasksItem.gxTpr_Taskname = StringUtil.Upper( A6TaskName);
            AV13TasksItem.gxTpr_Taskdescription = A7TaskDescription;
            AV13TasksItem.gxTpr_Taskcreatedate = A14TaskCreateDate;
            AV13TasksItem.gxTpr_Taskstatus = A8TaskStatus;
            AV13TasksItem.gxTpr_Userid = A9UserId;
            AV12Tasks.Add(AV13TasksItem, 0);
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections();
         }
         ExitApp();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV12Tasks = new GXBaseCollection<SdtTasks_TasksItem>( context, "TasksItem", "Servicios");
         scmdbuf = "";
         P000J2_A9UserId = new short[1] ;
         P000J2_A5TaskId = new short[1] ;
         P000J2_A6TaskName = new string[] {""} ;
         P000J2_A7TaskDescription = new string[] {""} ;
         P000J2_A8TaskStatus = new short[1] ;
         P000J2_A14TaskCreateDate = new DateTime[] {DateTime.MinValue} ;
         A6TaskName = "";
         A7TaskDescription = "";
         A14TaskCreateDate = (DateTime)(DateTime.MinValue);
         AV13TasksItem = new SdtTasks_TasksItem(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wstaskgetall__default(),
            new Object[][] {
                new Object[] {
               P000J2_A9UserId, P000J2_A5TaskId, P000J2_A6TaskName, P000J2_A7TaskDescription, P000J2_A8TaskStatus, P000J2_A14TaskCreateDate
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV14UserId ;
      private short A9UserId ;
      private short A5TaskId ;
      private short A8TaskStatus ;
      private string scmdbuf ;
      private DateTime A14TaskCreateDate ;
      private string A7TaskDescription ;
      private string A6TaskName ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private short[] P000J2_A9UserId ;
      private short[] P000J2_A5TaskId ;
      private string[] P000J2_A6TaskName ;
      private string[] P000J2_A7TaskDescription ;
      private short[] P000J2_A8TaskStatus ;
      private DateTime[] P000J2_A14TaskCreateDate ;
      private GXBaseCollection<SdtTasks_TasksItem> aP1_Tasks ;
      private GXBaseCollection<SdtTasks_TasksItem> AV12Tasks ;
      private SdtTasks_TasksItem AV13TasksItem ;
   }

   public class wstaskgetall__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP000J2;
          prmP000J2 = new Object[] {
          new ParDef("@AV14UserId",GXType.Int16,4,0)
          };
          def= new CursorDef[] {
              new CursorDef("P000J2", "SELECT [UserId], [TaskId], [TaskName], [TaskDescription], [TaskStatus], [TaskCreateDate] FROM [Task] WHERE [UserId] = @AV14UserId ORDER BY [TaskCreateDate] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP000J2,100, GxCacheFrequency.OFF ,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1);
                ((short[]) buf[1])[0] = rslt.getShort(2);
                ((string[]) buf[2])[0] = rslt.getVarchar(3);
                ((string[]) buf[3])[0] = rslt.getLongVarchar(4);
                ((short[]) buf[4])[0] = rslt.getShort(5);
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(6);
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.wstaskgetall_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class wstaskgetall_services : GxRestService
 {
    [OperationContract(Name = "WSTaskGetAll" )]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( short UserId ,
                         out GxGenericCollection<SdtTasks_TasksItem_RESTInterface> Tasks )
    {
       Tasks = new GxGenericCollection<SdtTasks_TasksItem_RESTInterface>() ;
       try
       {
          if ( ! ProcessHeaders("wstaskgetall") )
          {
             return  ;
          }
          wstaskgetall worker = new wstaskgetall(context);
          worker.IsMain = RunAsMain ;
          GXBaseCollection<SdtTasks_TasksItem> gxrTasks = new GXBaseCollection<SdtTasks_TasksItem>();
          worker.execute(UserId,out gxrTasks );
          worker.cleanup( );
          Tasks = new GxGenericCollection<SdtTasks_TasksItem_RESTInterface>(gxrTasks) ;
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
