using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wstaskupdate : GXProcedure
   {
      public wstaskupdate( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public wstaskupdate( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( short aP0_TaskId ,
                           string aP1_TaskName ,
                           string aP2_TaskDescription ,
                           short aP3_TaskStatus )
      {
         this.AV11TaskId = aP0_TaskId;
         this.AV8TaskName = aP1_TaskName;
         this.AV9TaskDescription = aP2_TaskDescription;
         this.AV10TaskStatus = aP3_TaskStatus;
         initialize();
         executePrivate();
      }

      public void executeSubmit( short aP0_TaskId ,
                                 string aP1_TaskName ,
                                 string aP2_TaskDescription ,
                                 short aP3_TaskStatus )
      {
         wstaskupdate objwstaskupdate;
         objwstaskupdate = new wstaskupdate();
         objwstaskupdate.AV11TaskId = aP0_TaskId;
         objwstaskupdate.AV8TaskName = aP1_TaskName;
         objwstaskupdate.AV9TaskDescription = aP2_TaskDescription;
         objwstaskupdate.AV10TaskStatus = aP3_TaskStatus;
         objwstaskupdate.context.SetSubmitInitialConfig(context);
         objwstaskupdate.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objwstaskupdate);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((wstaskupdate)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Optimized UPDATE. */
         /* Using cursor P000H2 */
         pr_default.execute(0, new Object[] {AV10TaskStatus, AV9TaskDescription, AV8TaskName, AV11TaskId});
         pr_default.close(0);
         dsDefault.SmartCacheProvider.SetUpdated("Task");
         /* End optimized UPDATE. */
         context.CommitDataStores("wstaskupdate",pr_default);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections();
         }
         ExitApp();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         A7TaskDescription = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wstaskupdate__default(),
            new Object[][] {
                new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV11TaskId ;
      private short AV10TaskStatus ;
      private short A8TaskStatus ;
      private string AV9TaskDescription ;
      private string A7TaskDescription ;
      private string AV8TaskName ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
   }

   public class wstaskupdate__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new UpdateCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP000H2;
          prmP000H2 = new Object[] {
          new ParDef("@TaskStatus",GXType.Int16,4,0) ,
          new ParDef("@TaskDescription",GXType.NVarChar,2097152,0) ,
          new ParDef("@AV8TaskName",GXType.VarChar,40,0) ,
          new ParDef("@AV11TaskId",GXType.Int16,4,0)
          };
          def= new CursorDef[] {
              new CursorDef("P000H2", "UPDATE [Task] SET [TaskStatus]=@TaskStatus, [TaskDescription]=@TaskDescription, [TaskName]=UPPER(@AV8TaskName)  WHERE [TaskId] = @AV11TaskId", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP000H2)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.wstaskupdate_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class wstaskupdate_services : GxRestService
 {
    [OperationContract(Name = "WSTaskUpdate" )]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( short TaskId ,
                         string TaskName ,
                         string TaskDescription ,
                         short TaskStatus )
    {
       try
       {
          if ( ! ProcessHeaders("wstaskupdate") )
          {
             return  ;
          }
          wstaskupdate worker = new wstaskupdate(context);
          worker.IsMain = RunAsMain ;
          worker.execute(TaskId,TaskName,TaskDescription,TaskStatus );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
