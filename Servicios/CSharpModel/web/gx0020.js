gx.evt.autoSkip = false;
gx.define('gx0020', false, function () {
   this.ServerClass =  "gx0020" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.ServerFullClass =  "gx0020.aspx" ;
   this.setObjectType("web");
   this.anyGridBaseTable = true;
   this.hasEnterEvent = true;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.ajaxSecurityToken =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV10pTaskId=gx.fn.getIntegerValue("vPTASKID",'.') ;
   };
   this.Validv_Ctaskcreatedate=function()
   {
      return this.validCliEvt("Validv_Ctaskcreatedate", 0, function () {
      try {
         var gxballoon = gx.util.balloon.getNew("vCTASKCREATEDATE");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV9cTaskCreateDate)===0) || new gx.date.gxdate( this.AV9cTaskCreateDate ).compare( gx.date.ymdhmstot( 1753, 1, 1, 0, 0, 0) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Task Create Date fuera de rango");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
          if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
      });
   }
   this.e16071_client=function()
   {
      this.clearMessages();
      if ( gx.text.compare( gx.fn.getCtrlProperty("ADVANCEDCONTAINER","Class") , "AdvancedContainer" ) == 0 )
      {
         gx.fn.setCtrlProperty("ADVANCEDCONTAINER","Class", "AdvancedContainer"+" "+"AdvancedContainerVisible" );
         gx.fn.setCtrlProperty("BTNTOGGLE","Class", gx.fn.getCtrlProperty("BTNTOGGLE","Class")+" "+"BtnToggleActive" );
      }
      else
      {
         gx.fn.setCtrlProperty("ADVANCEDCONTAINER","Class", "AdvancedContainer" );
         gx.fn.setCtrlProperty("BTNTOGGLE","Class", "BtnToggle" );
      }
      this.refreshOutputs([{av:'gx.fn.getCtrlProperty("ADVANCEDCONTAINER","Class")',ctrl:'ADVANCEDCONTAINER',prop:'Class'},{ctrl:'BTNTOGGLE',prop:'Class'}]);
      this.OnClientEventEnd();
      return gx.$.Deferred().resolve();
   };
   this.e11071_client=function()
   {
      this.clearMessages();
      if ( gx.text.compare( gx.fn.getCtrlProperty("TASKIDFILTERCONTAINER","Class") , "AdvancedContainerItem" ) == 0 )
      {
         gx.fn.setCtrlProperty("TASKIDFILTERCONTAINER","Class", "AdvancedContainerItem"+" "+"AdvancedContainerItemExpanded" );
         gx.fn.setCtrlProperty("vCTASKID","Visible", true );
      }
      else
      {
         gx.fn.setCtrlProperty("TASKIDFILTERCONTAINER","Class", "AdvancedContainerItem" );
         gx.fn.setCtrlProperty("vCTASKID","Visible", false );
      }
      this.refreshOutputs([{av:'gx.fn.getCtrlProperty("TASKIDFILTERCONTAINER","Class")',ctrl:'TASKIDFILTERCONTAINER',prop:'Class'},{av:'gx.fn.getCtrlProperty("vCTASKID","Visible")',ctrl:'vCTASKID',prop:'Visible'}]);
      this.OnClientEventEnd();
      return gx.$.Deferred().resolve();
   };
   this.e12071_client=function()
   {
      this.clearMessages();
      if ( gx.text.compare( gx.fn.getCtrlProperty("TASKNAMEFILTERCONTAINER","Class") , "AdvancedContainerItem" ) == 0 )
      {
         gx.fn.setCtrlProperty("TASKNAMEFILTERCONTAINER","Class", "AdvancedContainerItem"+" "+"AdvancedContainerItemExpanded" );
         gx.fn.setCtrlProperty("vCTASKNAME","Visible", true );
      }
      else
      {
         gx.fn.setCtrlProperty("TASKNAMEFILTERCONTAINER","Class", "AdvancedContainerItem" );
         gx.fn.setCtrlProperty("vCTASKNAME","Visible", false );
      }
      this.refreshOutputs([{av:'gx.fn.getCtrlProperty("TASKNAMEFILTERCONTAINER","Class")',ctrl:'TASKNAMEFILTERCONTAINER',prop:'Class'},{av:'gx.fn.getCtrlProperty("vCTASKNAME","Visible")',ctrl:'vCTASKNAME',prop:'Visible'}]);
      this.OnClientEventEnd();
      return gx.$.Deferred().resolve();
   };
   this.e13071_client=function()
   {
      this.clearMessages();
      if ( gx.text.compare( gx.fn.getCtrlProperty("TASKSTATUSFILTERCONTAINER","Class") , "AdvancedContainerItem" ) == 0 )
      {
         gx.fn.setCtrlProperty("TASKSTATUSFILTERCONTAINER","Class", "AdvancedContainerItem"+" "+"AdvancedContainerItemExpanded" );
         gx.fn.setCtrlProperty("vCTASKSTATUS","Visible", true );
      }
      else
      {
         gx.fn.setCtrlProperty("TASKSTATUSFILTERCONTAINER","Class", "AdvancedContainerItem" );
         gx.fn.setCtrlProperty("vCTASKSTATUS","Visible", false );
      }
      this.refreshOutputs([{av:'gx.fn.getCtrlProperty("TASKSTATUSFILTERCONTAINER","Class")',ctrl:'TASKSTATUSFILTERCONTAINER',prop:'Class'},{ctrl:'vCTASKSTATUS'}]);
      this.OnClientEventEnd();
      return gx.$.Deferred().resolve();
   };
   this.e14071_client=function()
   {
      this.clearMessages();
      if ( gx.text.compare( gx.fn.getCtrlProperty("TASKCREATEDATEFILTERCONTAINER","Class") , "AdvancedContainerItem" ) == 0 )
      {
         gx.fn.setCtrlProperty("TASKCREATEDATEFILTERCONTAINER","Class", "AdvancedContainerItem"+" "+"AdvancedContainerItemExpanded" );
      }
      else
      {
         gx.fn.setCtrlProperty("TASKCREATEDATEFILTERCONTAINER","Class", "AdvancedContainerItem" );
      }
      this.refreshOutputs([{av:'gx.fn.getCtrlProperty("TASKCREATEDATEFILTERCONTAINER","Class")',ctrl:'TASKCREATEDATEFILTERCONTAINER',prop:'Class'}]);
      this.OnClientEventEnd();
      return gx.$.Deferred().resolve();
   };
   this.e15071_client=function()
   {
      this.clearMessages();
      if ( gx.text.compare( gx.fn.getCtrlProperty("USERIDFILTERCONTAINER","Class") , "AdvancedContainerItem" ) == 0 )
      {
         gx.fn.setCtrlProperty("USERIDFILTERCONTAINER","Class", "AdvancedContainerItem"+" "+"AdvancedContainerItemExpanded" );
         gx.fn.setCtrlProperty("vCUSERID","Visible", true );
      }
      else
      {
         gx.fn.setCtrlProperty("USERIDFILTERCONTAINER","Class", "AdvancedContainerItem" );
         gx.fn.setCtrlProperty("vCUSERID","Visible", false );
      }
      this.refreshOutputs([{av:'gx.fn.getCtrlProperty("USERIDFILTERCONTAINER","Class")',ctrl:'USERIDFILTERCONTAINER',prop:'Class'},{av:'gx.fn.getCtrlProperty("vCUSERID","Visible")',ctrl:'vCUSERID',prop:'Visible'}]);
      this.OnClientEventEnd();
      return gx.$.Deferred().resolve();
   };
   this.e19072_client=function()
   {
      return this.executeServerEvent("ENTER", true, arguments[0], false, false);
   };
   this.e20071_client=function()
   {
      return this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,65,66,67,68,69,70,71,72,73];
   this.GXLastCtrlId =73;
   this.Grid1Container = new gx.grid.grid(this, 2,"WbpLvl2",64,"Grid1","Grid1","Grid1Container",this.CmpContext,this.IsMasterPage,"gx0020",[],false,1,false,true,10,true,false,false,"",0,"px",0,"px","Nueva fila",true,false,false,null,null,false,"",false,[1,1,1,1],false,0,true,false);
   var Grid1Container = this.Grid1Container;
   Grid1Container.addBitmap("&Linkselection","vLINKSELECTION",65,0,"px",17,"px",null,"","","SelectionAttribute","WWActionColumn");
   Grid1Container.addSingleLineEdit(5,66,"TASKID","Id","","TaskId","int",0,"px",4,4,"right",null,[],5,"TaskId",true,0,false,false,"Attribute",1,"WWColumn");
   Grid1Container.addSingleLineEdit(6,67,"TASKNAME","Name","","TaskName","svchar",0,"px",40,40,"left",null,[],6,"TaskName",true,0,false,false,"DescriptionAttribute",1,"WWColumn");
   Grid1Container.addComboBox(8,68,"TASKSTATUS","Status","TaskStatus","int",null,0,true,false,0,"px","WWColumn OptionalColumn");
   Grid1Container.addSingleLineEdit(14,69,"TASKCREATEDATE","Create Date","","TaskCreateDate","dtime",0,"px",14,14,"right",null,[],14,"TaskCreateDate",true,5,false,false,"Attribute",1,"WWColumn OptionalColumn");
   Grid1Container.addSingleLineEdit(9,70,"USERID","User Id","","UserId","int",0,"px",4,4,"right",null,[],9,"UserId",true,0,false,false,"Attribute",1,"WWColumn OptionalColumn");
   this.Grid1Container.emptyText = "";
   this.setGrid(Grid1Container);
   GXValidFnc[2]={ id: 2, fld:"",grid:0};
   GXValidFnc[3]={ id: 3, fld:"MAIN",grid:0};
   GXValidFnc[4]={ id: 4, fld:"",grid:0};
   GXValidFnc[5]={ id: 5, fld:"",grid:0};
   GXValidFnc[6]={ id: 6, fld:"ADVANCEDCONTAINER",grid:0};
   GXValidFnc[7]={ id: 7, fld:"",grid:0};
   GXValidFnc[8]={ id: 8, fld:"",grid:0};
   GXValidFnc[9]={ id: 9, fld:"TASKIDFILTERCONTAINER",grid:0};
   GXValidFnc[10]={ id: 10, fld:"",grid:0};
   GXValidFnc[11]={ id: 11, fld:"",grid:0};
   GXValidFnc[12]={ id: 12, fld:"LBLTASKIDFILTER", format:1,grid:0,evt:"e11071_client", ctrltype: "textblock"};
   GXValidFnc[13]={ id: 13, fld:"",grid:0};
   GXValidFnc[14]={ id: 14, fld:"",grid:0};
   GXValidFnc[15]={ id: 15, fld:"",grid:0};
   GXValidFnc[16]={ id:16 ,lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[this.Grid1Container],fld:"vCTASKID",gxz:"ZV6cTaskId",gxold:"OV6cTaskId",gxvar:"AV6cTaskId",ucs:[],op:[],ip:[],
						nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV6cTaskId=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV6cTaskId=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vCTASKID",gx.O.AV6cTaskId,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV6cTaskId=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vCTASKID",'.')},nac:gx.falseFn};
   GXValidFnc[17]={ id: 17, fld:"",grid:0};
   GXValidFnc[18]={ id: 18, fld:"",grid:0};
   GXValidFnc[19]={ id: 19, fld:"TASKNAMEFILTERCONTAINER",grid:0};
   GXValidFnc[20]={ id: 20, fld:"",grid:0};
   GXValidFnc[21]={ id: 21, fld:"",grid:0};
   GXValidFnc[22]={ id: 22, fld:"LBLTASKNAMEFILTER", format:1,grid:0,evt:"e12071_client", ctrltype: "textblock"};
   GXValidFnc[23]={ id: 23, fld:"",grid:0};
   GXValidFnc[24]={ id: 24, fld:"",grid:0};
   GXValidFnc[25]={ id: 25, fld:"",grid:0};
   GXValidFnc[26]={ id:26 ,lvl:0,type:"svchar",len:40,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[this.Grid1Container],fld:"vCTASKNAME",gxz:"ZV7cTaskName",gxold:"OV7cTaskName",gxvar:"AV7cTaskName",ucs:[],op:[],ip:[],
						nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV7cTaskName=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV7cTaskName=Value},v2c:function(){gx.fn.setControlValue("vCTASKNAME",gx.O.AV7cTaskName,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV7cTaskName=this.val()},val:function(){return gx.fn.getControlValue("vCTASKNAME")},nac:gx.falseFn};
   GXValidFnc[27]={ id: 27, fld:"",grid:0};
   GXValidFnc[28]={ id: 28, fld:"",grid:0};
   GXValidFnc[29]={ id: 29, fld:"TASKSTATUSFILTERCONTAINER",grid:0};
   GXValidFnc[30]={ id: 30, fld:"",grid:0};
   GXValidFnc[31]={ id: 31, fld:"",grid:0};
   GXValidFnc[32]={ id: 32, fld:"LBLTASKSTATUSFILTER", format:1,grid:0,evt:"e13071_client", ctrltype: "textblock"};
   GXValidFnc[33]={ id: 33, fld:"",grid:0};
   GXValidFnc[34]={ id: 34, fld:"",grid:0};
   GXValidFnc[35]={ id: 35, fld:"",grid:0};
   GXValidFnc[36]={ id:36 ,lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[this.Grid1Container],fld:"vCTASKSTATUS",gxz:"ZV8cTaskStatus",gxold:"OV8cTaskStatus",gxvar:"AV8cTaskStatus",ucs:[],op:[],ip:[],
						nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV8cTaskStatus=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV8cTaskStatus=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vCTASKSTATUS",gx.O.AV8cTaskStatus)},c2v:function(){if(this.val()!==undefined)gx.O.AV8cTaskStatus=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vCTASKSTATUS",'.')},nac:gx.falseFn};
   GXValidFnc[37]={ id: 37, fld:"",grid:0};
   GXValidFnc[38]={ id: 38, fld:"",grid:0};
   GXValidFnc[39]={ id: 39, fld:"TASKCREATEDATEFILTERCONTAINER",grid:0};
   GXValidFnc[40]={ id: 40, fld:"",grid:0};
   GXValidFnc[41]={ id: 41, fld:"",grid:0};
   GXValidFnc[42]={ id: 42, fld:"LBLTASKCREATEDATEFILTER", format:1,grid:0,evt:"e14071_client", ctrltype: "textblock"};
   GXValidFnc[43]={ id: 43, fld:"",grid:0};
   GXValidFnc[44]={ id: 44, fld:"",grid:0};
   GXValidFnc[45]={ id: 45, fld:"",grid:0};
   GXValidFnc[46]={ id:46 ,lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ctaskcreatedate,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[this.Grid1Container],fld:"vCTASKCREATEDATE",gxz:"ZV9cTaskCreateDate",gxold:"OV9cTaskCreateDate",gxvar:"AV9cTaskCreateDate",dp:{f:-1,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[46],ip:[46],
						nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV9cTaskCreateDate=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV9cTaskCreateDate=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vCTASKCREATEDATE",gx.O.AV9cTaskCreateDate,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV9cTaskCreateDate=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vCTASKCREATEDATE")},nac:gx.falseFn};
   GXValidFnc[47]={ id: 47, fld:"",grid:0};
   GXValidFnc[48]={ id: 48, fld:"",grid:0};
   GXValidFnc[49]={ id: 49, fld:"USERIDFILTERCONTAINER",grid:0};
   GXValidFnc[50]={ id: 50, fld:"",grid:0};
   GXValidFnc[51]={ id: 51, fld:"",grid:0};
   GXValidFnc[52]={ id: 52, fld:"LBLUSERIDFILTER", format:1,grid:0,evt:"e15071_client", ctrltype: "textblock"};
   GXValidFnc[53]={ id: 53, fld:"",grid:0};
   GXValidFnc[54]={ id: 54, fld:"",grid:0};
   GXValidFnc[55]={ id: 55, fld:"",grid:0};
   GXValidFnc[56]={ id:56 ,lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[this.Grid1Container],fld:"vCUSERID",gxz:"ZV12cUserId",gxold:"OV12cUserId",gxvar:"AV12cUserId",ucs:[],op:[],ip:[],
						nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV12cUserId=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV12cUserId=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vCUSERID",gx.O.AV12cUserId,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV12cUserId=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vCUSERID",'.')},nac:gx.falseFn};
   GXValidFnc[57]={ id: 57, fld:"",grid:0};
   GXValidFnc[58]={ id: 58, fld:"GRIDTABLE",grid:0};
   GXValidFnc[59]={ id: 59, fld:"",grid:0};
   GXValidFnc[60]={ id: 60, fld:"",grid:0};
   GXValidFnc[61]={ id: 61, fld:"BTNTOGGLE",grid:0,evt:"e16071_client"};
   GXValidFnc[62]={ id: 62, fld:"",grid:0};
   GXValidFnc[63]={ id: 63, fld:"",grid:0};
   GXValidFnc[65]={ id:65 ,lvl:2,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:64,gxgrid:this.Grid1Container,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[],fld:"vLINKSELECTION",gxz:"ZV5LinkSelection",gxold:"OV5LinkSelection",gxvar:"AV5LinkSelection",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV5LinkSelection=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV5LinkSelection=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vLINKSELECTION",row || gx.fn.currentGridRowImpl(64),gx.O.AV5LinkSelection,gx.O.AV15Linkselection_GXI)},c2v:function(row){gx.O.AV15Linkselection_GXI=this.val_GXI();if(this.val(row)!==undefined)gx.O.AV5LinkSelection=this.val(row)},val:function(row){return gx.fn.getGridControlValue("vLINKSELECTION",row || gx.fn.currentGridRowImpl(64))},val_GXI:function(row){return gx.fn.getGridControlValue("vLINKSELECTION_GXI",row || gx.fn.currentGridRowImpl(64))}, gxvar_GXI:'AV15Linkselection_GXI',nac:gx.falseFn};
   GXValidFnc[66]={ id:66 ,lvl:2,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:1,isacc:0,grid:64,gxgrid:this.Grid1Container,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[],fld:"TASKID",gxz:"Z5TaskId",gxold:"O5TaskId",gxvar:"A5TaskId",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'number',v2v:function(Value){if(Value!==undefined)gx.O.A5TaskId=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z5TaskId=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("TASKID",row || gx.fn.currentGridRowImpl(64),gx.O.A5TaskId,0)},c2v:function(row){if(this.val(row)!==undefined)gx.O.A5TaskId=gx.num.intval(this.val(row))},val:function(row){return gx.fn.getGridIntegerValue("TASKID",row || gx.fn.currentGridRowImpl(64),'.')},nac:gx.falseFn};
   GXValidFnc[67]={ id:67 ,lvl:2,type:"svchar",len:40,dec:0,sign:false,ro:1,isacc:0,grid:64,gxgrid:this.Grid1Container,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[],fld:"TASKNAME",gxz:"Z6TaskName",gxold:"O6TaskName",gxvar:"A6TaskName",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.A6TaskName=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z6TaskName=Value},v2c:function(row){gx.fn.setGridControlValue("TASKNAME",row || gx.fn.currentGridRowImpl(64),gx.O.A6TaskName,0)},c2v:function(row){if(this.val(row)!==undefined)gx.O.A6TaskName=this.val(row)},val:function(row){return gx.fn.getGridControlValue("TASKNAME",row || gx.fn.currentGridRowImpl(64))},nac:gx.falseFn};
   GXValidFnc[68]={ id:68 ,lvl:2,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:1,isacc:0,grid:64,gxgrid:this.Grid1Container,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[],fld:"TASKSTATUS",gxz:"Z8TaskStatus",gxold:"O8TaskStatus",gxvar:"A8TaskStatus",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",inputType:'number',v2v:function(Value){if(Value!==undefined)gx.O.A8TaskStatus=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z8TaskStatus=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridComboBoxValue("TASKSTATUS",row || gx.fn.currentGridRowImpl(64),gx.O.A8TaskStatus)},c2v:function(row){if(this.val(row)!==undefined)gx.O.A8TaskStatus=gx.num.intval(this.val(row))},val:function(row){return gx.fn.getGridIntegerValue("TASKSTATUS",row || gx.fn.currentGridRowImpl(64),'.')},nac:gx.falseFn};
   GXValidFnc[69]={ id:69 ,lvl:2,type:"dtime",len:8,dec:5,sign:false,ro:1,isacc:0,grid:64,gxgrid:this.Grid1Container,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[],fld:"TASKCREATEDATE",gxz:"Z14TaskCreateDate",gxold:"O14TaskCreateDate",gxvar:"A14TaskCreateDate",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A14TaskCreateDate=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z14TaskCreateDate=gx.fn.toDatetimeValue(Value)},v2c:function(row){gx.fn.setGridControlValue("TASKCREATEDATE",row || gx.fn.currentGridRowImpl(64),gx.O.A14TaskCreateDate,0)},c2v:function(row){if(this.val(row)!==undefined)gx.O.A14TaskCreateDate=gx.fn.toDatetimeValue(this.val(row))},val:function(row){return gx.fn.getGridDateTimeValue("TASKCREATEDATE",row || gx.fn.currentGridRowImpl(64))},nac:gx.falseFn};
   GXValidFnc[70]={ id:70 ,lvl:2,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:1,isacc:0,grid:64,gxgrid:this.Grid1Container,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[],fld:"USERID",gxz:"Z9UserId",gxold:"O9UserId",gxvar:"A9UserId",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'number',v2v:function(Value){if(Value!==undefined)gx.O.A9UserId=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z9UserId=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("USERID",row || gx.fn.currentGridRowImpl(64),gx.O.A9UserId,0)},c2v:function(row){if(this.val(row)!==undefined)gx.O.A9UserId=gx.num.intval(this.val(row))},val:function(row){return gx.fn.getGridIntegerValue("USERID",row || gx.fn.currentGridRowImpl(64),'.')},nac:gx.falseFn};
   GXValidFnc[71]={ id: 71, fld:"",grid:0};
   GXValidFnc[72]={ id: 72, fld:"",grid:0};
   GXValidFnc[73]={ id: 73, fld:"BTN_CANCEL",grid:0,evt:"e20071_client"};
   this.AV6cTaskId = 0 ;
   this.ZV6cTaskId = 0 ;
   this.OV6cTaskId = 0 ;
   this.AV7cTaskName = "" ;
   this.ZV7cTaskName = "" ;
   this.OV7cTaskName = "" ;
   this.AV8cTaskStatus = 0 ;
   this.ZV8cTaskStatus = 0 ;
   this.OV8cTaskStatus = 0 ;
   this.AV9cTaskCreateDate = gx.date.nullDate() ;
   this.ZV9cTaskCreateDate = gx.date.nullDate() ;
   this.OV9cTaskCreateDate = gx.date.nullDate() ;
   this.AV12cUserId = 0 ;
   this.ZV12cUserId = 0 ;
   this.OV12cUserId = 0 ;
   this.ZV5LinkSelection = "" ;
   this.OV5LinkSelection = "" ;
   this.Z5TaskId = 0 ;
   this.O5TaskId = 0 ;
   this.Z6TaskName = "" ;
   this.O6TaskName = "" ;
   this.Z8TaskStatus = 0 ;
   this.O8TaskStatus = 0 ;
   this.Z14TaskCreateDate = gx.date.nullDate() ;
   this.O14TaskCreateDate = gx.date.nullDate() ;
   this.Z9UserId = 0 ;
   this.O9UserId = 0 ;
   this.AV6cTaskId = 0 ;
   this.AV7cTaskName = "" ;
   this.AV8cTaskStatus = 0 ;
   this.AV9cTaskCreateDate = gx.date.nullDate() ;
   this.AV12cUserId = 0 ;
   this.AV10pTaskId = 0 ;
   this.AV5LinkSelection = "" ;
   this.A5TaskId = 0 ;
   this.A6TaskName = "" ;
   this.A8TaskStatus = 0 ;
   this.A14TaskCreateDate = gx.date.nullDate() ;
   this.A9UserId = 0 ;
   this.Events = {"e19072_client": ["ENTER", true] ,"e20071_client": ["CANCEL", true] ,"e16071_client": ["'TOGGLE'", false] ,"e11071_client": ["LBLTASKIDFILTER.CLICK", false] ,"e12071_client": ["LBLTASKNAMEFILTER.CLICK", false] ,"e13071_client": ["LBLTASKSTATUSFILTER.CLICK", false] ,"e14071_client": ["LBLTASKCREATEDATEFILTER.CLICK", false] ,"e15071_client": ["LBLUSERIDFILTER.CLICK", false]};
   this.EvtParms["REFRESH"] = [[{av:'GRID1_nFirstRecordOnPage'},{av:'GRID1_nEOF'},{ctrl:'GRID1',prop:'Rows'},{av:'AV6cTaskId',fld:'vCTASKID',pic:'ZZZ9'},{av:'AV7cTaskName',fld:'vCTASKNAME',pic:''},{ctrl:'vCTASKSTATUS'},{av:'AV8cTaskStatus',fld:'vCTASKSTATUS',pic:'ZZZ9'},{av:'AV9cTaskCreateDate',fld:'vCTASKCREATEDATE',pic:'99/99/99 99:99'},{av:'AV12cUserId',fld:'vCUSERID',pic:'ZZZ9'}],[]];
   this.EvtParms["START"] = [[],[{ctrl:'FORM',prop:'Caption'}]];
   this.EvtParms["'TOGGLE'"] = [[{av:'gx.fn.getCtrlProperty("ADVANCEDCONTAINER","Class")',ctrl:'ADVANCEDCONTAINER',prop:'Class'},{ctrl:'BTNTOGGLE',prop:'Class'}],[{av:'gx.fn.getCtrlProperty("ADVANCEDCONTAINER","Class")',ctrl:'ADVANCEDCONTAINER',prop:'Class'},{ctrl:'BTNTOGGLE',prop:'Class'}]];
   this.EvtParms["LBLTASKIDFILTER.CLICK"] = [[{av:'gx.fn.getCtrlProperty("TASKIDFILTERCONTAINER","Class")',ctrl:'TASKIDFILTERCONTAINER',prop:'Class'}],[{av:'gx.fn.getCtrlProperty("TASKIDFILTERCONTAINER","Class")',ctrl:'TASKIDFILTERCONTAINER',prop:'Class'},{av:'gx.fn.getCtrlProperty("vCTASKID","Visible")',ctrl:'vCTASKID',prop:'Visible'}]];
   this.EvtParms["LBLTASKNAMEFILTER.CLICK"] = [[{av:'gx.fn.getCtrlProperty("TASKNAMEFILTERCONTAINER","Class")',ctrl:'TASKNAMEFILTERCONTAINER',prop:'Class'}],[{av:'gx.fn.getCtrlProperty("TASKNAMEFILTERCONTAINER","Class")',ctrl:'TASKNAMEFILTERCONTAINER',prop:'Class'},{av:'gx.fn.getCtrlProperty("vCTASKNAME","Visible")',ctrl:'vCTASKNAME',prop:'Visible'}]];
   this.EvtParms["LBLTASKSTATUSFILTER.CLICK"] = [[{av:'gx.fn.getCtrlProperty("TASKSTATUSFILTERCONTAINER","Class")',ctrl:'TASKSTATUSFILTERCONTAINER',prop:'Class'}],[{av:'gx.fn.getCtrlProperty("TASKSTATUSFILTERCONTAINER","Class")',ctrl:'TASKSTATUSFILTERCONTAINER',prop:'Class'},{ctrl:'vCTASKSTATUS'}]];
   this.EvtParms["LBLTASKCREATEDATEFILTER.CLICK"] = [[{av:'gx.fn.getCtrlProperty("TASKCREATEDATEFILTERCONTAINER","Class")',ctrl:'TASKCREATEDATEFILTERCONTAINER',prop:'Class'}],[{av:'gx.fn.getCtrlProperty("TASKCREATEDATEFILTERCONTAINER","Class")',ctrl:'TASKCREATEDATEFILTERCONTAINER',prop:'Class'}]];
   this.EvtParms["LBLUSERIDFILTER.CLICK"] = [[{av:'gx.fn.getCtrlProperty("USERIDFILTERCONTAINER","Class")',ctrl:'USERIDFILTERCONTAINER',prop:'Class'}],[{av:'gx.fn.getCtrlProperty("USERIDFILTERCONTAINER","Class")',ctrl:'USERIDFILTERCONTAINER',prop:'Class'},{av:'gx.fn.getCtrlProperty("vCUSERID","Visible")',ctrl:'vCUSERID',prop:'Visible'}]];
   this.EvtParms["LOAD"] = [[],[{av:'AV5LinkSelection',fld:'vLINKSELECTION',pic:''}]];
   this.EvtParms["ENTER"] = [[{av:'A5TaskId',fld:'TASKID',pic:'ZZZ9',hsh:true}],[{av:'AV10pTaskId',fld:'vPTASKID',pic:'ZZZ9'}]];
   this.EvtParms["GRID1_FIRSTPAGE"] = [[{av:'GRID1_nFirstRecordOnPage'},{av:'GRID1_nEOF'},{ctrl:'GRID1',prop:'Rows'},{av:'AV6cTaskId',fld:'vCTASKID',pic:'ZZZ9'},{av:'AV7cTaskName',fld:'vCTASKNAME',pic:''},{ctrl:'vCTASKSTATUS'},{av:'AV8cTaskStatus',fld:'vCTASKSTATUS',pic:'ZZZ9'},{av:'AV9cTaskCreateDate',fld:'vCTASKCREATEDATE',pic:'99/99/99 99:99'},{av:'AV12cUserId',fld:'vCUSERID',pic:'ZZZ9'}],[]];
   this.EvtParms["GRID1_PREVPAGE"] = [[{av:'GRID1_nFirstRecordOnPage'},{av:'GRID1_nEOF'},{ctrl:'GRID1',prop:'Rows'},{av:'AV6cTaskId',fld:'vCTASKID',pic:'ZZZ9'},{av:'AV7cTaskName',fld:'vCTASKNAME',pic:''},{ctrl:'vCTASKSTATUS'},{av:'AV8cTaskStatus',fld:'vCTASKSTATUS',pic:'ZZZ9'},{av:'AV9cTaskCreateDate',fld:'vCTASKCREATEDATE',pic:'99/99/99 99:99'},{av:'AV12cUserId',fld:'vCUSERID',pic:'ZZZ9'}],[]];
   this.EvtParms["GRID1_NEXTPAGE"] = [[{av:'GRID1_nFirstRecordOnPage'},{av:'GRID1_nEOF'},{ctrl:'GRID1',prop:'Rows'},{av:'AV6cTaskId',fld:'vCTASKID',pic:'ZZZ9'},{av:'AV7cTaskName',fld:'vCTASKNAME',pic:''},{ctrl:'vCTASKSTATUS'},{av:'AV8cTaskStatus',fld:'vCTASKSTATUS',pic:'ZZZ9'},{av:'AV9cTaskCreateDate',fld:'vCTASKCREATEDATE',pic:'99/99/99 99:99'},{av:'AV12cUserId',fld:'vCUSERID',pic:'ZZZ9'}],[]];
   this.EvtParms["GRID1_LASTPAGE"] = [[{av:'GRID1_nFirstRecordOnPage'},{av:'GRID1_nEOF'},{ctrl:'GRID1',prop:'Rows'},{av:'AV6cTaskId',fld:'vCTASKID',pic:'ZZZ9'},{av:'AV7cTaskName',fld:'vCTASKNAME',pic:''},{ctrl:'vCTASKSTATUS'},{av:'AV8cTaskStatus',fld:'vCTASKSTATUS',pic:'ZZZ9'},{av:'AV9cTaskCreateDate',fld:'vCTASKCREATEDATE',pic:'99/99/99 99:99'},{av:'AV12cUserId',fld:'vCUSERID',pic:'ZZZ9'}],[]];
   this.EvtParms["VALIDV_CTASKCREATEDATE"] = [[],[]];
   this.setVCMap("AV10pTaskId", "vPTASKID", 0, "int", 4, 0);
   Grid1Container.addRefreshingParm({rfrProp:"Rows", gxGrid:"Grid1"});
   Grid1Container.addRefreshingVar(this.GXValidFnc[16]);
   Grid1Container.addRefreshingVar(this.GXValidFnc[26]);
   Grid1Container.addRefreshingVar(this.GXValidFnc[36]);
   Grid1Container.addRefreshingVar(this.GXValidFnc[46]);
   Grid1Container.addRefreshingVar(this.GXValidFnc[56]);
   Grid1Container.addRefreshingParm(this.GXValidFnc[16]);
   Grid1Container.addRefreshingParm(this.GXValidFnc[26]);
   Grid1Container.addRefreshingParm(this.GXValidFnc[36]);
   Grid1Container.addRefreshingParm(this.GXValidFnc[46]);
   Grid1Container.addRefreshingParm(this.GXValidFnc[56]);
   this.Initialize( );
});
gx.wi( function() { gx.createParentObj(this.gx0020);});
