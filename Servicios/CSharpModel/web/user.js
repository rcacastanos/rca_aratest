gx.evt.autoSkip = false;
gx.define('user', false, function () {
   this.ServerClass =  "user" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.ServerFullClass =  "user.aspx" ;
   this.setObjectType("trn");
   this.hasEnterEvent = true;
   this.skipOnEnter = false;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.ajaxSecurityToken =  true ;
   this.SetStandaloneVars=function()
   {
   };
   this.Valid_Userid=function()
   {
      return this.validSrvEvt("Valid_Userid", 0).then((function (ret) {
      return ret;
      }).closure(this));
   }
   this.e11033_client=function()
   {
      return this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e12033_client=function()
   {
      return this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,3,4,5,6,7,8,9,10,11,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58];
   this.GXLastCtrlId =58;
   GXValidFnc[2]={ id: 2, fld:"",grid:0};
   GXValidFnc[3]={ id: 3, fld:"MAINTABLE",grid:0};
   GXValidFnc[4]={ id: 4, fld:"",grid:0};
   GXValidFnc[5]={ id: 5, fld:"",grid:0};
   GXValidFnc[6]={ id: 6, fld:"TITLECONTAINER",grid:0};
   GXValidFnc[7]={ id: 7, fld:"",grid:0};
   GXValidFnc[8]={ id: 8, fld:"",grid:0};
   GXValidFnc[9]={ id: 9, fld:"TITLE", format:0,grid:0, ctrltype: "textblock"};
   GXValidFnc[10]={ id: 10, fld:"",grid:0};
   GXValidFnc[11]={ id: 11, fld:"",grid:0};
   GXValidFnc[13]={ id: 13, fld:"",grid:0};
   GXValidFnc[14]={ id: 14, fld:"",grid:0};
   GXValidFnc[15]={ id: 15, fld:"FORMCONTAINER",grid:0};
   GXValidFnc[16]={ id: 16, fld:"",grid:0};
   GXValidFnc[17]={ id: 17, fld:"TOOLBARCELL",grid:0};
   GXValidFnc[18]={ id: 18, fld:"",grid:0};
   GXValidFnc[19]={ id: 19, fld:"",grid:0};
   GXValidFnc[20]={ id: 20, fld:"",grid:0};
   GXValidFnc[21]={ id: 21, fld:"BTN_FIRST",grid:0,evt:"e13033_client",std:"FIRST"};
   GXValidFnc[22]={ id: 22, fld:"",grid:0};
   GXValidFnc[23]={ id: 23, fld:"BTN_PREVIOUS",grid:0,evt:"e14033_client",std:"PREVIOUS"};
   GXValidFnc[24]={ id: 24, fld:"",grid:0};
   GXValidFnc[25]={ id: 25, fld:"BTN_NEXT",grid:0,evt:"e15033_client",std:"NEXT"};
   GXValidFnc[26]={ id: 26, fld:"",grid:0};
   GXValidFnc[27]={ id: 27, fld:"BTN_LAST",grid:0,evt:"e16033_client",std:"LAST"};
   GXValidFnc[28]={ id: 28, fld:"",grid:0};
   GXValidFnc[29]={ id: 29, fld:"BTN_SELECT",grid:0,evt:"e17033_client",std:"SELECT"};
   GXValidFnc[30]={ id: 30, fld:"",grid:0};
   GXValidFnc[31]={ id: 31, fld:"",grid:0};
   GXValidFnc[32]={ id: 32, fld:"",grid:0};
   GXValidFnc[33]={ id: 33, fld:"",grid:0};
   GXValidFnc[34]={ id:34 ,lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:this.Valid_Userid,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[],fld:"USERID",gxz:"Z9UserId",gxold:"O9UserId",gxvar:"A9UserId",ucs:[],op:[49,44,39],ip:[49,44,39,34],
						nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A9UserId=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z9UserId=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("USERID",gx.O.A9UserId,0)},c2v:function(){if(this.val()!==undefined)gx.O.A9UserId=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("USERID",'.')},nac:gx.falseFn};
   GXValidFnc[35]={ id: 35, fld:"",grid:0};
   GXValidFnc[36]={ id: 36, fld:"",grid:0};
   GXValidFnc[37]={ id: 37, fld:"",grid:0};
   GXValidFnc[38]={ id: 38, fld:"",grid:0};
   GXValidFnc[39]={ id:39 ,lvl:0,type:"svchar",len:100,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[],fld:"USERNAME",gxz:"Z10UserName",gxold:"O10UserName",gxvar:"A10UserName",ucs:[],op:[],ip:[],
						nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A10UserName=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z10UserName=Value},v2c:function(){gx.fn.setControlValue("USERNAME",gx.O.A10UserName,0)},c2v:function(){if(this.val()!==undefined)gx.O.A10UserName=this.val()},val:function(){return gx.fn.getControlValue("USERNAME")},nac:gx.falseFn};
   GXValidFnc[40]={ id: 40, fld:"",grid:0};
   GXValidFnc[41]={ id: 41, fld:"",grid:0};
   GXValidFnc[42]={ id: 42, fld:"",grid:0};
   GXValidFnc[43]={ id: 43, fld:"",grid:0};
   GXValidFnc[44]={ id:44 ,lvl:0,type:"svchar",len:40,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[],fld:"USERNICKNAME",gxz:"Z11UserNickName",gxold:"O11UserNickName",gxvar:"A11UserNickName",ucs:[],op:[],ip:[],
						nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A11UserNickName=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z11UserNickName=Value},v2c:function(){gx.fn.setControlValue("USERNICKNAME",gx.O.A11UserNickName,0)},c2v:function(){if(this.val()!==undefined)gx.O.A11UserNickName=this.val()},val:function(){return gx.fn.getControlValue("USERNICKNAME")},nac:gx.falseFn};
   GXValidFnc[45]={ id: 45, fld:"",grid:0};
   GXValidFnc[46]={ id: 46, fld:"",grid:0};
   GXValidFnc[47]={ id: 47, fld:"",grid:0};
   GXValidFnc[48]={ id: 48, fld:"",grid:0};
   GXValidFnc[49]={ id:49 ,lvl:0,type:"svchar",len:40,dec:0,sign:false,isPwd:true,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,evt_cvc:null,evt_cvcing:null,rgrid:[],fld:"USERPASSWORD",gxz:"Z12UserPassword",gxold:"O12UserPassword",gxvar:"A12UserPassword",ucs:[],op:[],ip:[],
						nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A12UserPassword=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z12UserPassword=Value},v2c:function(){gx.fn.setControlValue("USERPASSWORD",gx.O.A12UserPassword,0)},c2v:function(){if(this.val()!==undefined)gx.O.A12UserPassword=this.val()},val:function(){return gx.fn.getControlValue("USERPASSWORD")},nac:gx.falseFn};
   GXValidFnc[50]={ id: 50, fld:"",grid:0};
   GXValidFnc[51]={ id: 51, fld:"",grid:0};
   GXValidFnc[52]={ id: 52, fld:"",grid:0};
   GXValidFnc[53]={ id: 53, fld:"",grid:0};
   GXValidFnc[54]={ id: 54, fld:"BTN_ENTER",grid:0,evt:"e11033_client",std:"ENTER"};
   GXValidFnc[55]={ id: 55, fld:"",grid:0};
   GXValidFnc[56]={ id: 56, fld:"BTN_CANCEL",grid:0,evt:"e12033_client"};
   GXValidFnc[57]={ id: 57, fld:"",grid:0};
   GXValidFnc[58]={ id: 58, fld:"BTN_DELETE",grid:0,evt:"e18033_client",std:"DELETE"};
   this.A9UserId = 0 ;
   this.Z9UserId = 0 ;
   this.O9UserId = 0 ;
   this.A10UserName = "" ;
   this.Z10UserName = "" ;
   this.O10UserName = "" ;
   this.A11UserNickName = "" ;
   this.Z11UserNickName = "" ;
   this.O11UserNickName = "" ;
   this.A12UserPassword = "" ;
   this.Z12UserPassword = "" ;
   this.O12UserPassword = "" ;
   this.A9UserId = 0 ;
   this.A10UserName = "" ;
   this.A11UserNickName = "" ;
   this.A12UserPassword = "" ;
   this.Events = {"e11033_client": ["ENTER", true] ,"e12033_client": ["CANCEL", true]};
   this.EvtParms["ENTER"] = [[{postForm:true}],[]];
   this.EvtParms["REFRESH"] = [[],[]];
   this.EvtParms["VALID_USERID"] = [[{av:'A9UserId',fld:'USERID',pic:'ZZZ9'},{av:'Gx_mode',fld:'vMODE',pic:'@!'}],[{av:'A10UserName',fld:'USERNAME',pic:''},{av:'A11UserNickName',fld:'USERNICKNAME',pic:''},{av:'A12UserPassword',fld:'USERPASSWORD',pic:''},{av:'Gx_mode',fld:'vMODE',pic:'@!'},{av:'Z9UserId'},{av:'Z10UserName'},{av:'Z11UserNickName'},{av:'Z12UserPassword'},{ctrl:'BTN_DELETE',prop:'Enabled'},{ctrl:'BTN_ENTER',prop:'Enabled'}]];
   this.EnterCtrl = ["BTN_ENTER"];
   this.Initialize( );
});
gx.wi( function() { gx.createParentObj(this.user);});
