using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wstaskdelete : GXProcedure
   {
      public wstaskdelete( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public wstaskdelete( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( short aP0_TaskId )
      {
         this.AV11TaskId = aP0_TaskId;
         initialize();
         executePrivate();
      }

      public void executeSubmit( short aP0_TaskId )
      {
         wstaskdelete objwstaskdelete;
         objwstaskdelete = new wstaskdelete();
         objwstaskdelete.AV11TaskId = aP0_TaskId;
         objwstaskdelete.context.SetSubmitInitialConfig(context);
         objwstaskdelete.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objwstaskdelete);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((wstaskdelete)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Optimized DELETE. */
         /* Using cursor P000I2 */
         pr_default.execute(0, new Object[] {AV11TaskId});
         pr_default.close(0);
         dsDefault.SmartCacheProvider.SetUpdated("Task");
         /* End optimized DELETE. */
         context.CommitDataStores("wstaskdelete",pr_default);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections();
         }
         ExitApp();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wstaskdelete__default(),
            new Object[][] {
                new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV11TaskId ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
   }

   public class wstaskdelete__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new UpdateCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP000I2;
          prmP000I2 = new Object[] {
          new ParDef("@AV11TaskId",GXType.Int16,4,0)
          };
          def= new CursorDef[] {
              new CursorDef("P000I2", "DELETE FROM [Task]  WHERE [TaskId] = @AV11TaskId", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP000I2)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.wstaskdelete_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class wstaskdelete_services : GxRestService
 {
    [OperationContract(Name = "WSTaskDelete" )]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( short TaskId )
    {
       try
       {
          if ( ! ProcessHeaders("wstaskdelete") )
          {
             return  ;
          }
          wstaskdelete worker = new wstaskdelete(context);
          worker.IsMain = RunAsMain ;
          worker.execute(TaskId );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
