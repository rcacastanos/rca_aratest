using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wsgetuserinfo : GXProcedure
   {
      public wsgetuserinfo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public wsgetuserinfo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( short aP0_UserId ,
                           out string aP1_UserName )
      {
         this.AV12UserId = aP0_UserId;
         this.AV11UserName = "" ;
         initialize();
         executePrivate();
         aP1_UserName=this.AV11UserName;
      }

      public string executeUdp( short aP0_UserId )
      {
         execute(aP0_UserId, out aP1_UserName);
         return AV11UserName ;
      }

      public void executeSubmit( short aP0_UserId ,
                                 out string aP1_UserName )
      {
         wsgetuserinfo objwsgetuserinfo;
         objwsgetuserinfo = new wsgetuserinfo();
         objwsgetuserinfo.AV12UserId = aP0_UserId;
         objwsgetuserinfo.AV11UserName = "" ;
         objwsgetuserinfo.context.SetSubmitInitialConfig(context);
         objwsgetuserinfo.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objwsgetuserinfo);
         aP1_UserName=this.AV11UserName;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((wsgetuserinfo)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P000F2 */
         pr_default.execute(0, new Object[] {AV12UserId});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A9UserId = P000F2_A9UserId[0];
            A10UserName = P000F2_A10UserName[0];
            AV11UserName = A10UserName;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections();
         }
         ExitApp();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV11UserName = "";
         scmdbuf = "";
         P000F2_A9UserId = new short[1] ;
         P000F2_A10UserName = new string[] {""} ;
         A10UserName = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wsgetuserinfo__default(),
            new Object[][] {
                new Object[] {
               P000F2_A9UserId, P000F2_A10UserName
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV12UserId ;
      private short A9UserId ;
      private string scmdbuf ;
      private string AV11UserName ;
      private string A10UserName ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private short[] P000F2_A9UserId ;
      private string[] P000F2_A10UserName ;
      private string aP1_UserName ;
   }

   public class wsgetuserinfo__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP000F2;
          prmP000F2 = new Object[] {
          new ParDef("@AV12UserId",GXType.Int16,4,0)
          };
          def= new CursorDef[] {
              new CursorDef("P000F2", "SELECT [UserId], [UserName] FROM [User] WHERE [UserId] = @AV12UserId ORDER BY [UserId] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP000F2,1, GxCacheFrequency.OFF ,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1);
                ((string[]) buf[1])[0] = rslt.getVarchar(2);
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.wsgetuserinfo_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class wsgetuserinfo_services : GxRestService
 {
    [OperationContract(Name = "WSGetUserInfo" )]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( short UserId ,
                         out string UserName )
    {
       UserName = "" ;
       try
       {
          if ( ! ProcessHeaders("wsgetuserinfo") )
          {
             return  ;
          }
          wsgetuserinfo worker = new wsgetuserinfo(context);
          worker.IsMain = RunAsMain ;
          worker.execute(UserId,out UserName );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
